<?php
namespace BCWC_PLUGIN;
class SkymidsApi{

	/**
	*@var string
	*
	**/
	protected $__BASE_URL='https://skymids.com/vt/';
    //protected $__BASE_URL='http://localhost/skymids/';
    
    private static $_instance; //The single instance
    /*
	Get an instance of the Database
	@return Instance
	*/
	public static function getInstance() {
		if(!self::$_instance) { // If no instance then make one
			self::$_instance = new self();
		}
		return self::$_instance;
	}
	/**
	*
	*@param array
	*
	* @Route('/api/merchant/update', 'POST')
	**/
	public function updateMerchant($token,$updateData){
		$data=$updateData;
		$endpoint="api/merchant/update";
     	$response=$this->curlCode($endpoint,$data,$token);
     	return $response;

	}//updateMerchant method end here


     /**
     *
     *@param array
     * @Route('/api/transaction/save', 'POST')
     **/
     public function saveTransaction($token,$sendData){
     	$data=$sendData;
     	$endpoint="api/transaction/save";
     	$response=$this->curlCode($endpoint,$data,$token);
     	return $response;
     }//saveTransaction method end here
/**
     *
     *@param array
     * @Route('/api/transaction/save', 'POST')
     **/
	public function saveTransactionNoEncrypt($token,$sendData){
		$data=$sendData;
		$endpoint="api/transaction/savenoencrypt";
		$response=$this->curlCode($endpoint,$data,$token);
		return $response;
	}//saveTransaction method end here
	/**
     *
     *@param array
     * @Route('/api/transaction/save', 'POST')
     **/
	public function declinedTransaction($token,$sendData){
		$data=$sendData;
		$endpoint="api/transaction/declinedtransaction";
		$response=$this->curlCode($endpoint,$data,$token);
		return $response;
	}//saveTransaction method end here
	/**
	*
	*@param array
	* @Route('/api/merchant/create', 'POST')
	*
	**/
	public function createMerchant($token,$RequestData){
		$data=$RequestData;
		$endpoint="api/merchant/create";
		$response=$this->curlCode($endpoint,$data,$token);
		return $response;
	}//createMerchant Account end here

	/**
	*@param string
	*@param array
	**/
	private function curlCode($endpoint,$data,$token){
		$curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL =>$this->__BASE_URL."".$endpoint,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => json_encode($data),
         CURLOPT_HTTPHEADER => array(
    "Content-Type: application/json",
    "token: $token"
    ),
        CURLOPT_SSL_VERIFYHOST => 0,
        CURLOPT_SSL_VERIFYPEER => 0,
    ));

    $resp = curl_exec($curl);
    curl_close($curl);

    $json = json_decode($resp, true);
     return $json;
	}//curlcode method end here

}//class end here