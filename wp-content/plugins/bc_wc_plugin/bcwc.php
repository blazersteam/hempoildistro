<?php
/**
 * @package bc_wc_plugin
 */
/*
Plugin Name: Woocommerce Block Chain Payment Extension. 
Plugin URI: https://api.quickcard.me/
Description:Payment Gateway Plugin for Direct Credit Card Transaction.
Version: 1.0.6
Author: Quickcard
Author URI: https://api.quickcard.me/
License:  
Text Domain: blockChain
*/

define( 'BLOCKCHAIN_VERSION', '1.0.6' );
define( 'BLOCKCHAIN_MINIMUM_WP_VERSION', '4.0' );
define( 'BLOCKCHAIN_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
define( 'BLOCKCHAIN_DELETE_LIMIT', 100000 );
if ( !function_exists( 'add_action' ) ) {
	echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
	exit;
}
/** Check if WooCommerce is active **/
if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
	add_action('plugins_loaded', 'init_block_chain_gateway', 0);
   /**
	* init function of woocomerce plugin load
    */
    function init_block_chain_gateway() {
		include_once BLOCKCHAIN_PLUGIN_DIR.'woocommerce.extended.php';
		/**
		 * Add the gateway to WooCommerce
		 *
		 */
		function add_block_chain_gateway( $methods ) {
			$methods[] = 'WC_Gateway_Block_Chain';
			return $methods;
		}
		add_filter('woocommerce_payment_gateways', 'add_block_chain_gateway' );
	}//init_block_chain_gateway end here
	
	// Register metabox
	add_action( 'add_meta_boxes',  'add_directdebit_metabox' );
	function add_directdebit_metabox(){
			 add_meta_box( 'direct-debit-metabox', 'Block Chain Payment Info',  'directdebit_information_render', 'shop_order', 'normal', 'default');
	}
	// Pull the $_POST info into the Order type item
	function directdebit_information_render(){
		global $post;
		//$blockchain_auth_token = get_post_meta( $post->ID, '_blockchain_auth_token', true);
		$blockchain_request_id = get_post_meta( $post->ID, '_blockchain_request_id', true);
		$blockchain_transaction_id = get_post_meta( $post->ID, '_blockchain_transaction_id', true);
		$information_set = true;
		
		if($information_set){
			echo "<div><b>Request Id: </b>" . $blockchain_request_id . "</div>" . "<div><b>Transaction Id: </b>".$blockchain_transaction_id."</div>";
		}	}

		if ( is_admin()) {
		
			add_filter( 'plugin_action_links_' . plugin_basename(__FILE__), 'add_action_links_blockchain');
			function  add_action_links_blockchain ( $links ) {
				$mylinks = array(
				'<a href="' . admin_url( 'admin.php?page=wc-settings&tab=checkout&section=blockchainpayment' ) . '">Settings</a>',
				);
			   return array_merge( $links, $mylinks );
			   }
		}
}
