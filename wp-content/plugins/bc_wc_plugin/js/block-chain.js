'use strict';
var amount = block_chain_payment_args.amount,
    bcbUrl  = block_chain_payment_args.cb_url,
    country = block_chain_payment_args.country,
    curr   = block_chain_payment_args.currency,
    //desc   = block_chain_payment_args.desc,
    email  = block_chain_payment_args.email,
    phone_number  = block_chain_payment_args.phone_number,
    form   = jQuery( '#blockchain-pay-now-button' ),
    //logo   = block_chain_payment_args.logo || block-hain,
   // p_key  = block_chain_payment_args.p_key,
    //title  = block_chain_payment_args.title,
    btxref  = block_chain_payment_args.txnref,
    redirect_url;
    var buttonId="#block_chain_form";
    var buttonId2="#pincode-form";
    var buttonId3="#review-btn-ok";
    var buttonId4=".verifyPin";
    if ( form ) {
        jQuery(document).ready(function(){
        jQuery("#block_email_address").val(""+email);
        jQuery("#block_phone_number").val(""+phone_number);
        jQuery(document).on( "click",buttonId3, function( evt ) {
          evt.preventDefault();
          let bc_obj=BlockChain.getInstance();
         bc_obj.sendPaymentOtp(jQuery(buttonId));
        } );
        
        jQuery(document).on( "click",buttonId4, function( evt ) {
            evt.preventDefault();
            BlockChain.getInstance().verifyPaymentOtp(jQuery(buttonId));
          } );

    
    });
}

      class BlockChain{
         static getInstance(){
             if(BlockChain.instance==undefined){
                BlockChain.instance = new BlockChain();
             }
             return BlockChain.instance;
         }
           /**
     * 
     */
   
     formDataPreview(obj)
     {
         //alert(this.SITE_URL);
         jQuery( "#reviewPopupDialog" ).dialog( "open" );
         var dialogHtml="<div class='detail-review'><ul><li><label>Customer Name:</label> <span>"+obj.block_chain_nameOnCard.value+"</span></li><li><label>Amount:</label> <span>$"+amount+"</span></li><li><label>Card Number:</label> <span>"+obj.block_chain_card_number.value+"</span></li><li><label>Exp:</label><span>"+obj.block_chain_expiration.value+"</span> <label>CVV:</label> <span>"+obj.block_chain_cvv.value+"</span></li></ul></div>";
         jQuery("#review-popup").html(dialogHtml);
        return false;
     }

           sendPaymentOtp(obj){
               let classObj=this;
               jQuery( "#reviewPopupDialog" ).dialog( "close" );
            var data = obj.serialize()+'&txRef=' + btxref+'&requestType=sendotp';
            classObj.loader(true);
            jQuery.post(bcbUrl, data, function(response) {
                let res=JSON.parse(response)
                classObj.loader(false);
                if(res.result=='success'){
                    jQuery(buttonId)[0].reset();
               // jQuery(buttonId2)[0].reset();
                   // jQuery( "#pincode_popup" ).dialog('close');
                    redirect_url = res.redirect_url;
                    setTimeout( function(){
                        location.href = redirect_url;
                    },1000);
                    // alert('Am here successfully');
                    // //jQuery( "#pincode_popup" ).dialog('open');
                    // jQuery("#block_chain_nameOnCard").prop("disabled", true);
                    // jQuery("#block_email_address").prop("disabled", true);
                    // jQuery("#block_phone_number").prop("disabled", true);
                    // jQuery("#block_chain_card_number").prop("disabled", true);
                    // jQuery("#block_chain_expiration").prop("disabled", true);
                    // jQuery("#block_chain_cvv").prop("disabled", true);
                    // jQuery("#varification_field").show();
                    // jQuery("#varification_message").show();
                    // jQuery("#blockchain-pay-now-button").text("Submit");
                    // jQuery("#blockchain-pay-now-button").addClass("verifyPin");
                   
                    // jQuery("#block_chain_form").attr("onsubmit","")
                }else{
                    alert(res.message);
                }
               // checkout_form.submit();
            });
        }

        verifyPaymentOtp(obj){
            let classObj=this;
            var data = obj.serialize()+'&txRef=' + btxref+'&requestType=pincode';
            classObj.loader(true);
            jQuery.post(bcbUrl, data, function(response) {
                let res=JSON.parse(response)
                classObj.loader(false);
               if(res.result=='success'){
                jQuery(buttonId)[0].reset();
               // jQuery(buttonId2)[0].reset();
                   // jQuery( "#pincode_popup" ).dialog('close');
                    redirect_url = res.redirect_url;
                    setTimeout( function(){
                        location.href = redirect_url;
                    },1000);
                }else{
                    alert(res.message);
                }
               // checkout_form.submit();
            });
        }
         /***loader show and hide */
    loader(isShow=false)
    {
        if(isShow){
        jQuery('#block-chain-overlay').show();
        jQuery('#block-chain-loader').show();
        }else{
            jQuery('#block-chain-overlay').hide();
            jQuery('#block-chain-loader').hide();
        }
    }

    changeCardIcon(){
        let cardtext=jQuery(".block-chain-card-type").text();
        let dircturl=jQuery(".block-chain-card-type-image").data("homeaddress");
        dircturl=dircturl.trim();
        if(cardtext=='Visa'){
            jQuery(".block-chain-card-type-image").html("<img src='"+dircturl+"/visa-icon.png' style='width:50px;height:50px'>");
        }else if(cardtext=='MasterCard'){
            jQuery(".block-chain-card-type-image").html("<img src='"+dircturl+"/Master-Card.png' style='width:50px;height:50px'>");
        }else if(cardtext=='American Express'){
            jQuery(".block-chain-card-type-image").html("<img src='"+dircturl+"/amex.png' style='width:50px;height:50px'>");
        }else if(cardtext=='Discover'){
            jQuery(".block-chain-card-type-image").html("<img src='"+dircturl+"/discover.png' style='width:50px;height:50px'>");
        }else{
            jQuery(".block-chain-card-type-image").text(cardtext);
        }
    }
        
    }

    jQuery( function($) {
        $( "#pincode_popup" ).dialog({
         autoOpen: false,
         width: 400,
         resizable: false,
         modal: true
        });
   });
   jQuery( function($) {
    jQuery( "#reviewPopupDialog" ).dialog({
     autoOpen: false,
     width: 460,
     resizable: false,
     modal: true
    });
});
   jQuery(function($) {
    
    if($('body').find('.block-chain-expiration-month-and-year').length>0){
    var creditly = Creditly.initialize(
        '.creditly-wrapper .block-chain-expiration-month-and-year',
        '.creditly-wrapper .block-chain-credit-card-number',
        '.creditly-wrapper .block-chain-security-code',
        '.creditly-wrapper .block-chain-card-type');
    }
    $('#block_chain_card_number').on('keypress change', function () {
        BlockChain.getInstance().changeCardIcon();
        $(this).val(function (index, value) {
           return value.replace(/\W/gi, '').replace(/(.{4})/g, '$1 ');
         });
       });
       
  });

  
  