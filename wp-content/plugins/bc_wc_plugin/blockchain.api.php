<?php

class BlockChainApi{
  
    private static $_instance; //The single instance
    protected $END_POINT_URL='https://api.quickcard.me';
    //protected $SANDBOX_END_POINT_URL='https://quickcard.herokuapp.com';
    // 192.168.100.8:3000
    // protected $SANDBOX_END_POINT_URL='https://quickcard-pr-360.herokuapp.com';
    protected $SANDBOX_END_POINT_URL='https://quickcard.herokuapp.com';
    // protected $SANDBOX_END_POINT_URL='https://test.quickcard.me';
    private $CURRENT_URI='';
    private $GATEWAY='';
    function __construct() {
    
    }
     /*
	Get an instance of the BlockChainApi
	@return Instance
	*/
	public static function getInstance() {
		if(!self::$_instance) { // If no instance then make one
			self::$_instance = new self();
		}
		return self::$_instance;
    }
    public function selectApiUrl($is_sandbox){
        if($is_sandbox=='yes'){
            $this->CURRENT_URI=$this->SANDBOX_END_POINT_URL;
        }else{
            $this->CURRENT_URI=$this->END_POINT_URL;
        }
    }
    public function setGateway($gateway){
        $this->GATEWAY=$gateway;
    }
    public function getGateway(){
        return $this->GATEWAY;
    }
    /**
     * refund Api Code
     */
    public function refundMoney($auth_token, $transact_id) {
        $param=array(
            'auth_token' => $auth_token,
            'transact_id' => $transact_id
        );
        $endpoint='api/wallets/refund_money';
        $response=$this->curl($param,$endpoint);
        return  $response;
    }
      /**
      * @client_id = token
      * @client_secret = token
      */

      public function getAccessToken($client_id, $client_secret,$location_secure_token) {
        $param=array(
            'client_id' => $client_id,
            'client_secret' => $client_secret,
            'location_secure_token'=>$location_secure_token
         );
         $endpoint='/oauth/token/retrieve';
          $response=$this->curl($param,$endpoint);
          if($response && $response["access_token"] && $response["payment_gateway"]){
              return $response;
           }else{
                return "Unauthorized";
           }
        }

        // --------------------------------VIRTUAL TRANSACTION-----------------------------
        /**
         * @param $auth_token
         * @param $location_id
         * @param $phone_number 
         * @param $exp_date
         * @param $card_number
         * @param $cvv
         * @param $amount 
         * @param $first_name 
         * @param $last_name
         * @param $name
         * @param $email 
         **/
        public function virtual_terminal_transaction($auth_token,$location_id,$card_number,$exp,$cvv,$amount,$email,$phone,$first_name,$last_name,$order,$order_id,$i_can_pay_secure){
                $billing_state = $order->get_billing_state();
                $billing_postcode = $order->get_billing_postcode(); 
                $billing_state = !empty($billing_state) ? $billing_state : 'xx';
                $billing_postcode = !empty($billing_postcode) ? $billing_postcode : '1234';
                $param=array(
                    'auth_token'            => $auth_token,
                    'location_id'           => $location_id,
                    'order_id'              => $order_id,
                    'transaction_type'      => 'a',
                    'currency'              =>  get_option('woocommerce_currency'),
                    "street"                => $order->get_billing_address_1(),
                    "city"                  => $order->get_billing_city(),
                    "zip_code"              => $billing_postcode,
                    "state"                 => strlen($billing_state) > 2 ? substr($billing_state, 0, 2) : $billing_state,
                    "country"               => $this->get_country_iso3($order->get_billing_country()),
                    'phone_number'          => $phone,
                    'exp_date'              => $exp,
                    'card_number'           => $card_number,
                    'card_cvv'              => $cvv,
                    'amount'                => $amount,
                    'email'                 => $email,
                    'name'                  => $first_name.' '.$last_name,
                    'first_name'            => $first_name,
                    'last_name'             => $last_name,
                    'dob'                   => date('Y-m-d', strtotime('- 25 years')),
                    "success_url"           => urlencode(esc_url_raw(WC()->api_request_url('wc_gateway_success_page'))),
                    "fail_url"              => urlencode(esc_url_raw(WC()->api_request_url('wc_gateway_failure_page'))),
                    "notify_url"            => urlencode(esc_url_raw(WC()->api_request_url('wc_gateway_failure_page'))),
                    "icanpay_3ds"           => $i_can_pay_secure 

                );
            $endpoint='/api/registrations/virtual_transaction';
            $response=$this->curl($param,$endpoint);
            return  $response;
        }
        // --------------------------------END VIRTUAL TRANSACTION------------------------

        /**
         * @param $data=array()
         * @param $endpoint
         */
    public function curl($data,$endpoint){
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $this->CURRENT_URI.''.$endpoint,
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
        ));
        $resp = curl_exec($curl);
        curl_close($curl);
        $json = json_decode($resp, true);
        return $json;
 }

 // ----------------------------------New Methods---------------------------------------
    function get_country_iso3($iso2)
    {
        $country = array(
            'AF' => 'AFG',
            'AL' => 'ALB',
            'DZ' => 'DZA',
            'AS' => 'ASM',
            'AD' => 'AND',
            'AO' => 'AGO',
            'AI' => 'AIA',
            'AQ' => 'ATA',
            'AG' => 'ATG',
            'AR' => 'ARG',
            'AM' => 'ARM',
            'AW' => 'ABW',
            'AU' => 'AUS',
            'AT' => 'AUT',
            'AZ' => 'AZE',
            'BS' => 'BHS',
            'BH' => 'BHR',
            'BD' => 'BGD',
            'BB' => 'BRB',
            'BY' => 'BLR',
            'BE' => 'BEL',
            'BZ' => 'BLZ',
            'BJ' => 'BEN',
            'BM' => 'BMU',
            'BT' => 'BTN',
            'BO' => 'BOL',
            'BA' => 'BIH',
            'BW' => 'BWA',
            'BV' => 'BVT',
            'BR' => 'BRA',
            'IO' => 'IOT',
            'BN' => 'BRN',
            'BG' => 'BGR',
            'BF' => 'BFA',
            'BI' => 'BDI',
            'KH' => 'KHM',
            'CM' => 'CMR',
            'CA' => 'CAN',
            'CV' => 'CPV',
            'KY' => 'CYM',
            'CF' => 'CAF',
            'TD' => 'TCD',
            'CL' => 'CHL',
            'CN' => 'CHN',
            'CX' => 'CXR',
            'CC' => 'CCK',
            'CO' => 'COL',
            'KM' => 'COM',
            'CG' => 'COG',
            'CK' => 'COK',
            'CR' => 'CRI',
            'CI' => 'CIV',
            'HR' => 'HRV',
            'CU' => 'CUB',
            'CY' => 'CYP',
            'CZ' => 'CZE',
            'DK' => 'DNK',
            'DJ' => 'DJI',
            'DM' => 'DMA',
            'DO' => 'DOM',
            'TL' => 'TLS',
            'EC' => 'ECU',
            'EG' => 'EGY',
            'SV' => 'SLV',
            'GQ' => 'GNQ',
            'ER' => 'ERI',
            'EE' => 'EST',
            'ET' => 'ETH',
            'FK' => 'FLK',
            'FO' => 'FRO',
            'FJ' => 'FJI',
            'FI' => 'FIN',
            'FR' => 'FRA',
            'GF' => 'GUF',
            'PF' => 'PYF',
            'TF' => 'ATF',
            'GA' => 'GAB',
            'GM' => 'GMB',
            'GE' => 'GEO',
            'DE' => 'DEU',
            'GH' => 'GHA',
            'GI' => 'GIB',
            'GR' => 'GRC',
            'GL' => 'GRL',
            'GD' => 'GRD',
            'GP' => 'GLP',
            'GU' => 'GUM',
            'GT' => 'GTM',
            'GN' => 'GIN',
            'GW' => 'GNB',
            'GY' => 'GUY',
            'HT' => 'HTI',
            'HM' => 'HMD',
            'HN' => 'HND',
            'HK' => 'HKG',
            'HU' => 'HUN',
            'IS' => 'ISL',
            'IN' => 'IND',
            'ID' => 'IDN',
            'IR' => 'IRN',
            'IQ' => 'IRQ',
            'IE' => 'IRL',
            'IL' => 'ISR',
            'IT' => 'ITA',
            'JM' => 'JAM',
            'JP' => 'JPN',
            'JO' => 'JOR',
            'KZ' => 'KAZ',
            'KE' => 'KEN',
            'KI' => 'KIR',
            'KP' => 'PRK',
            'KR' => 'KOR',
            'KW' => 'KWT',
            'KG' => 'KGZ',
            'LA' => 'LAO',
            'LV' => 'LVA',
            'LB' => 'LBN',
            'LS' => 'LSO',
            'LR' => 'LBR',
            'LY' => 'LBY',
            'LI' => 'LIE',
            'LT' => 'LTU',
            'LU' => 'LUX',
            'MO' => 'MAC',
            'MK' => 'MKD',
            'MG' => 'MDG',
            'MW' => 'MWI',
            'MY' => 'MYS',
            'MV' => 'MDV',
            'ML' => 'MLI',
            'MT' => 'MLT',
            'MH' => 'MHL',
            'MQ' => 'MTQ',
            'MR' => 'MRT',
            'MU' => 'MUS',
            'YT' => 'MYT',
            'MX' => 'MEX',
            'FM' => 'FSM',
            'MD' => 'MDA',
            'MC' => 'MCO',
            'MN' => 'MNG',
            'MS' => 'MSR',
            'MA' => 'MAR',
            'MZ' => 'MOZ',
            'MM' => 'MMR',
            'NA' => 'NAM',
            'NR' => 'NRU',
            'NP' => 'NPL',
            'NL' => 'NLD',
            'AN' => 'ANT',
            'NC' => 'NCL',
            'NZ' => 'NZL',
            'NI' => 'NIC',
            'NE' => 'NER',
            'NG' => 'NGA',
            'NU' => 'NIU',
            'NF' => 'NFK',
            'MP' => 'MNP',
            'NO' => 'NOR',
            'OM' => 'OMN',
            'PK' => 'PAK',
            'PW' => 'PLW',
            'PA' => 'PAN',
            'PG' => 'PNG',
            'PY' => 'PRY',
            'PE' => 'PER',
            'PH' => 'PHL',
            'PN' => 'PCN',
            'PL' => 'POL',
            'PT' => 'PRT',
            'PR' => 'PRI',
            'QA' => 'QAT',
            'RE' => 'REU',
            'RO' => 'ROM',
            'RU' => 'RUS',
            'RW' => 'RWA',
            'KN' => 'KNA',
            'LC' => 'LCA',
            'VC' => 'VCT',
            'WS' => 'WSM',
            'SM' => 'SMR',
            'ST' => 'STP',
            'SA' => 'SAU',
            'SN' => 'SEN',
            'SC' => 'SYC',
            'SL' => 'SLE',
            'SG' => 'SGP',
            'SK' => 'SVK',
            'SI' => 'SVN',
            'SB' => 'SLB',
            'SO' => 'SOM',
            'ZA' => 'ZAF',
            'GS' => 'SGS',
            'ES' => 'ESP',
            'LK' => 'LKA',
            'SH' => 'SHN',
            'PM' => 'SPM',
            'SD' => 'SDN',
            'SR' => 'SUR',
            'SJ' => 'SJM',
            'SZ' => 'SWZ',
            'SE' => 'SWE',
            'CH' => 'CHE',
            'SY' => 'SYR',
            'TW' => 'TWN',
            'TJ' => 'TJK',
            'TZ' => 'TZA',
            'TH' => 'THA',
            'TG' => 'TGO',
            'TK' => 'TKL',
            'TO' => 'TON',
            'TT' => 'TTO',
            'TN' => 'TUN',
            'TR' => 'TUR',
            'TM' => 'TKM',
            'TC' => 'TCA',
            'TV' => 'TUV',
            'UG' => 'UGA',
            'UA' => 'UKR',
            'AE' => 'ARE',
            'GB' => 'GBR',
            'US' => 'USA',
            'UM' => 'UMI',
            'UY' => 'URY',
            'UZ' => 'UZB',
            'VU' => 'VUT',
            'VA' => 'VAT',
            'VE' => 'VEN',
            'VN' => 'VNM',
            'VG' => 'VGB',
            'VI' => 'VIR',
            'WF' => 'WLF',
            'EH' => 'ESH',
            'YE' => 'YEM',
            'CD' => 'COD',
            'ZM' => 'ZMB',
            'ZW' => 'ZWE',
            'JE' => 'JEY',
            'GG' => 'GGY',
            'ME' => 'MNE',
            'RS' => 'SRB',
            'AX' => 'ALA',
            'BQ' => 'BES',
            'CW' => 'CUW',
            'PS' => 'PSE',
            'SS' => 'SSD',
            'BL' => 'BLM',
            'MF' => 'MAF',
            'IC' => 'ICA'
        );

        return $country[$iso2];
    }

}//class end here    