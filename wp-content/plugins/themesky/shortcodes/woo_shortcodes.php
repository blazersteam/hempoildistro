<?php
function ts_remove_product_hooks_shortcode( $options = array() ){
	if( isset($options['show_label']) && !$options['show_label'] ){
		remove_action('woocommerce_after_shop_loop_item_title', 'upstore_template_loop_product_label', 1);
	}
	if( isset($options['show_image']) && !$options['show_image'] ){
		remove_action('woocommerce_before_shop_loop_item_title', 'upstore_template_loop_product_thumbnail', 10);
	}
	if( isset($options['show_categories']) && !$options['show_categories'] ){
		remove_action('woocommerce_after_shop_loop_item', 'upstore_template_loop_categories', 10);
	}
	if( isset($options['show_title']) && !$options['show_title'] ){
		remove_action('woocommerce_after_shop_loop_item', 'upstore_template_loop_product_title', 20);
	}
	if( isset($options['show_sku']) && !$options['show_sku'] ){
		remove_action('woocommerce_after_shop_loop_item', 'upstore_template_loop_product_sku', 30);
	}
	if( isset($options['show_price']) && !$options['show_price'] ){
		remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_price', 40);
	}
	if( isset($options['show_rating']) && !$options['show_rating'] ){
		remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_rating', 50);
	}
	if( isset($options['show_short_desc']) && !$options['show_short_desc'] ){
		remove_action('woocommerce_after_shop_loop_item', 'upstore_template_loop_short_description', 60);
	}
	if( isset($options['show_add_to_cart']) && !$options['show_add_to_cart'] ){
		remove_action('woocommerce_after_shop_loop_item', 'upstore_template_loop_add_to_cart', 70);
		remove_action('woocommerce_after_shop_loop_item_title', 'upstore_template_loop_add_to_cart', 10004 );
	}
	if( isset($options['show_color_swatch']) && $options['show_color_swatch'] && function_exists('upstore_template_loop_product_variable_color') ){
		add_action('woocommerce_after_shop_loop_item', 'upstore_template_loop_product_variable_color', 45);
		if( isset($options['number_color_swatch']) ){
			$number_color_swatch = absint($options['number_color_swatch']);
			add_filter('upstore_loop_product_variable_color_number', function() use ($number_color_swatch){
				return $number_color_swatch;
			});
		}
	}
}

function ts_restore_product_hooks_shortcode(){
	add_action('woocommerce_after_shop_loop_item_title', 'upstore_template_loop_product_label', 1);
	add_action('woocommerce_before_shop_loop_item_title', 'upstore_template_loop_product_thumbnail', 10);
	
	add_action('woocommerce_after_shop_loop_item', 'upstore_template_loop_categories', 10);
	add_action('woocommerce_after_shop_loop_item', 'upstore_template_loop_product_title', 20);
	add_action('woocommerce_after_shop_loop_item', 'upstore_template_loop_product_sku', 30);
	add_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_price', 40);
	add_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_rating', 50);
	add_action('woocommerce_after_shop_loop_item', 'upstore_template_loop_short_description', 60); 
	add_action('woocommerce_after_shop_loop_item', 'upstore_template_loop_add_to_cart', 70); 
	add_action('woocommerce_after_shop_loop_item_title', 'upstore_template_loop_add_to_cart', 10004 );
	remove_action('woocommerce_after_shop_loop_item', 'upstore_template_loop_product_variable_color', 45);
	remove_all_filters('upstore_loop_product_variable_color_number');
}

function ts_filter_product_by_product_type( &$args = array(), $product_type = 'recent' ){
	switch( $product_type ){
		case 'sale':
			$args['post__in'] = array_merge( array( 0 ), wc_get_product_ids_on_sale() );
		break;
		case 'featured':
			$args['tax_query'][] = array(
				'taxonomy' => 'product_visibility',
				'field'    => 'name',
				'terms'    => 'featured',
				'operator' => 'IN',
			);
		break;
		case 'best_selling':
			$args['meta_key'] 	= 'total_sales';
			$args['orderby'] 	= 'meta_value_num';
			$args['order'] 		= 'desc';
		break;
		case 'top_rated':
			$args['meta_key'] 	= '_wc_average_rating';
			$args['orderby'] 	= 'meta_value_num';
			$args['order'] 		= 'desc';
		break;
		case 'mixed_order':
			$args['orderby'] 	= 'rand';
		break;
		default: /* Recent */
			$args['orderby'] 	= 'date';
			$args['order'] 		= 'desc';
		break;
	}
}

function ts_get_product_deals_transient(){
	$key = 'all';
	if( defined('ICL_LANGUAGE_CODE') ){
		$key .= '-' . ICL_LANGUAGE_CODE;
	}
	$transient = get_transient('ts_product_deals_ids');
	if( $transient && isset($transient[$key]) && is_array($transient[$key]) ){
		return $transient[$key];
	}
	return false;
}

function ts_set_product_deals_transient( $value = array() ){
	$key = 'all';
	if( defined('ICL_LANGUAGE_CODE') ){
		$key .= '-' . ICL_LANGUAGE_CODE;
	}
	$transient = get_transient('ts_product_deals_ids');
	if( is_array($transient) ){
		$transient[$key] = $value;
	}
	else{
		$transient = array( $key => $value );
	}
	set_transient( 'ts_product_deals_ids', $transient, MONTH_IN_SECONDS );
}

add_action('wc_after_products_starting_sales', 'ts_delete_product_deals_transient');
add_action('wc_after_products_ending_sales', 'ts_delete_product_deals_transient');
add_action('woocommerce_delete_product_transients', 'ts_delete_product_deals_transient');
function ts_delete_product_deals_transient(){
	set_transient( 'ts_product_deals_ids', false, MONTH_IN_SECONDS );
}

function ts_get_product_deals_ids(){
	$product_ids = ts_get_product_deals_transient();
	if( !is_array($product_ids) ){
		global $post;
		$product_ids = array();
		$args = array(
			'post_type'				=> array('product', 'product_variation')
			,'post_status' 			=> 'publish'
			,'posts_per_page' 		=> -1
			,'meta_query' => array(
				array(
					'key'		=> '_sale_price_dates_to'
					,'value'	=> current_time( 'timestamp', true )
					,'compare'	=> '>'
					,'type'		=> 'numeric'
				)
				,array(
					'key'		=> '_sale_price_dates_from'
					,'value'	=> current_time( 'timestamp', true )
					,'compare'	=> '<'
					,'type'		=> 'numeric'
				)
			)
			,'tax_query'			=> array()
		);
		
		$products = new WP_Query( $args );
		
		if( $products->have_posts() ){
			while( $products->have_posts() ){
				$products->the_post();
				if( $post->post_type == 'product' ){
					$product_ids[] = $post->ID;
				}
				else{ /* Variation product */
					$product_ids[] = $post->post_parent;
				}
			}
		}
		$product_ids = array_unique($product_ids);
		ts_set_product_deals_transient($product_ids);
		wp_reset_postdata();
	}
	
	return $product_ids;
}

/*** Products Shortcode ***/

if( !function_exists('ts_products_shortcode') ){

	function ts_products_shortcode($atts, $content){

		extract(shortcode_atts(array(
				'title' 					=> ''
				,'title_style' 				=> ''
				,'product_type'				=> 'recent'
				,'columns' 					=> 4
				,'per_page' 				=> 4
				,'product_cats'				=> ''
				,'ids'						=> ''
				,'meta_position'			=> 'bottom'
				,'item_line'				=> 0
				,'show_image' 				=> 1
				,'show_title' 				=> 1
				,'show_sku' 				=> 0
				,'show_price' 				=> 1
				,'show_short_desc'  		=> 0
				,'show_rating' 				=> 0
				,'show_label' 				=> 1	
				,'show_categories'			=> 1	
				,'show_add_to_cart' 		=> 1
				,'show_color_swatch'		=> 0
				,'number_color_swatch'		=> 3
				,'is_slider'				=> 0
				,'rows' 					=> 1
				,'show_nav'					=> 1
				,'nav_position'				=> 'nav-top'
				,'auto_play'				=> 0
				,'margin'					=> 30
				,'overlay_style'			=> 0
				,'disable_slider_responsive'=> 0
			), $atts));
			if ( !class_exists('WooCommerce') ){
				return;
			}
			
			$options = array(
					'show_image'			=> $show_image
					,'show_label'			=> $show_label
					,'show_title'			=> $show_title
					,'show_sku'				=> $show_sku
					,'show_price'			=> $show_price
					,'show_short_desc'		=> $show_short_desc
					,'show_categories'		=> $show_categories
					,'show_rating'			=> $show_rating
					,'show_add_to_cart'		=> $show_add_to_cart
					,'show_color_swatch'	=> $show_color_swatch
					,'number_color_swatch'	=> $number_color_swatch
				);
			ts_remove_product_hooks_shortcode( $options );
			
			$args = array(
				'post_type'				=> 'product'
				,'post_status' 			=> 'publish'
				,'ignore_sticky_posts'	=> 1
				,'posts_per_page' 		=> $per_page
				,'orderby' 				=> 'date'
				,'order' 				=> 'desc'
				,'meta_query' 			=> WC()->query->get_meta_query()
				,'tax_query'           	=> WC()->query->get_tax_query()
			);
			
			ts_filter_product_by_product_type($args, $product_type);

			$product_cats = str_replace(' ', '', $product_cats);
			if( strlen($product_cats) > 0 ){
				$product_cats = explode(',', $product_cats);
			}
			if( is_array($product_cats) && count($product_cats) > 0 ){
				$field_name = is_numeric($product_cats[0])?'term_id':'slug';
				$args['tax_query'][] = array(
											'taxonomy' => 'product_cat'
											,'terms' => $product_cats
											,'field' => $field_name
											,'include_children' => false
										);
			}
			
			$ids = str_replace(' ', '', $ids);
			if( strlen($ids) > 0 ){
				$ids = explode(',', $ids);
				if( is_array($ids) && count($ids) > 0 ){
					$args['post__in'] = $ids;
					if( count($ids) == 1 ){
						$columns = 1;
					}
				}
			}
			
			ob_start();
			global $woocommerce_loop;
			if( (int)$columns <= 0 ){
				$columns = 5;
			}
			$old_woocommerce_loop_columns = $woocommerce_loop['columns'];
			$woocommerce_loop['columns'] = $columns;

			$products = new WP_Query( $args );
			
			$classes = array();
			$classes[] = 'ts-product-wrapper ts-shortcode ts-product';
			$classes[] = $product_type;
			$classes[] = $title_style;
			$classes[] = 'meta-'.$meta_position;
			$classes[] = $item_line?'item-line':'';
			
			if( $is_slider ){
				$classes[] = 'ts-slider';
				$classes[] = 'rows-'.$rows;
				if( !$title ){
					$nav_position = 'nav-middle';
				}
				if( $show_nav ){
					$classes[] = 'show-nav';
					$classes[] = $nav_position;
					if( $nav_position == 'nav-middle'){
						$classes[] = 'middle-top';
					}
				}
				if( $overlay_style ){
					$classes[] = 'overlay-style';
				}
			}
			
			$data_attr = array();
			if( $is_slider ){
				$data_attr[] = 'data-nav="'.$show_nav.'"';
				$data_attr[] = 'data-autoplay="'.$auto_play.'"';
				$data_attr[] = 'data-margin="'.absint($margin).'"';
				$data_attr[] = 'data-columns="'.$columns.'"';
				$data_attr[] = 'data-disable_responsive="'.$disable_slider_responsive.'"';
			}

			if( $products->have_posts() ): 
			?>
			<div class="<?php echo esc_attr(implode(' ', $classes)); ?>" <?php echo implode(' ', $data_attr) ?>>
				<?php if( strlen($title) > 0 ): ?>
					<header class="shortcode-heading-wrapper">
						<h3 class="heading-title"><?php echo esc_html($title); ?></h3>
					</header>
				<?php endif; ?>
				<div class="content-wrapper <?php echo ($is_slider)?'loading':'' ?>">
					<?php
					$count = 0;
					woocommerce_product_loop_start();			

					while( $products->have_posts() ){ 
						$products->the_post();	
						if( $is_slider && $rows > 1 && $count % $rows == 0 ){
							echo '<div class="product-group">';
						}
						wc_get_template_part( 'content', 'product' );
						if( $is_slider && $rows > 1 && ($count % $rows == $rows - 1 || $count == $products->post_count - 1) ){
							echo '</div>';
						}
						$count++;
					}

					woocommerce_product_loop_end();
					?>
				</div>
			</div>
			<?php
			endif;
			
			wp_reset_postdata();

			/* restore hooks */
			ts_restore_product_hooks_shortcode();

			$woocommerce_loop['columns'] = $old_woocommerce_loop_columns;
			return '<div class="woocommerce columns-'.$columns.'">' . ob_get_clean() . '</div>';
	}	
}
add_shortcode('ts_products', 'ts_products_shortcode');

/*** Products Widget ***/
if( !function_exists('ts_products_widget_shortcode') ){
	function ts_products_widget_shortcode($atts, $content){
	
		if( !class_exists('TS_Products_Widget') ){
			return;
		}
	
		extract(shortcode_atts(array(
				'product_type'			=> 'recent'
				,'rows' 				=> 3
				,'columns' 				=> 1
				,'per_page' 			=> 6
				,'product_cats'			=> ''
				,'title' 				=> ''
				,'title_style' 			=> ''
				,'show_image' 			=> 1
				,'thumbnail_style' 		=> 'default-thumbnail'
				,'show_title' 			=> 1
				,'show_price' 			=> 1
				,'show_rating' 			=> 0
				,'show_categories'		=> 1
				,'is_slider'			=> 0
				,'show_nav'				=> 1
				,'auto_play'			=> 1
			), $atts));	
		if( trim($product_cats) != '' ){
			$product_cats = array_map('trim', explode(',', $product_cats));
		}
		
		$instance = array(
			'title'					=> $title
			,'title_style'			=> $title_style
			,'product_type'			=> $product_type
			,'product_cats'			=> $product_cats
			,'row'					=> $rows
			,'columns'				=> $columns
			,'limit'				=> $per_page
			,'show_thumbnail' 		=> $show_image
			,'thumbnail_style' 		=> $thumbnail_style
			,'show_categories' 		=> $show_categories
			,'show_product_title' 	=> $show_title
			,'show_price' 			=> $show_price
			,'show_rating' 			=> $show_rating
			,'is_slider'			=> $is_slider
			,'show_nav' 			=> $show_nav
			,'auto_play' 			=> $auto_play
		);
		
		ob_start();
		the_widget('TS_Products_Widget', $instance);
		return ob_get_clean();
	}
}
add_shortcode('ts_products_widget', 'ts_products_widget_shortcode');

/* Product Category Slider */

if( !function_exists('ts_product_categories_shortcode') ){
	function ts_product_categories_shortcode($atts, $content){
		extract(shortcode_atts(array(
			'title'					=> ''
			,'title_style'			=> ''
			,'style'				=> 'style-1'
			,'layout'				=> 'slider'
			,'per_page' 			=> 5
			,'columns' 				=> 4
			,'first_level' 			=> 0
			,'parent' 				=> ''
			,'child_of' 			=> 0
			,'ids'	 				=> ''
			,'hide_empty'			=> 1
			,'show_title'			=> 1
			,'show_product_count'	=> 0
			,'show_view_shop_button'=> 1
			,'view_shop_button_text'=> 'View Shop Now'
			,'nav_position'			=> 'nav-top'
			,'show_nav' 			=> 1
			,'auto_play' 			=> 1
			,'margin'				=> 0
		),$atts));

		if ( !class_exists('WooCommerce') ){
			return;
		}
		
		if( $first_level ){
			$parent = $child_of = 0;
		}

		$args = array(
			'taxonomy'	  => 'product_cat'
			,'orderby'    => 'name'
			,'order'      => 'ASC'
			,'hide_empty' => $hide_empty
			,'include'    => array_map('trim', explode(',', $ids))
			,'pad_counts' => true
			,'parent'     => $parent
			,'child_of'   => $child_of
			,'number'     => $per_page
		);
		if( $ids ){
			$args['orderby'] = 'include';
		}
		$product_categories = get_terms($args);
		global $woocommerce_loop;
		$old_woocommerce_loop_columns = $woocommerce_loop['columns'];
		$woocommerce_loop['columns'] = $columns;	
		
		ob_start();
		
		if( count($product_categories) > 0 ):
			$classes = array();
			$classes[] = 'ts-product-category-wrapper ts-product ts-shortcode';
			$classes[] = $title_style;
			$classes[] = $style;
			$classes[] = $layout == 'slider'?'ts-slider':'grid';
			if( $layout == 'slider' && $show_nav ){
				if( !$title ){
					$nav_position = 'nav-middle';
				}
				if( $show_nav ){
					$classes[] = 'show-nav';
					$classes[] = $nav_position;
					if( $nav_position == 'nav-middle'){
						if( $style == 'style-2'){
							$classes[] = 'middle-top';
						}
					}

				}
			}
		
			$data_attr = array();
			if( $layout == 'slider' ){
				$data_attr[] = 'data-nav="'.$show_nav.'"';
				$data_attr[] = 'data-autoplay="'.$auto_play.'"';
				$data_attr[] = 'data-margin="'.$margin.'"';
				$data_attr[] = 'data-columns="'.$columns.'"';
			}
		?>
			<div class="<?php echo esc_attr(implode(' ', $classes)) ?>" <?php echo implode(' ', $data_attr); ?>>
				<?php if( $title ): ?>
					<header class="shortcode-heading-wrapper">
						<h3 class="heading-title"><?php echo esc_html($title); ?></h3>
					</header>
				<?php endif; ?>
				<div class="content-wrapper <?php echo ($layout == 'slider')?'loading':''; ?>">
					<?php 
					woocommerce_product_loop_start();
					foreach ( $product_categories as $category ) {
						wc_get_template( 'content-product_cat.php', array(
							'category' 					=> $category
							,'show_title' 				=> $show_title
							,'show_product_count' 		=> $show_product_count
							,'show_view_shop_button' 	=> $show_view_shop_button
							,'view_shop_button_text' 	=> $view_shop_button_text
							,'style' 					=> $style
						) );
					}
					woocommerce_product_loop_end();
					?>
				</div>
			</div>
		<?php
		endif;
		
		$woocommerce_loop['columns'] = $old_woocommerce_loop_columns;
		
		return '<div class="woocommerce columns-'.$columns.'">' . ob_get_clean() . '</div>';			
	}
}
add_shortcode('ts_product_categories', 'ts_product_categories_shortcode');

/* Product Brands */
if( !function_exists('ts_product_brands_shortcode') ){
	function ts_product_brands_shortcode($atts, $content){
		extract(shortcode_atts(array(
			'title'					=> ''
			,'title_style'			=> ''
			,'use_logo_setting'		=> 1
			,'per_page' 			=> 6
			,'columns' 				=> 5
			,'first_level' 			=> 0
			,'hide_empty'			=> 1
			,'show_title'			=> 1
			,'show_product_count'	=> 0
			,'nav_position'			=> 'nav-top'
			,'show_nav' 			=> 1
			,'auto_play' 			=> 1
			,'margin'				=> 0
		),$atts));

		if ( !class_exists('WooCommerce') ){
			return;
		}

		$args = array(
			'taxonomy'	  => 'ts_product_brand'
			,'orderby'    => 'name'
			,'order'      => 'ASC'
			,'hide_empty' => $hide_empty
			,'pad_counts' => true
			,'number'     => $per_page
		);
		if( $first_level ){
			$args['parent'] = 0;
		}
		$product_brands = get_terms($args);
		
		ob_start();
		
		if( count($product_brands) > 0 ):
			$classes = array();
			$classes[] = 'ts-product-brand-wrapper ts-product ts-shortcode ts-slider';
			$classes[] = $title_style;
			$classes[] = $use_logo_setting?'use-logo-setting':'';
			if( $show_nav ){
				if( !$title ){
					$nav_position = 'nav-middle';
				}
				$classes[] = 'show-nav';
				$classes[] = $nav_position;
			}
		
			$data_attr = array();
			$data_attr[] = 'data-nav="'.$show_nav.'"';
			$data_attr[] = 'data-autoplay="'.$auto_play.'"';
			$data_attr[] = 'data-margin="'.$margin.'"';
			$data_attr[] = 'data-columns="'.$columns.'"';
			
			if( $use_logo_setting ){
				$settings_option = get_option('ts_logo_setting', array());
				$data_break_point = isset($settings_option['responsive']['break_point'])?$settings_option['responsive']['break_point']:array();
				$data_item = isset($settings_option['responsive']['item'])?$settings_option['responsive']['item']:array();
				
				$data_attr[] = 'data-break_point="'.htmlentities(json_encode( $data_break_point )).'"';
				$data_attr[] = 'data-item="'.htmlentities(json_encode( $data_item )).'"';
			}
		?>
			<div class="<?php echo esc_attr(implode(' ', $classes)) ?>" <?php echo implode(' ', $data_attr); ?>>
				<?php if( $title ): ?>
					<header class="shortcode-heading-wrapper">
						<h3 class="heading-title"><?php echo esc_html($title); ?></h3>
					</header>
				<?php endif; ?>
				<div class="content-wrapper loading">
					<?php 
					foreach( $product_brands as $brand ){
						$brand_link = get_term_link($brand, 'ts_product_brand');
						$thumbnail_id = absint(get_term_meta( $brand->term_id, 'thumbnail_id', true ));
						$image_size = $use_logo_setting?'ts_logo_thumb':'upstore_product_cat';
						?>
						<div class="item">
							<a href="<?php echo esc_url( $brand_link ) ?>">
							<?php
							if( $thumbnail_id ){
								echo wp_get_attachment_image($thumbnail_id, $image_size);
							}
							else{
								echo wc_placeholder_img();
							}
							?>
							</a>
							<div class="meta-wrapper">
								<?php if( $show_title ): ?>
								<h3 class="heading-title">
									<a href="<?php echo esc_url($brand_link); ?>"><?php echo $brand->name; ?></a>
								</h3>
								<?php endif; ?>
								<?php if( $show_product_count ): ?>
								<div class="count"><?php echo sprintf( _n( '%s Product', '%s Products', $brand->count, 'themesky' ), $brand->count ); ?></div>
								<?php endif; ?>
							</div>
						</div>
						<?php
					}
					?>
				</div>
			</div>
		<?php
		endif;
		
		return '<div class="woocommerce columns-'.$columns.'">' . ob_get_clean() . '</div>';			
	}
}
add_shortcode('ts_product_brands', 'ts_product_brands_shortcode');

/* List of Product Categories */
if( !function_exists('ts_list_of_product_categories_shortcode') ){
	function ts_list_of_product_categories_shortcode( $atts ){
		extract(shortcode_atts(array(
			'title' 				=> ''
			,'style' 				=> 'style-1'
			,'color'				=> '#27af7d'
			,'banners'				=> ''
			,'parent'				=> ''
			,'direct_child'			=> 1
			,'include_parent'		=> 1
			,'ids'					=> ''
			,'limit'				=> 8
			,'hide_empty'			=> 1
			,'show_view_all_button'	=> 0
			,'view_all_button_text'	=> '+ View All'
			,'view_all_button_link'	=> ''
		), $atts));
		
		if( !class_exists('WooCommerce') ){
			return;
		}
		
		if( $parent && $include_parent ){
			$limit = absint($limit) - 1;
		}
		
		$has_banner = $banners != '';
		if( $has_banner ){
			$banners = array_map('trim', explode(',', $banners));
		}
		
		$list_categories = array();
		$args = array(
			'taxonomy'		=> 'product_cat'
			,'hide_empty'	=> $hide_empty
			,'number'		=> $limit
		);
		if( $parent ){
			if( $direct_child ){
				$args['parent'] = $parent;
			}
			else{
				$args['child_of'] = $parent;
			}
		}
		if( $ids ){
			$args['include'] = $ids;
			$args['orderby'] = 'include';
		}
		
		$list_categories = get_terms( $args );
		if( empty($list_categories) ){
			return;
		}
		
		if( !$view_all_button_link ){
			if( $parent ){
				$parent_link = get_term_link( (int)$parent, 'product_cat' );
				if( is_string($parent_link) ){
					$view_all_button_link = $parent_link;
				}
			}
			else{
				$view_all_button_link = wc_get_page_permalink( 'shop' );
			}
		}
		
		$classes = array();
		$classes[] = 'ts-list-of-product-categories-wrapper ts-shortcode';
		$classes[] = $style;
		if( $has_banner ){
			$classes[] = 'has-banner';
			if( count($banners) > 1 ){
				$classes[] = 'banner-slider';
			}
		}
		
		$rand_id = 'ts-list-of-product-categories-wrapper-'.mt_rand(0, 1000);
		$inline_style = '';
		if( $style == 'style-1' ){
			$selector = '#'.$rand_id;
			$inline_style = '<div class="ts-shortcode-custom-style hidden">';
			$inline_style .= $selector.' .heading-title{background-color:'.$color.'}';
			$inline_style .= $selector.' ul li a:hover{color:'.$color.'}';
			$inline_style .= '</div>';
		}
		ob_start();
		?>
		<div class="<?php echo esc_attr(implode(' ', $classes)); ?>" id="<?php echo esc_attr($rand_id) ?>">
			<?php echo trim($inline_style); ?>
			<?php if( $has_banner  && $style == 'style-1'): ?>
			<div class="banners loading">
				<figure>
				<?php 
				foreach( $banners as $banner ){
					echo wp_get_attachment_image($banner, 'full');
				}
				?>
				</figure>
			</div>
			<?php endif; ?>
			<div class="list-categories">
				<?php if( $title ): ?>
				<h3 class="heading-title"><?php echo esc_html($title) ?></h3>
				<?php endif; ?>
				<ul>
					<?php 
					if( $parent && $include_parent ){
						$parent_obj = get_term($parent, 'product_cat');
						if( is_object($parent_obj) ){
					?>
						<li><a href="<?php echo get_term_link($parent_obj, 'product_cat'); ?>"><?php echo esc_html($parent_obj->name); ?></a></li>
					<?php
						}
					}
					?>
					
					<?php foreach( $list_categories as $category ): ?>
					<li><a href="<?php echo get_term_link($category, 'product_cat'); ?>"><?php echo esc_html($category->name); ?></a></li>
					<?php endforeach; ?>
					
					<?php if( $show_view_all_button ){ ?>
					<li class="view-all"><a href="<?php echo esc_url($view_all_button_link); ?>"><?php echo esc_html($view_all_button_text); ?></a></li>
					<?php } ?>
				</ul>
			</div>
			<?php if( $has_banner  && $style == 'style-2'): ?>
			<div class="banners loading">
				<figure>
				<?php 
				foreach( $banners as $banner ){
					echo wp_get_attachment_image($banner, 'full');
				}
				?>
				</figure>
			</div>
			<?php endif; ?>
		</div>
		<?php
		return ob_get_clean();
	}
}
add_shortcode('ts_list_of_product_categories', 'ts_list_of_product_categories_shortcode');

/* TS Product Deals */
if( !function_exists('ts_product_deals_shortcode') ){
	function ts_product_deals_shortcode($atts, $content = null){

		extract(shortcode_atts(array(
				'title' 				=> ''
				,'title_style' 			=> ''
				,'text_style' 			=> 'text-default'
				,'single_style' 		=> 0
				,'single_style_layout' 	=> 'list'
				,'counter_style'		=> 'counter-horizontal'
				,'layout' 				=> 'slider'
				,'item_border' 			=> 0
				,'product_type'			=> 'recent'
				,'columns' 				=> 4
				,'per_page' 			=> 5
				,'product_cats'			=> ''
				,'ids'					=> ''
				,'show_counter'			=> 1
				,'show_image' 			=> 1
				,'show_title' 			=> 1
				,'show_sku' 			=> 0
				,'show_price' 			=> 1
				,'show_short_desc'  	=> 0
				,'short_desc_words'  	=> 8
				,'show_rating' 			=> 1
				,'show_label' 			=> 1	
				,'show_categories'		=> 0	
				,'show_add_to_cart' 	=> 1
				,'availability_bar' 	=> 0
				,'show_nav'				=> 1
				,'nav_position'			=> 'nav-top'
				,'auto_play'			=> 1
				,'margin'				=> 30
			), $atts));			

			if ( !class_exists('WooCommerce') ){
				return;
			}
			
			$product_ids_on_sale = ts_get_product_deals_ids();
			
			if( $ids ){
				$ids = array_map('trim', explode(',', $ids));
				$product_ids_on_sale = array_intersect($product_ids_on_sale, $ids);
			}
			
			if( !$product_ids_on_sale ){
				return;
			}
			
			if( $single_style ){
				$layout = 'grid';
				$columns = 1;
				$per_page = 1;
				$item_border = 0;
				if( $single_style_layout == 'grid' ){
					$counter_style = 'counter-horizontal';
				}
			}
			
			if( $item_border ){
				$margin = 0;
			}
			
			$per_page = absint($per_page);
			
			add_filter('upstore_grid_short_desc_limit_words', function() use ($short_desc_words){
				return absint($short_desc_words);
			});
			
			add_filter('upstore_loop_product_thumbnail', function($thumbnail_size) use ($single_style){
				if( $single_style ){
					return 'woocommerce_single';
				}
				return $thumbnail_size;
			});
			
			if( $show_counter ){
				add_action('woocommerce_after_shop_loop_item', 'ts_template_loop_time_deals', 65);
			}
			
			if( $availability_bar ){
				add_action('woocommerce_after_shop_loop_item', 'ts_product_availability_bar', 64);
			}
			
			/* Remove hook */
			$options = array(
					'show_image'		=> $show_image
					,'show_label'		=> $show_label
					,'show_title'		=> $show_title
					,'show_sku'			=> $show_sku
					,'show_price'		=> $show_price
					,'show_short_desc'	=> $show_short_desc
					,'show_categories'	=> $show_categories
					,'show_rating'		=> $show_rating
					,'show_add_to_cart'	=> $show_add_to_cart
				);
			ts_remove_product_hooks_shortcode( $options );

			global $woocommerce_loop, $post, $product;
			if( (int)$columns <= 0 ){
				$columns = 5;
			}
			$old_woocommerce_loop_columns = $woocommerce_loop['columns'];
			$woocommerce_loop['columns'] = $columns;
			
			$args = array(
				'post_type'				=> 'product'
				,'post_status' 			=> 'publish'
				,'posts_per_page' 		=> $per_page
				,'orderby' 				=> 'date'
				,'order' 				=> 'desc'
				,'post__in'				=> $product_ids_on_sale
				,'meta_query' 			=> WC()->query->get_meta_query()
				,'tax_query'           	=> WC()->query->get_tax_query()
			);
			
			ts_filter_product_by_product_type($args, $product_type);
			
			if( $product_cats ){
				$product_cats = array_map('trim', explode(',', $product_cats));
				$args['tax_query'][] = array(
								'taxonomy' 	=> 'product_cat'
								,'terms' 	=> $product_cats
								,'field' 	=> 'term_id'
							);
			}
			
			$products = new WP_Query($args);
			
			ob_start();
			
			if( $products->have_posts() ): 
				$classes = array();
				$classes[] = 'ts-product-deals-wrapper ts-shortcode ts-product';
				if( $single_style ){
					$classes[] = 'single-style';
					$classes[] = 'single-style-layout-' . $single_style_layout;
					$classes[] = $show_counter?$counter_style:'';
				}
				$classes[] = $show_image?'':'no-thumbnail';
				$classes[] = $title_style;
				$classes[] = $text_style;
				$classes[] = 'layout-' . $layout;
				$classes[] = $item_border?'item-border':'';
				
				if( $layout == 'slider' ){
					$classes[] = 'ts-slider';
					if( !$title ){
						$classes[] = 'no-title';
					}
					if( $show_nav ){
						$classes[] = 'show-nav';
						$classes[] = $nav_position;
					}
				}
				$classes = array_filter($classes);
				
				$data_attr = array();
				if( $layout == 'slider' ){
					$data_attr[] = 'data-nav="'.esc_attr($show_nav).'"';
					$data_attr[] = 'data-autoplay="'.esc_attr($auto_play).'"';
					$data_attr[] = 'data-margin="'.esc_attr($margin).'"';
					$data_attr[] = 'data-columns="'.esc_attr($columns).'"';
				}
				?>
				<div class="<?php echo esc_attr( implode(' ', $classes) ); ?>" <?php echo implode(' ', $data_attr); ?>>
					<?php if( $title ): ?>
						<header class="shortcode-heading-wrapper">
							<h3 class="heading-title"><?php echo esc_html($title); ?></h3>
						</header>
					<?php endif; ?>
					<div class="content-wrapper <?php echo ($layout == 'slider')?'loading':''; ?>">
						<?php woocommerce_product_loop_start(); ?>				

						<?php while( $products->have_posts() ): $products->the_post(); ?>
							<?php wc_get_template_part( 'content', 'product' ); ?>							
						<?php endwhile; ?>			

						<?php woocommerce_product_loop_end(); ?>
					</div>
				</div>
				<?php
			endif;
			
			wp_reset_postdata();			

			/* restore hooks */
			if( $show_counter ){
				remove_action('woocommerce_after_shop_loop_item', 'ts_template_loop_time_deals', 65);
			}
			
			if( $availability_bar ){
				remove_action('woocommerce_after_shop_loop_item', 'ts_product_availability_bar', 64);
			}
			
			remove_all_filters('upstore_grid_short_desc_limit_words');
			remove_all_filters('upstore_loop_product_thumbnail');

			ts_restore_product_hooks_shortcode();

			$woocommerce_loop['columns'] = $old_woocommerce_loop_columns;
			
			return '<div class="woocommerce columns-'.$columns.'">' . ob_get_clean() . '</div>';
	}
}
add_shortcode('ts_product_deals', 'ts_product_deals_shortcode');

/* TS Product Deals 2 */
if( !function_exists('ts_product_deals_2_shortcode') ){
	function ts_product_deals_2_shortcode($atts, $content = null){

		extract(shortcode_atts(array(
				'title' 				=> ''
				,'title_style' 			=> ''
				,'show_small_products' 	=> 1
				,'product_type'			=> 'recent'
				,'per_page' 			=> 4
				,'columns' 				=> 2
				,'item_line' 			=> 1
				,'product_cats'			=> ''
				,'ids'					=> ''
				,'show_counter'			=> 1
				,'show_image' 			=> 1
				,'show_title' 			=> 1
				,'show_sku' 			=> 0
				,'show_price' 			=> 1
				,'show_short_desc'  	=> 1
				,'short_desc_words'  	=> 20
				,'show_rating' 			=> 1
				,'show_label' 			=> 1	
				,'show_categories'		=> 0	
				,'show_add_to_cart' 	=> 1
				,'availability_bar'		=> 1
				,'show_nav'				=> 1
				,'prev_nav_text'		=> 'Prev deal'
				,'next_nav_text'		=> 'Next deal'
				,'auto_play'			=> 1
			), $atts));

			if ( !class_exists('WooCommerce') ){
				return;
			}
			
			$product_ids_on_sale = ts_get_product_deals_ids();
			
			if( $ids ){
				$ids = array_map('trim', explode(',', $ids));
				$product_ids_on_sale = array_intersect($product_ids_on_sale, $ids);
			}
			
			if( !$product_ids_on_sale ){
				return;
			}
			
			$per_page = absint($per_page);
			
			add_filter('upstore_grid_short_desc_limit_words', function() use ($short_desc_words){
				return absint($short_desc_words);
			});
			
			if( $show_counter ){
				add_action('woocommerce_after_shop_loop_item', 'ts_template_loop_time_deals', 65);
			}
			
			if( $availability_bar ){
				add_action('woocommerce_after_shop_loop_item', 'ts_product_availability_bar', 64);
			}
			
			/* Remove hook */
			$options = array(
					'show_image'		=> $show_image
					,'show_label'		=> $show_label
					,'show_title'		=> $show_title
					,'show_sku'			=> $show_sku
					,'show_price'		=> $show_price
					,'show_short_desc'	=> $show_short_desc
					,'show_categories'	=> $show_categories
					,'show_rating'		=> $show_rating
					,'show_add_to_cart'	=> $show_add_to_cart
				);
			ts_remove_product_hooks_shortcode( $options );

			global $post, $product;
			
			$args = array(
				'post_type'				=> 'product'
				,'post_status' 			=> 'publish'
				,'posts_per_page' 		=> $per_page
				,'orderby' 				=> 'date'
				,'order' 				=> 'desc'
				,'post__in'				=> $product_ids_on_sale
				,'meta_query' 			=> WC()->query->get_meta_query()
				,'tax_query'           	=> WC()->query->get_tax_query()
			);
			
			ts_filter_product_by_product_type($args, $product_type);
			
			if( $product_cats ){
				$product_cats = array_map('trim', explode(',', $product_cats));
				$args['tax_query'][] = array(
								'taxonomy' 	=> 'product_cat'
								,'terms' 	=> $product_cats
								,'field' 	=> 'term_id'
							);
			}
			
			$products = new WP_Query($args);
			
			ob_start();
			
			if( $products->have_posts() ): 
				$classes = array();
				$classes[] = 'ts-product-deals-2-wrapper ts-shortcode ts-product';
				$classes[] = $show_image?'':'no-thumbnail';
				$classes[] = $show_small_products?'':'no-small-products';
				$classes[] = $title_style;
				$classes[] = 'ts-slider';
				if( $show_nav ){
					$classes[] = 'show-nav';
				}
				if( !$show_small_products ){
					$classes[] = $item_line?'item-line':'';
				}
				
				$small_products_slider = $products->post_count > 4?true:false;
				if( $show_small_products && $small_products_slider ){
					$classes[] = 'small-products-slider';
					wp_enqueue_script( 'jquery-caroufredsel' );
				}
				
				$data_attr = array();
				$data_attr[] = 'data-nav="'.esc_attr($show_nav).'"';
				$data_attr[] = 'data-autoplay="'.esc_attr($auto_play).'"';
				$data_attr[] = 'data-prev_nav_text="'.esc_attr($prev_nav_text).'"';
				$data_attr[] = 'data-next_nav_text="'.esc_attr($next_nav_text).'"';
				if( !$show_small_products ){
					$data_attr[] = 'data-columns="'.absint($columns).'"';
				}
				?>
				<div class="<?php echo esc_attr( implode(' ', $classes) ); ?>" <?php echo implode(' ', $data_attr); ?>>
					<?php if( $title ): ?>
						<header class="shortcode-heading-wrapper">
							<h3 class="heading-title"><?php echo esc_html($title); ?></h3>
						</header>
					<?php endif; ?>
					<div class="content-wrapper">
						<?php if( $show_small_products ): ?>
						<div class="small-products <?php echo $small_products_slider?'loading':''; ?>">
							<ul class="product_list_widget">
							<?php
							$index = 0;
							while( $products->have_posts() ): $products->the_post();
							$product = wc_get_product( $post->ID );
							$product_title = $product->get_title();
							$product_permalink = $product->get_permalink();
							
							?>
								<li data-index="<?php echo $index++; ?>">
									<?php if( $show_image ): ?>
									<a class="ts-wg-thumbnail" href="<?php echo esc_url( $product_permalink ); ?>" title="<?php echo esc_attr( $product_title ); ?>">
									<?php echo $product->get_image(); ?>
									</a>
									<?php endif; ?>
									<div class="ts-wg-meta">
										<?php if( $show_title ): ?>
										<a href="<?php echo esc_url( $product_permalink ); ?>" title="<?php echo esc_attr( $product_title ); ?>">
											<?php echo esc_html( $product_title ); ?>
										</a>
										<?php 
										endif;
										if( $show_price ){ woocommerce_template_loop_price(); }
										if( $show_rating ){ woocommerce_template_loop_rating(); }
										?>
									</div>
								</li>
							<?php
							endwhile;
							?>
							</ul>
							<?php if( $show_nav && $small_products_slider ): ?>
							<div class="navigation">
								<div class="prev-button"></div>
								<div class="next-button"></div>
							</div>
							<?php endif; ?>
						</div>
						<?php endif; ?>
						<div class="big-product list loading">
							<?php woocommerce_product_loop_start(); ?>				

							<?php while( $products->have_posts() ): $products->the_post(); ?>
								<?php wc_get_template_part( 'content', 'product' ); ?>							
							<?php endwhile; ?>			

							<?php woocommerce_product_loop_end(); ?>
						</div>
					</div>
				</div>
				<?php
			endif;
			
			wp_reset_postdata();			

			/* restore hooks */
			if( $show_counter ){
				remove_action('woocommerce_after_shop_loop_item', 'ts_template_loop_time_deals', 65);
			}
			
			if( $availability_bar ){
				remove_action('woocommerce_after_shop_loop_item', 'ts_product_availability_bar', 64);
			}
			
			remove_all_filters('upstore_grid_short_desc_limit_words');

			ts_restore_product_hooks_shortcode();
			
			return '<div class="woocommerce">' . ob_get_clean() . '</div>';
	}
}
add_shortcode('ts_product_deals_2', 'ts_product_deals_2_shortcode');

if( !function_exists('ts_product_availability_bar') ){
	function ts_product_availability_bar(){
		global $product;
		$total_sales = $product->get_total_sales();
		$stock_quantity = $product->get_stock_quantity();
		if( $stock_quantity ){
			$total = $total_sales + $stock_quantity;
			$percent = $stock_quantity * 100 / $total;
		?>
		<div class="availability-bar">
			<span class="available"><?php esc_html_e('Available:', 'themesky') ?> <?php echo esc_html($stock_quantity) ?></span>
			<span class="sold"><?php esc_html_e('Already Sold:', 'themesky') ?> <?php echo esc_html($total_sales) ?></span>
			<div class="progress-bar">
				<span style="width:<?php echo number_format($percent, 2) ?>%"></span>
			</div>
		</div>
		<?php
		}
	}
}

if( !function_exists('ts_template_loop_time_deals') ){
	function ts_template_loop_time_deals(){
		global $product;
		$date_to = '';
		$date_from = '';
		if( $product->get_type() == 'variable' ){
			$children = $product->get_children();
			if( is_array($children) && count($children) > 0 ){
				foreach( $children as $children_id ){
					$date_to = get_post_meta($children_id, '_sale_price_dates_to', true);
					$date_from = get_post_meta($children_id, '_sale_price_dates_from', true);
					if( $date_to != '' ){
						break;
					}
				}
			}
		}
		else{
			$date_to = get_post_meta($product->get_id(), '_sale_price_dates_to', true);
			$date_from = get_post_meta($product->get_id(), '_sale_price_dates_from', true);
		}
		
		$current_time = current_time('timestamp', true);
		
		if( $date_to == '' || $date_from == '' || $date_from > $current_time || $date_to < $current_time ){
			return;
		}
		
		$delta = $date_to - $current_time;
		
		$time_day = 60 * 60 * 24;
		$time_hour = 60 * 60;
		$time_minute = 60;
		
		$day = floor( $delta / $time_day );
		$delta -= $day * $time_day;
		
		$hour = floor( $delta / $time_hour );
		$delta -= $hour * $time_hour;
		
		$minute = floor( $delta / $time_minute );
		$delta -= $minute * $time_minute;
		
		if( $delta > 0 ){
			$second = $delta;
		}
		else{
			$second = 0;
		}
		
		$day = zeroise($day, 2);
		$hour = zeroise($hour, 2);
		$minute = zeroise($minute, 2);
		$second = zeroise($second, 2);

		?>
		<div class="counter-wrapper days-<?php echo strlen($day); ?>">
			<div class="days">
				<div class="number-wrapper">
					<span class="number"><?php echo esc_html($day); ?></span>
				</div>
				<div class="ref-wrapper">
					<?php esc_html_e('Days', 'themesky'); ?>
				</div>
			</div>
			<div class="hours">
				<div class="number-wrapper">
					<span class="number"><?php echo esc_html($hour); ?></span>
				</div>
				<div class="ref-wrapper">
					<?php esc_html_e('Hours', 'themesky'); ?>
				</div>
			</div>
			<div class="minutes">
				<div class="number-wrapper">
					<span class="number"><?php echo esc_html($minute); ?></span>
				</div>
				<div class="ref-wrapper">
					<?php esc_html_e('Mins', 'themesky'); ?>
				</div>
			</div>
			<div class="seconds">
				<div class="number-wrapper">
					<span class="number"><?php echo esc_html($second); ?></span>
				</div>
				<div class="ref-wrapper">
					<?php esc_html_e('Secs', 'themesky'); ?>
				</div>
			</div>
		</div>
		<?php
	}
}

/* Product in category tabs */
if( !function_exists('ts_products_in_category_tabs_shortcode') ){
	function ts_products_in_category_tabs_shortcode($atts, $content){

		extract(shortcode_atts(array(
			'title' 						=> ''
			,'tab_style' 					=> 'horizontal-tab'
			,'style' 						=> 'style-1'
			,'tab_heading_style' 			=> 'tab-heading-style-1'
			,'tab_icons'					=> ''
			,'tab_icons_hover'				=> ''
			,'background_images'			=> ''
			,'product_type'					=> 'recent'
			,'columns' 						=> 4
			,'per_page' 					=> 8
			,'product_cats'					=> ''
			,'parent_cat' 					=> ''
			,'include_children' 			=> 0
			,'show_general_tab' 			=> 0
			,'general_tab_heading' 			=> ''
			,'product_type_general_tab' 	=> 'recent'
			,'show_image' 					=> 1
			,'show_title' 					=> 1
			,'show_sku' 					=> 0
			,'show_price' 					=> 1
			,'show_short_desc'  			=> 0
			,'show_rating' 					=> 1
			,'show_label' 					=> 1
			,'show_categories'				=> 0	
			,'show_add_to_cart' 			=> 1
			,'show_see_more_button' 		=> 0
			,'show_see_more_general_tab' 	=> 0
			,'see_more_button_text' 		=> 'See more'
			,'is_slider' 					=> 0
			,'nav_position'					=> 'nav-top'
			,'rows' 						=> 1
			,'show_nav' 					=> 1
			,'auto_play' 					=> 1
		), $atts));
		if ( !class_exists('WooCommerce') ){
			return;
		}
		
		if( !$product_cats && !$parent_cat ){
			return;
		}
		
		if( !$product_cats ){
			$sub_cats = get_terms('product_cat', array('parent' => $parent_cat, 'fields' => 'ids', 'orderby' => 'none'));
			if( is_array($sub_cats) && !empty($sub_cats) ){
				$product_cats = implode(',', $sub_cats);
			}
			else{
				return;
			}
		}
		else{
			$parent_cat = '';
		}
		
		$atts = compact('product_type', 'columns', 'rows', 'per_page' ,'product_cats', 'include_children'
						,'show_image', 'show_title', 'show_sku', 'show_price', 'show_short_desc', 'show_rating', 'show_label' ,'show_categories', 'show_add_to_cart'
						,'show_see_more_button', 'show_see_more_general_tab', 'show_general_tab', 'product_type_general_tab', 'is_slider', 'show_nav', 'auto_play');
		
		$icon_data = array();
		$background_image_data = array();
		if( $style == 'style-2' ){
			if( $tab_icons ){
				$tab_icons = array_map('trim', explode(',', $tab_icons));
				if( $tab_icons_hover ){
					$tab_icons_hover = array_map('trim', explode(',', $tab_icons_hover));
				}
				
				foreach( $tab_icons as $k => $icon ){
					$data = array('icon' => wp_get_attachment_image($icon, 'thumbnail'));
					if( is_array($tab_icons_hover) && isset($tab_icons_hover[$k]) ){
						$data['icon_hover'] = wp_get_attachment_image($tab_icons_hover[$k], 'thumbnail', false, array('class' => 'hover-icon'));
					}
					$icon_data[] = $data;
				}
			}
			
			if( $background_images ){
				$background_images = array_map('trim', explode(',', $background_images));
				foreach( $background_images as $image ){
					$background_image_data[] = wp_get_attachment_image_url($image, 'full');
				}
			}
		}
		
		$classes = array();
		$classes[] = 'ts-product-in-category-tab-wrapper ts-shortcode ts-product title-center';
		$classes[] = $product_type;
		$classes[] = $tab_style;
		$classes[] = $style;
		
		if( $style == 'style-1' ){
			$classes[] = $tab_heading_style;
		}
		
		if( $show_see_more_button ){
			$classes[] = 'has-see-more-button';
		}
		else{
			$classes[] = 'no-see-more-button';
		}
		if( isset($background_image_data[0]) ){
			$classes[] = 'has-background';
		}
		
		if( $is_slider ){
			$classes[] = 'ts-slider';
			$classes[] = 'rows-'.$rows;
			if( $show_nav ){
				$classes[] = 'show-nav';
				$classes[] = $nav_position;
				if( $rows < 2 ){
					$classes[] = 'middle-top';
				}
			}
		}
		$current_cat = '';
		$is_general_tab = false;
		$see_more_link = '#';
		
		$rand_id = 'ts-product-in-category-tab-'.mt_rand(0, 1000);
		ob_start();
		?>
		<div class="<?php echo esc_attr(implode(' ', $classes)); ?>" id="<?php echo esc_attr($rand_id) ?>" data-atts="<?php echo htmlentities(json_encode($atts)); ?>">
			<div class="column-tabs">
				<?php if( $title ): ?>
				<header class="heading-tab">
					<h3 class="heading-title"><?php echo esc_html($title); ?></h3>
				</header>
				<?php endif; ?>
				<ul class="tabs">
				<?php 
				$tab_icon_index = 0;
				if( $show_general_tab ){
					if( $parent_cat ){
						$current_cat = $parent_cat;
						$see_more_link = get_term_link((int)$parent_cat, 'product_cat');
						if( is_wp_error($see_more_link) ){
							$see_more_link = wc_get_page_permalink('shop');
						}
					}
					else{
						$current_cat = $product_cats;
						$see_more_link = wc_get_page_permalink('shop');
					}
					$is_general_tab = true;
				?>
					<li class="tab-item general-tab current" data-product_cat="<?php echo $current_cat; ?>" data-link="<?php echo esc_url($see_more_link) ?>">
						<?php if( isset($icon_data[$tab_icon_index]) ): $has_icon_hover = isset($icon_data[$tab_icon_index]['icon_hover']); ?>
						<span class="<?php echo $has_icon_hover?'has-icon-hover':''; ?>">
							<?php echo $icon_data[$tab_icon_index]['icon']; ?>
							<?php echo $has_icon_hover?$icon_data[$tab_icon_index]['icon_hover']:''; ?>
						</span>
						<?php endif; ?>
						<span><?php echo esc_html($general_tab_heading) ?></span>
					</li>
				<?php
					$tab_icon_index++;
				}
				
				$product_cats = array_map('trim', explode(',', $product_cats));
				foreach( $product_cats as $k => $product_cat ):
					$term = get_term_by( 'term_id', $product_cat, 'product_cat');
					if( !isset($term->name) ){
						continue;
					}
					$current_tab = false;
					if( $current_cat == '' ){
						$current_tab = true;
						$current_cat = $product_cat;
						$see_more_link = get_term_link($term, 'product_cat');
					}
				?>
					<li class="tab-item <?php echo ($current_tab)?'current':''; ?>" data-product_cat="<?php echo esc_attr($product_cat) ?>" data-link="<?php echo esc_url(get_term_link($term, 'product_cat')) ?>">
						<?php if( isset($icon_data[$tab_icon_index]) ): $has_icon_hover = isset($icon_data[$tab_icon_index]['icon_hover']); ?>
						<span class="<?php echo $has_icon_hover?'has-icon-hover':''; ?>">
							<?php echo $icon_data[$tab_icon_index]['icon']; ?>
							<?php echo $has_icon_hover?$icon_data[$tab_icon_index]['icon_hover']:''; ?>
						</span>
						<?php endif; ?>
						<span><?php echo esc_html($term->name) ?></span>
					</li>
				<?php
					$tab_icon_index++;
				endforeach;
				?>
				</ul>
			</div>
			
			<div class="column-content" <?php echo isset($background_image_data[0])?'style="background-image:url('.$background_image_data[0].')"':''; ?> <?php echo (count($background_image_data) > 1)?'data-background_images="'.implode(',', $background_image_data).'"':''; ?>>
				<div class="column-products woocommerce columns-<?php echo esc_attr($columns) ?>">
					<?php echo ts_get_product_content_in_category_tab($atts, $current_cat, $is_general_tab); ?>
				</div>
				
				<?php if( $show_see_more_button ): ?>
				<div class="see-more-wrapper">
					<a class="see-more-button button" href="<?php echo esc_url($see_more_link) ?>"><?php echo esc_html($see_more_button_text) ?></a>
				</div>
				<?php endif; ?>
			</div>
		</div>
		<?php
		
		return ob_get_clean();
	}	
}
add_shortcode('ts_products_in_category_tabs', 'ts_products_in_category_tabs_shortcode');

add_action('wp_ajax_ts_get_product_content_in_category_tab', 'ts_get_product_content_in_category_tab');
add_action('wp_ajax_nopriv_ts_get_product_content_in_category_tab', 'ts_get_product_content_in_category_tab');
if( !function_exists('ts_get_product_content_in_category_tab') ){
	function ts_get_product_content_in_category_tab( $atts = array(), $product_cat = '', $is_general_tab = false ){
		if( wp_doing_ajax() ){
			if( empty($_POST['atts']) ){
				die('0');
			}
			$atts = $_POST['atts'];
			$product_cat = isset($_POST['product_cat'])?$_POST['product_cat']:'';
			$is_general_tab = (isset($_POST['is_general_tab']) && $_POST['is_general_tab'])?true:false;
		}
		
		if( $is_general_tab ){
			$atts['product_type'] = $atts['product_type_general_tab'];
		}
		
		ob_start();
		extract($atts);
		
		$options = array(
				'show_image'		=> $show_image
				,'show_label'		=> $show_label
				,'show_title'		=> $show_title
				,'show_sku'			=> $show_sku
				,'show_price'		=> $show_price
				,'show_short_desc'	=> $show_short_desc
				,'show_categories'	=> $show_categories
				,'show_rating'		=> $show_rating
				,'show_add_to_cart'	=> $show_add_to_cart
			);
		ts_remove_product_hooks_shortcode( $options );
		
		$args = array(
			'post_type'				=> 'product'
			,'post_status' 			=> 'publish'
			,'ignore_sticky_posts'	=> 1
			,'posts_per_page' 		=> $per_page
			,'orderby' 				=> 'date'
			,'order' 				=> 'desc'
			,'meta_query' 			=> WC()->query->get_meta_query()
			,'tax_query'           	=> WC()->query->get_tax_query()
		);

		ts_filter_product_by_product_type($args, $product_type);
		
		if( $product_cat ){
			$args['tax_query'][] = array(
									'taxonomy' => 'product_cat'
									,'terms' => array_map('trim', explode(',', $product_cat))
									,'field' => 'term_id'
									,'include_children' => $include_children
									);
		}
		
		global $woocommerce_loop;
		if( (int)$columns <= 0 ){
			$columns = 3;
		}
		$old_woocommerce_loop_columns = $woocommerce_loop['columns'];
		$woocommerce_loop['columns'] = $columns;

		$products = new WP_Query( $args );
		
		$count = 0;
		
		woocommerce_product_loop_start();
		if( $products->have_posts() ){	

			if( isset($show_see_more_button, $products->found_posts, $products->post_count) && $products->found_posts == $products->post_count ){
				echo '<div class="hidden hide-see-more"></div>';
			}

			while( $products->have_posts() ){ 
				$products->the_post();
				
				if( $is_slider && $rows > 1 && $count % $rows == 0 ){
					echo '<div class="product-group">';
				}
				
				wc_get_template_part( 'content', 'product' );
				
				if( $is_slider && $rows > 1 && ($count % $rows == $rows - 1 || $count == $products->post_count - 1) ){
					echo '</div>';
				}
				$count++;
			}

		}
		woocommerce_product_loop_end();
		
		wp_reset_postdata();

		/* restore hooks */
		ts_restore_product_hooks_shortcode();

		$woocommerce_loop['columns'] = $old_woocommerce_loop_columns;
		
		if( wp_doing_ajax() ){
			die(ob_get_clean());
		}
		else{
			return ob_get_clean();
		}
	}
}

/* Product in category tabs */
if( !function_exists('ts_products_in_product_type_tabs_shortcode') ){
	function ts_products_in_product_type_tabs_shortcode($atts, $content){

		extract(shortcode_atts(array(
			'title' 						=> ''
			,'tab_style'					=> 'style-1'
			,'tab_1'					    => 1
			,'tab_1_heading'				=> 'Featured'
			,'tab_1_product_type'			=> 'featured'
			,'tab_2'					    => 1
			,'tab_2_heading'				=> 'Best Selling'
			,'tab_2_product_type'			=> 'best_selling'
			,'tab_3'					    => 1
			,'tab_3_heading'				=> 'On Sale'
			,'tab_3_product_type'			=> 'sale'
			,'tab_4'					    => 1
			,'tab_4_heading'				=> 'Top Rated'
			,'tab_4_product_type'			=> 'top_rated'
			,'tab_5'					    => 1
			,'tab_5_heading'				=> 'Recent'
			,'tab_5_product_type'			=> 'recent'
			,'color'						=> '#27af7d'
			,'banners'						=> ''
			,'active_tab'					=> 1
			,'columns' 						=> 4
			,'per_page' 					=> 6
			,'product_cats'					=> ''
			,'include_children' 			=> 1
			,'show_list_categories'			=> 1
			,'banner_1'						=> ''
			,'banner_1_link'				=> ''
			,'banner_2'						=> ''
			,'banner_2_link'				=> ''
			,'item_border' 					=> 1
			,'item_line' 					=> 1
			,'show_image' 					=> 1
			,'show_title' 					=> 1
			,'show_sku' 					=> 0
			,'show_price' 					=> 1
			,'show_short_desc'  			=> 0
			,'show_rating' 					=> 1
			,'show_label' 					=> 1
			,'show_categories'				=> 0	
			,'show_add_to_cart' 			=> 1
			,'is_slider' 					=> 1
			,'rows' 						=> 1
			,'show_nav' 					=> 1
			,'auto_play' 					=> 1
			,'margin'						=> 30
		), $atts));
		if ( !class_exists('WooCommerce') ){
			return;
		}
		
		if( !$tab_1 && !$tab_2 && !$tab_3 && !$tab_4 && !$tab_5 ){
			return;
		}
		
		if( $tab_style == 'style-2' ){
			if( $item_border ){
				$margin = 0;
			}
			else{
				$margin = 10;
			}
		}
		
		$tabs = array();
		for( $i = 1; $i <= 5; $i++ ){
			if( ${'tab_' . $i} ){
				$tabs[] = array(
					'heading'		=> ${'tab_' . $i . '_heading'}
					,'product_type'	=> ${'tab_' . $i . '_product_type'}
				);
			}
		}
		
		if( $active_tab > count($tabs) ){
			$active_tab = 1;
		}
		
		if( !$product_cats ){
			$show_list_categories = 0;
		}
		
		$product_type = $tabs[$active_tab-1]['product_type'];
		
		$atts = compact('columns', 'rows', 'per_page', 'product_cats', 'include_children', 'product_type'
						,'show_image', 'show_title', 'show_sku', 'show_price', 'show_short_desc', 'show_rating', 'show_label'
						,'show_categories', 'show_add_to_cart', 'is_slider', 'show_nav', 'auto_play', 'margin');
		
		$classes = array();
		$classes[] = 'ts-product-in-product-type-tab-wrapper ts-shortcode ts-product';
		$classes[] = $tab_style;
		if( $tab_style == 'style-1' ){
			$classes[] = $item_line?'item-line':'';
		}
		
		if( $tab_style == 'style-2' ){
			$classes[] = $show_list_categories?'has-list-categories':'';
			$classes[] = $banner_1?'has-banner-1':'';
			$classes[] = $banner_2?'has-banner-2':'';
			$classes[] = $item_border?'item-border':'';
		}
		if( $tab_style == 'style-5' ){
			$has_banner = $banners != '';
			if( $has_banner ){
				$classes[] = 'has-banner';
				$banners = array_map('trim', explode(',', $banners));
				if( count($banners) > 1 ){
					$classes[] = 'banner-slider';
				}
			}
			$classes[] = $item_line?'item-line':'';
		}
		if( $is_slider ){
			$classes[] = 'ts-slider';
			$classes[] = 'rows-'.$rows;
			if( $show_nav ){
				$classes[] = 'nav-middle middle-top';
			}
		}
		
		$classes = array_filter($classes);
		
		$rand_id = 'ts-product-in-product-type-tab-'.mt_rand(0, 1000);
		
		$inline_style = '';
		if( $tab_style == 'style-2' ){
			$selector = '#' . $rand_id;
			$inline_style = '<div class="ts-shortcode-custom-style hidden">';
			$inline_style .= $selector . ' .column-tabs .tabs li.current{background:'.$color.';}';
			$inline_style .= $selector . ' .banner-row ul.categories li a:hover{color:'.$color.';}';
			$inline_style .= $selector . ' .column-tabs:before{border-color:'.$color.';}';
			$inline_style .= '</div>';
		}
		
		if( $tab_style == 'style-4' ){
			$selector = '#' . $rand_id;
			$inline_style = '<div class="ts-shortcode-custom-style hidden">';
			$inline_style .= $selector . ' .column-tabs .heading-tab h3{ background-color:'.$color.';}';
			$inline_style .= $selector . ' .column-tabs .tabs li.current{color:'.$color.';}';
			$inline_style .= $selector . ' .column-tabs .tabs li:hover{color:'.$color.';}';
			$inline_style .= $selector . ' .column-tabs{border-color:'.$color.';}';
			$inline_style .= '</div>';
		}
		
		ob_start();
		echo trim($inline_style);
		?>
		<div class="<?php echo esc_attr(implode(' ', $classes)); ?>" id="<?php echo esc_attr($rand_id) ?>" data-atts="<?php echo htmlentities(json_encode($atts)); ?>">
			<div class="column-tabs">
				<?php if( $title ): ?>
				<header class="heading-tab">
					<h3 class="heading-title"><?php echo esc_html($title); ?></h3>
				</header>
				<?php endif; ?>
				<ul class="tabs">
				<?php
				
				foreach( $tabs as $i => $tab ):
				?>
					<li class="tab-item <?php echo ($active_tab == $i + 1)?'active current':''; ?>" data-product_type="<?php echo esc_attr($tab['product_type']) ?>"><?php echo esc_html($tab['heading']) ?></li>
				<?php
				endforeach;
				?>
				</ul>
			</div>
			
			<div class="column-content">
				<div class="column-products woocommerce columns-<?php echo esc_attr($columns) ?> <?php echo $product_type; ?>">
					<?php echo ts_get_product_content_in_category_tab($atts, $product_cats); ?>
				</div>
				<?php if( $tab_style == 'style-5' && $has_banner ): ?>
				<div class="column-banners loading">
					<figure>
						<?php 
						foreach( $banners as $banner ){
							echo wp_get_attachment_image($banner, 'full');
						}
						?>
					</figure>
				</div>
				<?php endif; ?>
				
				<?php if( $tab_style == 'style-2' && ( $show_list_categories || $banner_1 || $banner_2 ) ): ?>
				<div class="categories-banners">
					<?php if( $show_list_categories || $banner_1 ): ?>
					<div class="banner-row">
						<?php if( $show_list_categories ): ?>
						<ul class="categories">
							<?php 
							$cats = array_map('trim', explode(',', $product_cats));
							foreach( $cats as $cat ):
							$term = get_term_by('term_id', $cat, 'product_cat');
							if( !is_object($term) ){
								continue;
							}
							?>
							<li><a href="<?php echo esc_url(get_term_link($term, 'product_cat')) ?>"><?php echo esc_html($term->name) ?></a></li>
							<?php endforeach; ?>
						</ul>
						<?php endif; ?>
						
						<?php if( $banner_1 ): ?>
						<div class="banner">
							<?php if( $banner_1_link ): ?>
							<a href="<?php echo esc_url($banner_1_link) ?>">
							<?php endif; ?>
								<?php echo wp_get_attachment_image($banner_1, 'full'); ?>
							<?php if( $banner_1_link ): ?>
							</a>
							<?php endif; ?>
						</div>
						<?php endif; ?>
					</div>
					<?php endif; ?>
					<?php if( $banner_2 ): ?>
					<div class="banner-row-2">
						<?php if( $banner_2_link ): ?>
						<a href="<?php echo esc_url($banner_2_link) ?>">
						<?php endif; ?>
							<?php echo wp_get_attachment_image($banner_2, 'full'); ?>
						<?php if( $banner_2_link ): ?>
						</a>
						<?php endif; ?>
					</div>
					<?php endif; ?>
				</div>
				<?php endif; ?>
			</div>
		</div>
		<?php
		
		return ob_get_clean();
	}	
}
add_shortcode('ts_products_in_product_type_tabs', 'ts_products_in_product_type_tabs_shortcode');

/* TS Products Video */
if( !function_exists('ts_products_video_shortcode') ){
	function ts_products_video_shortcode($atts, $content = null){

		extract(shortcode_atts(array(
				'title' 				=> ''
				,'title_style' 			=> ''
				,'product_type'			=> 'recent'
				,'per_page' 			=> 5
				,'product_cats'			=> ''
				,'ids'					=> ''
				,'show_image' 			=> 1
				,'show_title' 			=> 1
				,'show_price' 			=> 1
				,'show_rating' 			=> 1
				,'show_nav'				=> 1
				,'auto_play'			=> 1
			), $atts));

			if ( !class_exists('WooCommerce') ){
				return;
			}
			
			$args = array(
				'post_type'				=> 'product'
				,'post_status' 			=> 'publish'
				,'ignore_sticky_posts'	=> 1
				,'posts_per_page' 		=> absint($per_page)
				,'orderby' 				=> 'date'
				,'order' 				=> 'desc'
				,'meta_query' 			=> WC()->query->get_meta_query()
				,'tax_query'           	=> WC()->query->get_tax_query()
			);
			
			$args['meta_query'][] = array(
										'key'		=> 'ts_prod_video_url'
										,'value'	=> ''
										,'compare'	=> '!='
									);
			
			ts_filter_product_by_product_type($args, $product_type);
			
			$product_cats = str_replace(' ', '', $product_cats);
			if( strlen($product_cats) > 0 ){
				$product_cats = explode(',', $product_cats);
				if( is_array($product_cats) && count($product_cats) > 0 ){
					$args['tax_query'][] = array(
												'taxonomy' => 'product_cat'
												,'terms' => $product_cats
												,'field' => 'term_id'
												,'include_children' => false
											);
				}
			}
			
			$ids = str_replace(' ', '', $ids);
			if( strlen($ids) > 0 ){
				$ids = explode(',', $ids);
				if( is_array($ids) && count($ids) > 0 ){
					$args['post__in'] = $ids;
				}
			}
			
			ob_start();
			global $post;
			$products = new WP_Query( $args );
			
			if( $products->have_posts() ): 
				$classes = array();
				$classes[] = 'ts-products-video-wrapper ts-shortcode ts-product';
				$classes[] = $show_image?'':'no-thumbnail';
				$classes[] = $title_style;
				if( $show_nav ){
					$classes[] = 'show-nav';
				}
				
				$is_slider = $products->post_count > 4?true:false;
				if( $is_slider ){
					$classes[] = 'has-slider';
					wp_enqueue_script( 'jquery-caroufredsel' );
				}
				
				$data_attr = array();
				$data_attr[] = 'data-nav="'.esc_attr($show_nav).'"';
				$data_attr[] = 'data-autoplay="'.esc_attr($auto_play).'"';
				?>
				<div class="<?php echo esc_attr( implode(' ', $classes) ); ?>" <?php echo implode(' ', $data_attr); ?>>
					<?php if( $title ): ?>
						<header class="shortcode-heading-wrapper">
							<h3 class="heading-title"><?php echo esc_html($title); ?></h3>
						</header>
					<?php endif; ?>
					<div class="content-wrapper">
						<div class="content-products <?php echo $is_slider?'loading':''; ?>">
							<ul class="product_list_widget">
							<?php
							$first_video_url = '';
							while( $products->have_posts() ): $products->the_post();
								$product = wc_get_product( $post->ID );
								$product_title = $product->get_title();
								$product_permalink = $product->get_permalink();
								$current_class = '';
								if( !$first_video_url ){
									$first_video_url = get_post_meta($product->get_id(), 'ts_prod_video_url', true);
									$current_class = 'current';
								}
							?>
								<li class="<?php echo $current_class; ?>">
									<?php if( $show_image ): ?>
									<a class="ts-wg-thumbnail" href="<?php echo esc_url( $product_permalink ); ?>" title="<?php echo esc_attr( $product_title ); ?>">
										<?php echo $product->get_image(); ?>
									</a>
									<?php endif; ?>
									<div class="ts-wg-meta">
										<?php if( $show_title ): ?>
										<a href="<?php echo esc_url( $product_permalink ); ?>" title="<?php echo esc_attr( $product_title ); ?>">
											<?php echo esc_html( $product_title ); ?>
										</a>
										<?php 
										endif;
										if( $show_price ){ woocommerce_template_loop_price(); }
										if( $show_rating ){ woocommerce_template_loop_rating(); }
										?>
									</div>
									<a href="#" class="play-button" data-product_id="<?php echo $product->get_id(); ?>"></a>
								</li>
							<?php
							endwhile;
							?>
							</ul>
							<?php if( $show_nav && $is_slider ): ?>
							<div class="navigation">
								<div class="prev-button"></div>
								<div class="next-button"></div>
							</div>
							<?php endif; ?>
						</div>
						<div class="content-video">
							<?php echo do_shortcode('[ts_video src="'.$first_video_url.'"]'); ?>
						</div>
					</div>
				</div>
				<?php
			endif;
			
			wp_reset_postdata();
			
			return '<div class="woocommerce">' . ob_get_clean() . '</div>';
	}
}
add_shortcode('ts_products_video', 'ts_products_video_shortcode');

if( !function_exists('ts_instagram_shop_shortcode') ){
	function ts_instagram_shop_shortcode( $atts ){
		extract(shortcode_atts(array(
			'account_id' 		=> ''
			,'embed_stype'		=> 'grid'
		), $atts));
		if( !$account_id ){
			return;
		}
		$src = 'https://snapppt.com/widgets/widget_loader/' . $account_id . '/' . $embed_stype . '.js';
		return '<script src="'.esc_url($src).'" class="snapppt-widget"></script>';
	}
}
add_shortcode('ts_instagram_shop', 'ts_instagram_shop_shortcode');
?>