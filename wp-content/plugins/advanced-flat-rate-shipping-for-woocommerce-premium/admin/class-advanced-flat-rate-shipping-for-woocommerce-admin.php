<?php
// If this file is called directly, abort.
if ( !defined( 'ABSPATH' ) ) {
    exit;
}
/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://www.multidots.com
 * @since      1.0.0
 *
 * @package    Advanced_Flat_Rate_Shipping_For_WooCommerce_Pro
 * @subpackage Advanced_Flat_Rate_Shipping_For_WooCommerce_Pro/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Advanced_Flat_Rate_Shipping_For_WooCommerce_Pro
 * @subpackage Advanced_Flat_Rate_Shipping_For_WooCommerce_Pro/admin
 * @author     Multidots <inquiry@multidots.in>
 */
class Advanced_Flat_Rate_Shipping_For_WooCommerce_Pro_Admin {

    const afrsm_shipping_post_type = 'wc_afrsm';
    const afrsm_zone_post_type = 'wc_afrsm_zone';
    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string $plugin_name The ID of this plugin.
     */
    private $plugin_name;

    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string $version The current version of this plugin.
     */
    private $version;

    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     * @param      string $plugin_name The name of this plugin.
     * @param      string $version The version of this plugin.
     */
    public function __construct($plugin_name, $version) {
        $this->plugin_name = $plugin_name;
        $this->version = $version;
        $this->afrsm_pro_load_dependencies();
    }

    /**
     * Register the stylesheets for the admin area.
     *
     * @since    1.0.0
     *
     * @param string $hook display current page name
     */
    public function afrsm_pro_enqueue_styles($hook) {
        if (false !== strpos($hook, 'dotstore-plugins_page_afrsm')) {
            wp_enqueue_style($this->plugin_name . 'select2-min', plugin_dir_url(__FILE__) . 'css/select2.min.css', array(), 'all');
            wp_enqueue_style($this->plugin_name . '-jquery-ui-css', plugin_dir_url(__FILE__) . 'css/jquery-ui.min.css', array(), $this->version, 'all');
            wp_enqueue_style($this->plugin_name . '-timepicker-min-css', plugin_dir_url(__FILE__) . 'css/jquery.timepicker.min.css', $this->version, 'all');
            wp_enqueue_style($this->plugin_name . 'font-awesome', plugin_dir_url(__FILE__) . 'css/font-awesome.min.css', array(), $this->version, 'all');
            wp_enqueue_style($this->plugin_name . 'main-style', plugin_dir_url(__FILE__) . 'css/style.css', array(), 'all');
            wp_enqueue_style($this->plugin_name . 'media-css', plugin_dir_url(__FILE__) . 'css/media.css', array(), 'all');
        }
    }

    /**
     * Register the JavaScript for the admin area.
     *
     * @since    1.0.0
     *
     * @param string $hook display current page name
     */
    public function afrsm_pro_enqueue_scripts($hook) {
        global $wp;
        wp_enqueue_style('wp-jquery-ui-dialog');
        wp_enqueue_script('jquery-ui-accordion');
        wp_enqueue_script('jquery-ui-datepicker');
        if (false !== strpos($hook, 'dotstore-plugins_page_afrsm')) {
            wp_enqueue_script($this->plugin_name . '-select2-full-min', plugin_dir_url(__FILE__) . 'js/select2.full.min.js', array('jquery', 'jquery-ui-datepicker'), $this->version, false);
            wp_enqueue_script($this->plugin_name . '-tablesorter-js', plugin_dir_url(__FILE__) . 'js/jquery.tablesorter.js', array('jquery'), $this->version, false);
            wp_enqueue_script($this->plugin_name . '-timepicker-js', plugin_dir_url(__FILE__) . 'js/jquery.timepicker.js', array('jquery'), $this->version, false);
            wp_enqueue_script($this->plugin_name, plugin_dir_url(__FILE__) . 'js/advanced-flat-rate-shipping-for-woocommerce-admin.js', array('jquery', 'jquery-ui-dialog', 'jquery-ui-accordion', 'jquery-ui-sortable', 'select2'), $this->version, false);
            $current_url = home_url(add_query_arg($wp->query_vars, $wp->request));
            wp_localize_script($this->plugin_name, 'coditional_vars', array(
                    'ajaxurl'                           => admin_url( 'admin-ajax.php' ),
                    'ajax_icon'                         => esc_url( plugin_dir_url( __FILE__ ) . '/images/ajax-loader.gif' ),
                    'plugin_url'                        => plugin_dir_url(__FILE__),
                    'dsm_ajax_nonce'                    => wp_create_nonce('dsm_nonce'),
                    'country'                           => esc_html__('Country', 'advanced-flat-rate-shipping-for-woocommerce'),
                    'state'                             => esc_html__('State', 'advanced-flat-rate-shipping-for-woocommerce'),
                    'postcode'                          => esc_html__('Postcode', 'advanced-flat-rate-shipping-for-woocommerce'),
                    'zone'                              => esc_html__('Zone', 'advanced-flat-rate-shipping-for-woocommerce'),
                    'cart_contains_product'             => esc_html__('Cart contains product', 'advanced-flat-rate-shipping-for-woocommerce'),
                    'cart_contains_variable_product'    => esc_html__('Cart contains variable product', 'advanced-flat-rate-shipping-for-woocommerce'),
                    'cart_contains_category_product'    => esc_html__('Cart contains category\'s product', 'advanced-flat-rate-shipping-for-woocommerce'),
                    'cart_contains_tag_product'         => esc_html__('Cart contains tag\'s product', 'advanced-flat-rate-shipping-for-woocommerce'),
                    'cart_contains_sku_product'         => esc_html__('Cart contains SKU\'s product', 'advanced-flat-rate-shipping-for-woocommerce'),
                    'user'                              => esc_html__('User', 'advanced-flat-rate-shipping-for-woocommerce'),
                    'user_role'                         => esc_html__('User Role', 'advanced-flat-rate-shipping-for-woocommerce'),
                    'cart_subtotal_before_discount'     => esc_html__('Cart Subtotal (Before Discount)', 'advanced-flat-rate-shipping-for-woocommerce'),
                    'cart_subtotal_after_discount'      => esc_html__('Cart Subtotal (After Discount)', 'advanced-flat-rate-shipping-for-woocommerce'),
                    'quantity'                          => esc_html__('Quantity', 'advanced-flat-rate-shipping-for-woocommerce'),
                    'weight'                            => esc_html__('Weight', 'advanced-flat-rate-shipping-for-woocommerce'),
                    'coupon'                            => esc_html__('Coupon', 'advanced-flat-rate-shipping-for-woocommerce'),
                    'shipping_class'                    => esc_html__('Shipping Class', 'advanced-flat-rate-shipping-for-woocommerce'),
                    'min_quantity'                      => esc_html__('Min quantity', 'advanced-flat-rate-shipping-for-woocommerce'),
                    'max_quantity'                      => esc_html__('Max quantity', 'advanced-flat-rate-shipping-for-woocommerce'),
                    'amount'                            => esc_html__('Amount', 'advanced-flat-rate-shipping-for-woocommerce'),
                    'equal_to'                          => esc_html__('Equal to ( = )', 'advanced-flat-rate-shipping-for-woocommerce'),
                    'not_equal_to'                      => esc_html__('Not Equal to ( != )', 'advanced-flat-rate-shipping-for-woocommerce'),
                    'less_or_equal_to'                  => esc_html__('Less or Equal to ( <= )', 'woocommerce-conditional-product-fees-for-checkout'),
                    'less_than'                         => esc_html__('Less then ( < )', 'woocommerce-conditional-product-fees-for-checkout'),
                    'greater_or_equal_to'               => esc_html__('greater or Equal to ( >= )', 'woocommerce-conditional-product-fees-for-checkout'),
                    'greater_than'                      => esc_html__('greater then ( > )', 'woocommerce-conditional-product-fees-for-checkout'),
                    'validation_length1'                => esc_html__('Please enter 3 or more characters', 'advanced-flat-rate-shipping-for-woocommerce'),
                    'select_category'                   => esc_html__('Select Category', 'advanced-flat-rate-shipping-for-woocommerce'),
                    'delete'                            => esc_html__('Delete', 'advanced-flat-rate-shipping-for-woocommerce'),
                    'cart_qty'                          => esc_html__('Cart Qty', 'advanced-flat-rate-shipping-for-woocommerce'),
                    'cart_weight'                       => esc_html__('Cart Weight', 'advanced-flat-rate-shipping-for-woocommerce'),
                    'min_weight'                        => esc_html__('Min Weight', 'advanced-flat-rate-shipping-for-woocommerce'),
                    'max_weight'                        => esc_html__('Max Weight', 'advanced-flat-rate-shipping-for-woocommerce'),
                    'cart_subtotal'                     => esc_html__('Cart Subtotal', 'advanced-flat-rate-shipping-for-woocommerce'),
                    'min_subtotal'                      => esc_html__('Min Subtotal', 'advanced-flat-rate-shipping-for-woocommerce'),
                    'max_subtotal'                      => esc_html__('Max Subtotal', 'advanced-flat-rate-shipping-for-woocommerce'),
                    'validation_length2'                => esc_html__('Please enter', 'advanced-flat-rate-shipping-for-woocommerce'),
                    'validation_length3'                => esc_html__('or more characters', 'advanced-flat-rate-shipping-for-woocommerce'),
                    'location_specific'                 => esc_html__('Location Specific', 'advanced-flat-rate-shipping-for-woocommerce'),
                    'product_specific'                  => esc_html__('Product Specific', 'advanced-flat-rate-shipping-for-woocommerce'),
                    'user_specific'                     => esc_html__('User Specific', 'advanced-flat-rate-shipping-for-woocommerce'),
                    'cart_specific'                     => esc_html__('Cart Specific', 'advanced-flat-rate-shipping-for-woocommerce'),
                    'attribute_specific'                => esc_html__('Attribute Specific', 'advanced-flat-rate-shipping-for-woocommerce'),
                    'checkout_specific'                 => esc_html__('Checkout Specific', 'advanced-flat-rate-shipping-for-woocommerce'),
                    'payment_method'                    => esc_html__('Payment Method', 'advanced-flat-rate-shipping-for-woocommerce'),
                    'min_max_qty_error'                 => esc_html__('Max qty should greater then min qty', 'advanced-flat-rate-shipping-for-woocommerce'),
                    'min_max_weight_error'              => esc_html__('Max weight should greater then min weight', 'advanced-flat-rate-shipping-for-woocommerce'),
                    'min_max_subtotal_error'            => esc_html__('Max subtotal should greater then min subtotal', 'advanced-flat-rate-shipping-for-woocommerce'),
                    'success_msg1'                      => esc_html__('Shipping method order saved successfully', 'advanced-flat-rate-shipping-for-woocommerce'),
                    'success_msg2'                      => esc_html__('Your settings successfully saved.', 'advanced-flat-rate-shipping-for-woocommerce'),
                    'warning_msg1'                      => sprintf(__('<p><b style="color: red;">Note: </b>If entered price is more than total shipping price than Message looks like: <b>Shipping Method Name: Curreny Symbole like($) -60.00 Price </b> and if shipping minus price is more than total price than it will set Total Price to Zero(0).</p>', 'advanced-flat-rate-shipping-for-woocommerce')),
                    'warning_msg2'                      => esc_html__('Please disable Advance Pricing Rule if you dont need because you have not created rule there.', 'advanced-flat-rate-shipping-for-woocommerce'),
                    'warning_msg3'                      => esc_html__('You need to select product specific option in Shipping Method Rules for product based option', 'advanced-flat-rate-shipping-for-woocommerce'),
                    'warning_msg4'                      => esc_html__('If you active Apply Per Quantity option then Advance Pricing Rule will be disable and not working.', 'advanced-flat-rate-shipping-for-woocommerce'),
                    'warning_msg5'                      => esc_html__('Please fill some required field in advance pricing rule section', 'advanced-flat-rate-shipping-for-woocommerce'),
                    'attribute_list'                    => wp_json_encode($this->afrsm_pro_attribute_list()),
                    'note'                              => esc_html__('Note: ', 'advanced-flat-rate-shipping-for-woocommerce'),
                    'click_here'                        => esc_html__('Click Here', 'advanced-flat-rate-shipping-for-woocommerce'),
                    'weight_msg'                        => esc_html__('Please make sure that when you add rules in Advanced Pricing > Cost per weight Section It contains in 
                                                                        above entered weight, otherwise it may be not apply proper shipping charges. For more detail please view 
                                                                        our documentation at ', 'advanced-flat-rate-shipping-for-woocommerce'),
                    'cart_contains_product_msg'         => esc_html__('Please make sure that when you add rules in Advanced Pricing > Cost per product Section It contains in 
                                                                        above selected product list, otherwise it may be not apply proper shipping charges. For more detail please view 
                                                                        our documentation at ', 'advanced-flat-rate-shipping-for-woocommerce'),
                    'cart_contains_category_msg'        => esc_html__('Please make sure that when you add rules in Advanced Pricing > Cost per category Section It contains in 
                                                                        above selected category list, otherwise it may be not apply proper shipping charges. For more detail please view 
                                                                        our documentation at ', 'advanced-flat-rate-shipping-for-woocommerce'),
                    'cart_subtotal_after_discount_msg'  => esc_html__('This rule will apply when you would apply coupun in front side. ', 'advanced-flat-rate-shipping-for-woocommerce'),
                    'payment_method_msg'                => esc_html__('This rule will work for Force All Shipping Method in master setting ', 'advanced-flat-rate-shipping-for-woocommerce'),
                    'current_url'                       => $current_url,
                    'doc_url'                           => "https://www.thedotstore.com/docs/plugin/advanced-flat-rate-shipping-method-for-woocommerce/",
                    'list_page_url'                     => add_query_arg(array('page' => 'afrsm-start-page'), admin_url('admin.php'))
                )
            );
        }
    }

    /**
     * Load zone section
     *
     * @since    1.0.0
     */
    private function afrsm_pro_load_dependencies() {
        require_once plugin_dir_path( __FILE__ ) . 'partials/afrsm-pro-shipping-zone-page.php';
    }

    /*
     * Shipping method Pro Menu
     *
     * @since 3.0.0
     */
    public function afrsm_pro_dot_store_menu_shipping_method_pro() {
        global $GLOBALS;
        if (empty($GLOBALS['admin_page_hooks']['dots_store'])) {
            add_menu_page('DotStore Plugins', __('DotStore Plugins'), 'null', 'dots_store', array($this, 'dot_store_menu_page'), AFRSM_PRO_PLUGIN_URL . 'admin/images/menu-icon.png', 25);
        }

        add_submenu_page('dots_store', 'Advanced Flat Rate Shipping For WooCommerce Pro', 'Advanced Flat Rate Shipping For WooCommerce Pro', 'manage_options', 'afrsm-pro-list', array($this, 'afrsm_pro_fee_list_page'));
        add_submenu_page('dots_store', 'Add Shipping Method', 'Add Shipping Method', 'manage_options', 'afrsm-pro-add-shipping', array($this, 'afrsm_pro_add_new_fee_page'));
        add_submenu_page('dots_store', 'Edit Shipping Method', 'Edit Shipping Method', 'manage_options', 'afrsm-pro-edit-shipping', array($this, 'afrsm_pro_edit_fee_page'));
        add_submenu_page('dots_store', 'Manage Shipping Zones', 'Manage Shipping Zones', 'manage_options', 'afrsm-wc-shipping-zones', array(__CLASS__, 'afrsm_pro_shipping_zone_page'));
        add_submenu_page('dots_store', 'Getting Started', 'Getting Started', 'manage_options', 'afrsm-pro-get-started', array($this, 'afrsm_pro_get_started_page'));
        add_submenu_page('dots_store', 'Quick info', 'Quick info', 'manage_options', 'afrsm-pro-information', array($this, 'afrsm_pro_information_page'));
    }

    /**
     * Shipping List Page
     *
     * @since    1.0.0
     */
    public function afrsm_pro_fee_list_page() {
        require_once(plugin_dir_path( __FILE__ ).'partials/afrsm-pro-list-page.php');
    }

    /**
     * Add new shipping method Page
     *
     * @since    1.0.0
     */
    public function afrsm_pro_add_new_fee_page() {
        require_once(plugin_dir_path( __FILE__ ).'partials/afrsm-pro-add-new-page.php');
    }

    /**
     * Edit shipping method Page
     *
     * @since    1.0.0
     */
    public function afrsm_pro_edit_fee_page() {
        require_once(plugin_dir_path( __FILE__ ).'partials/afrsm-pro-add-new-page.php');
    }

    /**
     * Shipping zone page
     * @uses  AFRSM_Shipping_Zone class
     * @uses AFRSM_Shipping_Zone::output()
     *
     * @since    1.0.0
     */
    public static function afrsm_pro_shipping_zone_page() {
        $shipping_zone_obj = new AFRSM_Shipping_Zone();
        $shipping_zone_obj->afrsm_pro_sz_output();
    }

    /**
     * Quick guide page
     *
     * @since    1.0.0
     */
    public function afrsm_pro_get_started_page() {
        require_once(plugin_dir_path( __FILE__ ).'partials/afrsm-pro-get-started-page.php');
    }

    /**
     * Plugin information page
     *
     * @since    1.0.0
     */
    public function afrsm_pro_information_page() {
        require_once(plugin_dir_path( __FILE__ ).'partials/afrsm-pro-information-page.php');
    }

    /**
     * Redirect to shipping list page
     *
     * @since    1.0.0
     */
    public function afrsm_pro_redirect_shipping_function() {
        $get_section = filter_input(INPUT_GET,'section',FILTER_SANITIZE_STRING);
        if ((isset($get_section) && !empty($get_section)) && 'advanced_flat_rate_shipping' === $get_section) {
            wp_safe_redirect(add_query_arg(array('page' => 'afrsm-pro-list'), admin_url('admin.php')));
            exit;
        }
    }

    /**
     * Redirect to quick start guide after plugin activation
     *
     * @uses afrsm_pro_register_post_type()
     *
     * @since    1.0.0
     */
    public function afrsm_pro_welcome_shipping_method_screen_do_activation_redirect() {
        $this->afrsm_pro_register_post_type();

        // if no activation redirect
        if (!get_transient('_welcome_screen_afrsm_pro_mode_activation_redirect_data')) {
            return;
        }

        // Delete the redirect transient
        delete_transient('_welcome_screen_afrsm_pro_mode_activation_redirect_data');

        // if activating from network, or bulk
        $activate_multi = filter_input(INPUT_GET,'activate-multi',FILTER_SANITIZE_STRING);
        if (is_network_admin() || isset($activate_multi)) {
            return;
        }
        // Redirect to extra cost welcome  page
        wp_safe_redirect(add_query_arg(array('page' => 'afrsm-pro-get-started'), admin_url('admin.php')));
        exit;
    }


    /**
     * Register post type
     *
     * @since    1.0.0
     */
    public function afrsm_pro_register_post_type() {
        register_post_type( self::afrsm_shipping_post_type, array(
            'labels'          => array(
                'name'          => __( 'Advance Shipping Method', 'advanced-flat-rate-shipping-for-woocommerce' ),
                'singular_name' => __( 'Advance Shipping Method', 'advanced-flat-rate-shipping-for-woocommerce' ),
            ),
        ) );

        register_post_type( self::afrsm_zone_post_type, array(
            'labels' => array(
                'name' => __( 'Advance Shipping Zone', 'advanced-flat-rate-shipping-for-woocommerce' ),
                'singular_name' => __( 'Advance Shipping Zone', 'advanced-flat-rate-shipping-for-woocommerce' ),
            ),
        ) );
    }

    /**
     * Remove submenu from admin screeen
     *
     * @since    1.0.0
     */
    public function afrsm_pro_remove_admin_submenus() {
        remove_submenu_page('dots_store', 'afrsm-pro-add-shipping');
        remove_submenu_page('dots_store', 'afrsm-pro-edit-shipping');
        remove_submenu_page('dots_store', 'afrsm-wc-shipping-zones');
        remove_submenu_page('dots_store', 'afrsm-pro-get-started');
        remove_submenu_page('dots_store', 'afrsm-pro-information');
    }

    /**
     * Match condition based on shipping list
     *
     * @since    1.0.0
     *
     * @uses afrsm_pro_get_default_langugae_with_sitpress()
     * @uses afrsm_pro_get_woo_version_number()
     * @uses WC_Cart::get_cart()
     * @uses afrsm_pro_fee_array_column_admin()
     * @uses afrsm_pro_match_product_per_qty()
     * @uses afrsm_pro_match_category_per_qty()
     * @uses afrsm_pro_match_total_cart_qty()
     * @uses afrsm_pro_match_product_per_weight()
     * @uses afrsm_pro_match_category_per_weight()
     * @uses afrsm_pro_match_total_cart_weight()
     * @uses afrsm_pro_match_country_rules()
     * @uses afrsm_pro_match_state_rules()
     * @uses afrsm_pro_match_postcode_rules()
     * @uses afrsm_pro_match_zone_rules()
     * @uses afrsm_pro_match_variable_products_rule()
     * @uses afrsm_pro_match_simple_products_rule()
     * @uses afrsm_pro_match_category_rule()
     * @uses afrsm_pro_match_tag_rule()
     * @uses afrsm_pro_match_sku_rule()
     * @uses afrsm_pro_match_user_rule()
     * @uses afrsm_pro_match_user_role_rule()
     * @uses afrsm_pro_match_coupon_rule()
     * @uses afrsm_pro_match_cart_subtotal_before_discount_rule()
     * @uses afrsm_pro_match_cart_subtotal_after_discount_rule()
     * @uses afrsm_pro_match_cart_total_cart_qty_rule()
     * @uses afrsm_pro_match_cart_total_weight_rule()
     * @uses afrsm_pro_match_shipping_class_rule()
     *
     * @param int $sm_post_id
     * @param array|object $package
     *
     * @return bool True if $final_condition_flag is 1, false otherwise. if $sm_status is off then also return false.
     */
    public function afrsm_pro_condition_match_rules($sm_post_id, $package = array()) {

        if (empty($sm_post_id)) {
            return false;
        }

        global $sitepress;

        $default_lang = $this->afrsm_pro_get_default_langugae_with_sitpress();

        if (!empty($sitepress)) {
            $sm_post_id = apply_filters('wpml_object_id', $sm_post_id, 'wc_afrsm', TRUE, $default_lang);
        } else {
            $sm_post_id = $sm_post_id;
        }

        $wc_curr_version = $this->afrsm_pro_get_woo_version_number();

        $is_passed = array();
        $final_is_passed_general_rule = array();
        $new_is_passed = array();
        $final_condition_flag = array();

        $cart_array = $this->afrsm_pro_get_cart();
        $cart_main_product_ids_array = $this->afrsm_pro_get_main_prd_id($sitepress, $default_lang);
        $cart_product_ids_array = $this->afrsm_pro_get_prd_var_id($sitepress, $default_lang);
        $variation_cart_products_array = $this->afrsm_pro_get_var_name($sitepress, $default_lang);

        $sm_status = get_post_status($sm_post_id);
        $sm_start_date = get_post_meta($sm_post_id, 'sm_start_date', true);
        $sm_end_date = get_post_meta($sm_post_id, 'sm_end_date', true);

        $sm_time_from = get_post_meta($sm_post_id, 'sm_time_from', true);
        $sm_time_to = get_post_meta($sm_post_id, 'sm_time_to', true);
        $sm_select_day_of_week = get_post_meta($sm_post_id, 'sm_select_day_of_week', true);

        $get_condition_array = get_post_meta($sm_post_id, 'sm_metabox', true);

        $cost_rule_match       = get_post_meta( $sm_post_id, 'cost_rule_match', true );
        if (!empty($cost_rule_match)) {
            if (is_serialized($cost_rule_match)) {
                $cost_rule_match = maybe_unserialize($cost_rule_match);
            } else {
                $cost_rule_match = $cost_rule_match;
            }

            if (array_key_exists('general_rule_match', $cost_rule_match)) {
                $general_rule_match = $cost_rule_match['general_rule_match'];
            } else {
                $general_rule_match = 'all';
            }
        } else {
            $general_rule_match = 'all';
        }

        if (isset($sm_status) && 'off' === $sm_status) {
            return false;
        }

        if (!empty($get_condition_array) ||  '' !== $get_condition_array || null !== $get_condition_array) {

            $country_array = array();
            $state_array = array();
            $postcode_array = array();
            $zone_array = array();
            $product_array = array();
            $variableproduct_array = array();
            $category_array = array();
            $tag_array = array();
            $sku_array = array();
            $user_array = array();
            $user_role_array = array();
            $cart_total_array = array();
            $cart_totalafter_array = array();
            $quantity_array = array();
            $weight_array = array();
            $coupon_array = array();
            $shipping_class_array = array();
            $payment_methods_array = array();
            $attribute_taxonomies = wc_get_attribute_taxonomies();
            $atta_name = array();

            foreach ($get_condition_array as $key => $value) {
                if (array_search('country', $value, true)) {
                    $country_array[$key] = $value;
                }
                if (array_search('state', $value, true)) {
                    $state_array[$key] = $value;
                }
                if (array_search('postcode', $value, true)) {
                    $postcode_array[$key] = $value;
                }
                if (array_search('zone', $value, true)) {
                    $zone_array[$key] = $value;
                }
                if (array_search('product', $value, true)) {
                    $product_array[$key] = $value;
                }
                if (array_search('variableproduct', $value, true)) {
                    $variableproduct_array[$key] = $value;
                }
                if (array_search('category', $value, true)) {
                    $category_array[$key] = $value;
                }
                if (array_search('tag', $value, true)) {
                    $tag_array[$key] = $value;
                }
                if (array_search('sku', $value, true)) {
                    $sku_array[$key] = $value;
                }
                if ($attribute_taxonomies) {
                    foreach ($attribute_taxonomies as $attribute) {
                        $att_name = wc_attribute_taxonomy_name($attribute->attribute_name);
                        if (array_search($att_name, $value, true)) {
                            $atta_name['att_' . $att_name] = $value;
                        }
                    }
                }
                if (array_search('user', $value, true)) {
                    $user_array[$key] = $value;
                }
                if (array_search('user_role', $value, true)) {
                    $user_role_array[$key] = $value;
                }
                if (array_search('cart_total', $value, true)) {
                    $cart_total_array[$key] = $value;
                }
                if (array_search('cart_totalafter', $value, true)) {
                    $cart_totalafter_array[$key] = $value;
                }
                if (array_search('quantity', $value, true)) {
                    $quantity_array[$key] = $value;
                }
                if (array_search('weight', $value, true)) {
                    $weight_array[$key] = $value;
                }
                if (array_search('coupon', $value, true)) {
                    $coupon_array[$key] = $value;
                }
                if (array_search('shipping_class', $value, true)) {
                    $shipping_class_array[$key] = $value;
                }
                if (array_search('payment_method', $value, true)) {
                    $payment_methods_array[$key] = $value;
                }

                //Check if is country exist
                if (is_array($country_array) && isset($country_array) && !empty($country_array) && !empty($cart_product_ids_array)) {
                    $country_passed = $this->afrsm_pro_match_country_rules($country_array, $general_rule_match);
                    if ('yes' === $country_passed) {
                        $is_passed['has_fee_based_on_country'] = 'yes';
                    } else {
                        $is_passed['has_fee_based_on_country'] = 'no';
                    }
                }

                //Check if is state exist
                if (is_array($state_array) && isset($state_array) && !empty($state_array) && !empty($cart_product_ids_array)) {
                    $state_passed = $this->afrsm_pro_match_state_rules($state_array, $general_rule_match);
                    if ('yes' === $state_passed) {
                        $is_passed['has_fee_based_on_state'] = 'yes';
                    } else {
                        $is_passed['has_fee_based_on_state'] = 'no';
                    }
                }

                //Check if is postcode exist
                if (is_array($postcode_array) && isset($postcode_array) && !empty($postcode_array) && !empty($cart_product_ids_array)) {
                    $postcode_passed = $this->afrsm_pro_match_postcode_rules($postcode_array, $general_rule_match);
                    if ('yes' === $postcode_passed) {
                        $is_passed['has_fee_based_on_postcode'] = 'yes';
                    } else {
                        $is_passed['has_fee_based_on_postcode'] = 'no';
                    }
                }

                //Check if is zone exist
                if (is_array($zone_array) && isset($zone_array) && !empty($zone_array) && !empty($cart_product_ids_array)) {
                    $zone_passed = $this->afrsm_pro_match_zone_rules($zone_array, $package, $general_rule_match);
                    if ('yes' === $zone_passed) {
                        $is_passed['has_fee_based_on_zone'] = 'yes';
                    } else {
                        $is_passed['has_fee_based_on_zone'] = 'no';
                    }
                }

                //Check if is variable product exist
                if (is_array($variableproduct_array) && isset($variableproduct_array) && !empty($variableproduct_array) && !empty($cart_product_ids_array)) {
                    $variable_prd_passed = $this->afrsm_pro_match_variable_products_rule($cart_product_ids_array, $variableproduct_array, $general_rule_match);
                    if ('yes' === $variable_prd_passed) {
                        $is_passed['has_fee_based_on_variable_prd'] = 'yes';
                    } else {
                        $is_passed['has_fee_based_on_variable_prd'] = 'no';
                    }
                }

                //Check if is product exist
                if (is_array($product_array) && isset($product_array) && !empty($product_array) && !empty($cart_product_ids_array)) {
                    $product_passed = $this->afrsm_pro_match_simple_products_rule($cart_product_ids_array, $product_array, $general_rule_match);
                    if ('yes' === $product_passed) {
                        $is_passed['has_fee_based_on_product'] = 'yes';
                    } else {
                        $is_passed['has_fee_based_on_product'] = 'no';
                    }
                }

                //Check if is Category exist
                if (is_array($category_array) && isset($category_array) && !empty($category_array) && !empty($cart_main_product_ids_array)) {
                    $category_passed = $this->afrsm_pro_match_category_rule($cart_main_product_ids_array, $category_array, $general_rule_match);
                    if ('yes' === $category_passed) {
                        $is_passed['has_fee_based_on_category'] = 'yes';
                    } else {
                        $is_passed['has_fee_based_on_category'] = 'no';
                    }
                }

                //Check if is tag exist
                if (is_array($tag_array) && isset($tag_array) && !empty($tag_array) && !empty($cart_main_product_ids_array)) {
                    $tag_passed = $this->afrsm_pro_match_tag_rule($cart_main_product_ids_array, $tag_array, $general_rule_match);
                    if ('yes' === $tag_passed) {
                        $is_passed['has_fee_based_on_tag'] = 'yes';
                    } else {
                        $is_passed['has_fee_based_on_tag'] = 'no';
                    }
                }

                //Check if sku exist
                if (is_array($sku_array) && isset($sku_array) && !empty($sku_array) && !empty($cart_product_ids_array)) {
                    $sku_passed = $this->afrsm_pro_match_sku_rule($cart_product_ids_array, $sku_array, $general_rule_match);
                    if ('yes' === $sku_passed) {
                        $is_passed['has_fee_based_on_sku'] = 'yes';
                    } else {
                        $is_passed['has_fee_based_on_sku'] = 'no';
                    }
                }

                if (!empty($attribute_taxonomies)) {
                    if (is_array($atta_name) && isset($atta_name) && !empty($atta_name) && !empty($cart_product_ids_array)) {
                        $attribute_passed = $this->afrsm_pro_match_attribute_rule($variation_cart_products_array, $atta_name, $general_rule_match);
                        if ('yes' === $attribute_passed) {
                            $is_passed['has_fee_based_on_product_att'] = 'yes';
                        } else {
                            $is_passed['has_fee_based_on_product_att'] = 'no';
                        }
                    }
                }

                //Check if is user exist
                if (is_array($user_array) && isset($user_array) && !empty($user_array) && !empty($cart_product_ids_array)) {
                    $user_passed = $this->afrsm_pro_match_user_rule($user_array, $general_rule_match);
                    if ('yes' === $user_passed) {
                        $is_passed['has_fee_based_on_user'] = 'yes';
                    }else {
                        $is_passed['has_fee_based_on_user'] = 'no';
                    }
                }

                //Check if is user role exist
                if (is_array($user_role_array) && isset($user_role_array) && !empty($user_role_array) && !empty($cart_product_ids_array)) {
                    $user_role_passed = $this->afrsm_pro_match_user_role_rule($user_role_array, $general_rule_match);
                    if ('yes' === $user_role_passed) {
                        $is_passed['has_fee_based_on_user_role'] = 'yes';
                    } else {
                        $is_passed['has_fee_based_on_user_role'] = 'no';
                    }
                }

                //Check if is coupon exist
                if (is_array($coupon_array) && isset($coupon_array) && !empty($coupon_array) && !empty($cart_product_ids_array)) {
                    $coupon_passed = $this->afrsm_pro_match_coupon_rule($wc_curr_version, $coupon_array, $general_rule_match);
                    if ('yes' === $coupon_passed) {
                        $is_passed['has_fee_based_on_coupon'] = 'yes';
                    } else {
                        $is_passed['has_fee_based_on_coupon'] = 'no';
                    }
                }

                //Check if is Cart Subtotal (Before Discount) exist
                if (is_array($cart_total_array) && isset($cart_total_array) && !empty($cart_total_array) && !empty($cart_product_ids_array)) {
                    $cart_total_before_passed = $this->afrsm_pro_match_cart_subtotal_before_discount_rule($wc_curr_version, $cart_total_array, $general_rule_match);
                    if ('yes' === $cart_total_before_passed) {
                        $is_passed['has_fee_based_on_cart_total_before'] = 'yes';
                    } else {
                        $is_passed['has_fee_based_on_cart_total_before'] = 'no';
                    }
                }

                //Check if is Cart Subtotal (After Discount) exist
                if (is_array($cart_totalafter_array) && isset($cart_totalafter_array) && !empty($cart_totalafter_array) && !empty($cart_product_ids_array)) {
                    $cart_total_after_passed = $this->afrsm_pro_match_cart_subtotal_after_discount_rule($wc_curr_version, $cart_totalafter_array, $general_rule_match);
                    if ('yes' === $cart_total_after_passed) {
                        $is_passed['has_fee_based_on_cart_total_after'] = 'yes';
                    } else {
                        $is_passed['has_fee_based_on_cart_total_after'] = 'no';
                    }
                }

                //Check if is quantity exist
                if (is_array($quantity_array) && isset($quantity_array) && !empty($quantity_array) && !empty($cart_product_ids_array)) {
                    $quantity_passed = $this->afrsm_pro_match_cart_total_cart_qty_rule($cart_array, $quantity_array, $general_rule_match);
                    if ('yes' === $quantity_passed) {
                        $is_passed['has_fee_based_on_quantity'] = 'yes';
                    } else {
                        $is_passed['has_fee_based_on_quantity'] = 'no';
                    }
                }

                //Check if is weight exist
                if (is_array($weight_array) && isset($weight_array) && !empty($weight_array) && !empty($cart_product_ids_array)) {
                    $weight_passed = $this->afrsm_pro_match_cart_total_weight_rule($cart_array, $weight_array, $general_rule_match);
                    if ('yes' === $weight_passed) {
                        $is_passed['has_fee_based_on_weight'] = 'yes';
                    } else {
                        $is_passed['has_fee_based_on_weight'] = 'no';
                    }
                }

                //Check if is shipping class exist
                if (is_array($shipping_class_array) && isset($shipping_class_array) && !empty($shipping_class_array) && !empty($cart_product_ids_array)) {
                    $shipping_class_passed = $this->afrsm_pro_match_shipping_class_rule( $cart_product_ids_array , $shipping_class_array , $general_rule_match);
                    if ('yes' === $shipping_class_passed) {
                        $is_passed['has_fee_based_on_shipping_class'] = 'yes';
                    } else {
                        $is_passed['has_fee_based_on_shipping_class'] = 'no';
                    }
                }

                if (is_array($payment_methods_array) && isset($payment_methods_array) && !empty($payment_methods_array) ) {
                    $payment_methods_passed = $this->afrsm_pro_match_payment_gateway_rule( $payment_methods_array , $general_rule_match);
                    if ('yes' === $payment_methods_passed) {
                        $is_passed['has_fee_based_on_payment'] = 'yes';
                    } else {
                        $is_passed['has_fee_based_on_payment'] = 'no';
                    }
                }
            }

            if (isset($is_passed) && !empty($is_passed) && is_array($is_passed)) {
                $fnispassed = array();
                foreach ($is_passed as $val) {
                    if ('' !== $val) {
                        $fnispassed[] = $val;
                    }
                }
                if ('all' === $general_rule_match) {
                    if (in_array('no', $fnispassed, true)) {
                        $final_is_passed_general_rule['passed'] = 'no';
                    } else {
                        $final_is_passed_general_rule['passed'] = 'yes';
                    }
                } else {
                    if (in_array('yes', $fnispassed, true)) {
                        $final_is_passed_general_rule['passed'] = 'yes';
                    } else {
                        $final_is_passed_general_rule['passed'] = 'no';
                    }
                }
            }
        }

        if (empty($final_is_passed_general_rule) || '' === $final_is_passed_general_rule || null === $final_is_passed_general_rule) {
            $new_is_passed['passed'] = 'no';
        } else if (!empty($final_is_passed_general_rule) && in_array('no', $final_is_passed_general_rule, true)) {
            $new_is_passed['passed'] = 'no';
        } else if (empty($final_is_passed_general_rule) && in_array('', $final_is_passed_general_rule, true)) {
            $new_is_passed['passed'] = 'no';
        } else if (!empty($final_is_passed_general_rule) && in_array('yes', $final_is_passed_general_rule, true)) {
            $new_is_passed['passed'] = 'yes';
        }

        if (isset($new_is_passed) && !empty($new_is_passed) && is_array($new_is_passed)) {
            if (!in_array('no', $new_is_passed, true)) {
                $current_date = strtotime(date('d-m-Y'));
                $sm_start_date = (isset($sm_start_date) && !empty($sm_start_date)) ? strtotime($sm_start_date) : '';
                $sm_end_date = (isset($sm_end_date) && !empty($sm_end_date)) ? strtotime($sm_end_date) : '';
                /*Check for date*/
                if (($current_date >= $sm_start_date || '' === $sm_start_date) && ($current_date <= $sm_end_date || '' === $sm_end_date)) {
                    $final_condition_flag['date'] = 'yes';
                } else {
                    $final_condition_flag['date'] = 'no';
                }

                /*Check for time*/
                $local_nowtimestamp = current_time('timestamp') ;
                $sm_time_from   = (isset($sm_time_from) && !empty($sm_time_from)) ? strtotime($sm_time_from) : '';
                $sm_time_to     = (isset($sm_time_to) && !empty($sm_time_to)) ? strtotime($sm_time_to) : '';

                if (($local_nowtimestamp >= $sm_time_from || '' === $sm_time_from) && ($local_nowtimestamp <= $sm_time_to || '' === $sm_time_to)) {
                    $final_condition_flag['time'] = 'yes';
                } else {
                    $final_condition_flag['time'] = 'no';
                }

                /*Check for day*/
                $today = strtolower(date("D"));
                if(!empty($sm_select_day_of_week)) {
                    if( in_array( $today, $sm_select_day_of_week, true ) ||  '' === $sm_select_day_of_week ){
                        $final_condition_flag['day'] = 'yes';
                    }else{
                        $final_condition_flag['day'] = 'no';
                    }
                }
            } else {
                $final_condition_flag[] = 'no';
            }
        }

        if (empty($final_condition_flag) && $final_condition_flag === '') {
            return false;
        } else if (!empty($final_condition_flag) && in_array('no', $final_condition_flag, true)) {
            return false;
        } else if (empty($final_condition_flag) && in_array('', $final_condition_flag, true)) {
            return false;
        } else if (!empty($final_condition_flag) && in_array('yes', $final_condition_flag, true)) {
            return true;
        }
    }

    /**
     * Match country rules
     *
     * @since    3.4
     *
     * @uses WC_Customer::get_shipping_country()
     *
     * @param array $country_array
     * @param string $general_rule_match
     *
     * @return string $main_is_passed
     *
     */
    public function afrsm_pro_match_country_rules($country_array, $general_rule_match) {
        $selected_country = WC()->customer->get_shipping_country();
        $is_passed = array();
        foreach ($country_array as $key => $country) {
            if ('is_equal_to' === $country['product_fees_conditions_is']) {
                if (!empty($country['product_fees_conditions_values'])) {
                    if (in_array($selected_country, $country['product_fees_conditions_values'], true)) {
                        $is_passed[$key]['has_fee_based_on_country'] = 'yes';
                    } else {
                        $is_passed[$key]['has_fee_based_on_country'] = 'no';
                    }
                }
                if (empty($country['product_fees_conditions_values'])) {
                    $is_passed[$key]['has_fee_based_on_country'] = 'yes';
                }
            }
            if ('not_in' === $country['product_fees_conditions_is']) {
                if (!empty($country['product_fees_conditions_values'])) {
                    if (in_array($selected_country, $country['product_fees_conditions_values'], true)
                        || in_array('all', $country['product_fees_conditions_values'], true)) {
                        $is_passed[$key]['has_fee_based_on_country'] = 'no';
                    } else {
                        $is_passed[$key]['has_fee_based_on_country'] = 'yes';
                    }
                }
            }
        }

        $main_is_passed = $this->afrsm_pro_check_all_passed_general_rule($is_passed, 'has_fee_based_on_country', $general_rule_match);
        return $main_is_passed;
    }

    /**
     * Match state rules
     *
     * @since    3.4
     *
     * @uses WC_Customer::get_shipping_country()
     * @uses WC_Customer::get_shipping_state()
     *
     * @param array $state_array
     * @param string $general_rule_match
     *
     * @return string $main_is_passed
     *
     */
    public function afrsm_pro_match_state_rules($state_array, $general_rule_match) {
        $country = WC()->customer->get_shipping_country();
        $state = WC()->customer->get_shipping_state();
        $selected_state = $country . ':' . $state;
        $is_passed = array();
        foreach ($state_array as $key => $get_state) {
            if ('is_equal_to' === $get_state['product_fees_conditions_is']) {
                if (!empty($get_state['product_fees_conditions_values'])) {
                    if (in_array($selected_state, $get_state['product_fees_conditions_values'], true)) {
                        $is_passed[$key]['has_fee_based_on_state'] = 'yes';
                    } else {
                        $is_passed[$key]['has_fee_based_on_state'] = 'no';
                    }
                }
            }
            if ('not_in' === $get_state['product_fees_conditions_is']) {
                if (!empty($get_state['product_fees_conditions_values'])) {
                    if (in_array($selected_state, $get_state['product_fees_conditions_values'], true)) {
                        $is_passed[$key]['has_fee_based_on_state'] = 'no';
                    } else {
                        $is_passed[$key]['has_fee_based_on_state'] = 'yes';
                    }
                }
            }
        }

        $main_is_passed = $this->afrsm_pro_check_all_passed_general_rule($is_passed, 'has_fee_based_on_state', $general_rule_match);
        return $main_is_passed;
    }

    /**
     * Match postcode rules
     *
     * @since    3.4
     *
     * @uses WC_Customer::get_shipping_postcode()
     *
     * @param array $postcode_array
     * @param string $general_rule_match
     *
     * @return string $main_is_passed
     */
    public function afrsm_pro_match_postcode_rules($postcode_array, $general_rule_match) {
        $selected_postcode = WC()->customer->get_shipping_postcode();
        $is_passed = array();

        foreach ($postcode_array as $key => $postcode) {
            if ('is_equal_to' === $postcode['product_fees_conditions_is']) {
                if (!empty($postcode['product_fees_conditions_values'])) {
                    $postcodestr = str_replace(PHP_EOL, "<br/>", $postcode['product_fees_conditions_values']);
                    $postcode_val_array = explode('<br/>', $postcodestr);
                    if (in_array($selected_postcode, $postcode_val_array, true)) {
                        $is_passed[$key]['has_fee_based_on_postcode'] = 'yes';
                    } else {
                        $is_passed[$key]['has_fee_based_on_postcode'] = 'no';
                    }
                }
            }
            if ('not_in' === $postcode['product_fees_conditions_is']) {
                if (!empty($postcode['product_fees_conditions_values'])) {
                    $postcodestr = str_replace(PHP_EOL, "<br/>", $postcode['product_fees_conditions_values']);
                    $postcode_val_array = explode('<br/>', $postcodestr);
                    if (in_array($selected_postcode, $postcode_val_array, true)) {
                        $is_passed[$key]['has_fee_based_on_postcode'] = 'no';
                    } else {
                        $is_passed[$key]['has_fee_based_on_postcode'] = 'yes';
                    }
                }
            }
        }

        $main_is_passed = $this->afrsm_pro_check_all_passed_general_rule($is_passed, 'has_fee_based_on_postcode', $general_rule_match);
        return $main_is_passed;
    }

    /**
     * Match zone rules
     *
     * @since    3.4
     *
     * @uses afrsm_pro_check_zone_available()
     *
     * @param array $zone_array
     * @param array|object $package
     * @param $general_rule_match
     *
     * @return string $main_is_passed
     */
    public function afrsm_pro_match_zone_rules($zone_array, $package, $general_rule_match) {
        $is_passed = array();
        foreach ($zone_array as $key => $zone) {
            if ('is_equal_to' === $zone['product_fees_conditions_is']) {
                if (!empty($zone['product_fees_conditions_values'])) {
                    $get_zonelist = $this->afrsm_pro_check_zone_available($package, $zone['product_fees_conditions_values']);
                    if (in_array($get_zonelist, $zone['product_fees_conditions_values'], true)) {
                        $is_passed[$key]['has_fee_based_on_zone'] = 'yes';
                    } else {
                        $is_passed[$key]['has_fee_based_on_zone'] = 'no';
                    }
                }
            }
            if ('not_in' === $zone['product_fees_conditions_is']) {
                if (!empty($zone['product_fees_conditions_values'])) {
                    $get_zonelist = $this->afrsm_pro_check_zone_available($package, $zone['product_fees_conditions_values']);
                    if (in_array($get_zonelist, $zone['product_fees_conditions_values'], true)) {
                        $is_passed[$key]['has_fee_based_on_zone'] = 'no';
                    } else {
                        $is_passed[$key]['has_fee_based_on_zone'] = 'yes';
                    }
                }
            }
        }

        $main_is_passed = $this->afrsm_pro_check_all_passed_general_rule($is_passed, 'has_fee_based_on_zone', $general_rule_match);
        return $main_is_passed;
    }

    /**
     * Match variable products rules
     *
     * @since    3.4
     *
     * @uses afrsm_pro_fee_array_column_admin()
     *
     * @param array $cart_product_ids_array
     * @param array $variableproduct_array
     * @param string $general_rule_match
     *
     * @return string $main_is_passed
     */
    public function afrsm_pro_match_variable_products_rule($cart_product_ids_array, $variableproduct_array, $general_rule_match) {
        $is_passed = array();
        $passed_product = array();
        foreach ($variableproduct_array as $key => $product) {
            if ('is_equal_to' === $product['product_fees_conditions_is']) {
                if (!empty($product['product_fees_conditions_values'])) {
                    foreach ($product['product_fees_conditions_values'] as $product_id) {
                        settype($product_id, 'integer');
                        $passed_product[] = $product_id;
                        if (in_array($product_id, $cart_product_ids_array, true)) {
                            $is_passed[$key]['has_fee_based_on_product'] = 'yes';
                            break;
                        } else {
                            $is_passed[$key]['has_fee_based_on_product'] = 'no';
                        }
                    }
                }
            }
            if ('not_in' === $product['product_fees_conditions_is']) {
                if (!empty($product['product_fees_conditions_values'])) {
                    foreach ($product['product_fees_conditions_values'] as $product_id) {
                        settype($product_id, 'integer');
                        if (in_array($product_id, $cart_product_ids_array, true)) {
                            $is_passed[$key]['has_fee_based_on_product'] = 'no';
                            break;
                        } else {
                            $is_passed[$key]['has_fee_based_on_product'] = 'yes';
                        }
                    }
                }
            }
        }

        $main_is_passed = $this->afrsm_pro_check_all_passed_general_rule($is_passed, 'has_fee_based_on_product', $general_rule_match);
        return $main_is_passed;
    }

    /**
     * Match simple products rules
     *
     * @since    3.4
     *
     * @uses afrsm_pro_fee_array_column_admin()
     *
     * @param array $cart_product_ids_array
     * @param array $product_array
     * @param string $general_rule_match
     *
     * @return string $main_is_passed
     */
    public function afrsm_pro_match_simple_products_rule($cart_product_ids_array, $product_array, $general_rule_match) {
        $is_passed = array();
        foreach ($product_array as $key => $product) {
            if ('is_equal_to' === $product['product_fees_conditions_is']) {
                if (!empty($product['product_fees_conditions_values'])) {
                    foreach ($product['product_fees_conditions_values'] as $product_id) {
                        settype($product_id, 'integer');
                        if (in_array($product_id, $cart_product_ids_array, true)) {
                            $is_passed[$key]['has_fee_based_on_product'] = 'yes';
                            break;
                        } else {
                            $is_passed[$key]['has_fee_based_on_product'] = 'no';
                        }
                    }
                }
            }
            if ('not_in' === $product['product_fees_conditions_is']) {
                if (!empty($product['product_fees_conditions_values'])) {
                    foreach ($product['product_fees_conditions_values'] as $product_id) {
                        settype($product_id, 'integer');
                        if (in_array($product_id, $cart_product_ids_array, true)) {
                            $is_passed[$key]['has_fee_based_on_product'] = 'no';
                            break;
                        } else {
                            $is_passed[$key]['has_fee_based_on_product'] = 'yes';
                        }
                    }
                }
            }
        }

        $main_is_passed = $this->afrsm_pro_check_all_passed_general_rule($is_passed, 'has_fee_based_on_product', $general_rule_match);
        return $main_is_passed;
    }

    /**
     * Match category rules
     *
     * @since    3.4
     *
     * @uses afrsm_pro_fee_array_column_admin()
     * @uses WC_Product class
     * @uses WC_Product::is_virtual()
     * @uses wp_get_post_terms()
     * @uses afrsm_pro_array_flatten()
     *
     * @param array $cart_product_ids_array
     * @param array $category_array
     * @param string $general_rule_match
     *
     * @return string $main_is_passed
     */
    public function afrsm_pro_match_category_rule($cart_product_ids_array, $category_array, $general_rule_match) {
        $is_passed = array();
        $cart_category_id_array = array();
        foreach ($cart_product_ids_array as $product) {
            $cart_product_category = wp_get_post_terms($product, 'product_cat', array('fields' => 'ids'));
            if (isset($cart_product_category) && !empty($cart_product_category) && is_array($cart_product_category)) {
                $cart_category_id_array[] = $cart_product_category;
            }
        }
        $get_cat_all = array_unique($this->afrsm_pro_array_flatten($cart_category_id_array));

        foreach ($category_array as $key => $category) {
            if ('is_equal_to' === $category['product_fees_conditions_is']) {
                if (!empty($category['product_fees_conditions_values'])) {
                    foreach ($category['product_fees_conditions_values'] as $category_id) {
                        settype($category_id, 'integer');
                        if (in_array($category_id, $get_cat_all, true)) {
                            $is_passed[$key]['has_fee_based_on_category'] = 'yes';
                            break;
                        } else {
                            $is_passed[$key]['has_fee_based_on_category'] = 'no';
                        }
                    }
                }
            }
            if ('not_in' === $category['product_fees_conditions_is']) {
                if (!empty($category['product_fees_conditions_values'])) {
                    foreach ($category['product_fees_conditions_values'] as $category_id) {
                        settype($category_id, 'integer');
                        if (in_array($category_id, $get_cat_all, true)) {
                            $is_passed[$key]['has_fee_based_on_category'] = 'no';
                            break;
                        } else {
                            $is_passed[$key]['has_fee_based_on_category'] = 'yes';
                        }
                    }
                }
            }
        }

        $main_is_passed = $this->afrsm_pro_check_all_passed_general_rule($is_passed, 'has_fee_based_on_category', $general_rule_match);
        return $main_is_passed;
    }

    /**
     * Match tag rules
     *
     * @since    3.4
     *
     * @uses afrsm_pro_fee_array_column_admin()
     * @uses WC_Product class
     * @uses WC_Product::is_virtual()
     * @uses wp_get_post_terms()
     * @uses afrsm_pro_array_flatten()
     *
     * @param array $cart_product_ids_array
     * @param array $tag_array
     * @param string $general_rule_match
     *
     * @return string $main_is_passed
     */
    public function afrsm_pro_match_tag_rule($cart_product_ids_array, $tag_array, $general_rule_match) {
        $tagid = array();
        $is_passed = array();

        foreach ($cart_product_ids_array as $product) {
            $cart_product_tag = wp_get_post_terms($product, 'product_tag', array('fields' => 'ids'));
            if (isset($cart_product_tag) && !empty($cart_product_tag) && is_array($cart_product_tag)) {
                $tagid[] = $cart_product_tag;
            }
        }

        $get_tag_all = array_unique($this->afrsm_pro_array_flatten($tagid));
        foreach ($tag_array as $key => $tag) {
            if ('is_equal_to' === $tag['product_fees_conditions_is']) {
                if (!empty($tag['product_fees_conditions_values'])) {
                    foreach ($tag['product_fees_conditions_values'] as $tag_id) {
                        settype($tag_id, 'integer');
                        if (in_array($tag_id, $get_tag_all, true)) {
                            $is_passed[$key]['has_fee_based_on_tag'] = 'yes';
                            break;
                        } else {
                            $is_passed[$key]['has_fee_based_on_tag'] = 'no';
                        }
                    }
                }
            }
            if ('not_in' === $tag['product_fees_conditions_is']) {
                if (!empty($tag['product_fees_conditions_values'])) {
                    foreach ($tag['product_fees_conditions_values'] as $tag_id) {
                        settype($tag_id, 'integer');
                        if (in_array($tag_id, $get_tag_all, true)) {
                            $is_passed[$key]['has_fee_based_on_tag'] = 'no';
                            break;
                        } else {
                            $is_passed[$key]['has_fee_based_on_tag'] = 'yes';
                        }
                    }
                }
            }
        }

        $main_is_passed = $this->afrsm_pro_check_all_passed_general_rule($is_passed, 'has_fee_based_on_tag', $general_rule_match);
        return $main_is_passed;
    }

    /**
     * Match sku rules
     *
     * @since    3.4
     *
     * @uses afrsm_pro_fee_array_column_admin()
     * @uses WC_Product class
     * @uses WC_Product::is_virtual()
     * @uses WC_Product::get_sku()
     * @uses afrsm_pro_array_flatten()
     *
     * @param array $cart_product_ids_array
     * @param array $sku_array
     * @param string $general_rule_match
     *
     * @return string $main_is_passed
     */
    public function afrsm_pro_match_sku_rule($cart_product_ids_array, $sku_array, $general_rule_match) {
        $sku_ids = array();
        $is_passed = array();
        $passed_sku = array();

        if (!empty($cart_product_ids_array)) {
            foreach ($cart_product_ids_array as $product_id) {
                $product_sku = get_post_meta( $product_id, '_sku', true );
                if (isset($product_sku) && !empty($product_sku)) {
                    $sku_ids[] = $product_sku;
                }
            }
        }

        $get_all_unique_sku = array_unique($this->afrsm_pro_array_flatten($sku_ids));
        foreach ($sku_array as $key => $sku) {
            if ('is_equal_to' === $sku['product_fees_conditions_is']) {
                if (!empty($sku['product_fees_conditions_values'])) {
                    foreach ($sku['product_fees_conditions_values'] as $sku_name) {
                        $passed_sku[] = $sku_name;
                        if (in_array($sku_name, $get_all_unique_sku, true)) {
                            $is_passed[$key]['has_fee_based_on_sku'] = 'yes';
                            break;
                        } else {
                            $is_passed[$key]['has_fee_based_on_sku'] = 'no';
                        }
                    }
                }
            }
            if ('not_in' === $sku['product_fees_conditions_is']) {
                if (!empty($sku['product_fees_conditions_values'])) {
                    foreach ($sku['product_fees_conditions_values'] as $sku_name) {
                        if (in_array($sku_name, $get_all_unique_sku, true)) {
                            $is_passed[$key]['has_fee_based_on_sku'] = 'no';
                            break;
                        } else {
                            $is_passed[$key]['has_fee_based_on_sku'] = 'yes';
                        }
                    }
                }
            }
        }

        $main_is_passed = $this->afrsm_pro_check_all_passed_general_rule($is_passed, 'has_fee_based_on_sku', $general_rule_match);
        return $main_is_passed;
    }

    /**
     * Match attribute rules
     *
     * @since    3.4
     *
     * @uses afrsm_pro_fee_array_column_admin()
     *
     * @param array $cart_product_ids_array
     * @param string $att_name
     * @param string $general_rule_match
     *
     * @return string $main_is_passed
     */
    public function afrsm_pro_match_attribute_rule($cart_product_ids_array, $att_name, $general_rule_match) {
        $is_passed = array();
        $passed_product = array();

        foreach ($att_name as $key => $product) {
            if ($product['product_fees_conditions_is'] === 'is_equal_to') {
                if (!empty($product['product_fees_conditions_values'])) {
                    foreach ($product['product_fees_conditions_values'] as $product_id) {
                        $passed_product[] = $product_id;
                        if (in_array($product_id, $cart_product_ids_array, true)) {
                            $is_passed[$key]['has_fee_based_on_product_att'] = 'yes';
                            break;
                        } else {
                            $is_passed[$key]['has_fee_based_on_product_att'] = 'no';
                        }
                    }
                }
            }
            if ($product['product_fees_conditions_is'] === 'not_in') {
                if (!empty($product['product_fees_conditions_values'])) {
                    foreach ($product['product_fees_conditions_values'] as $product_id) {
                        if (in_array($product_id, $cart_product_ids_array, true)) {
                            $is_passed[$key]['has_fee_based_on_product_att'] = 'no';
                            break;
                        } else {
                            $is_passed[$key]['has_fee_based_on_product_att'] = 'yes';
                        }
                    }
                }
            }
        }

        $main_is_passed = $this->afrsm_pro_check_all_passed_general_rule($is_passed, 'has_fee_based_on_product_att', $general_rule_match);
        return $main_is_passed;
    }

    /**
     * Match user rules
     *
     * @since    3.4
     *
     * @uses is_user_logged_in()
     * @uses get_current_user_id()
     * @param string $general_rule_match
     *
     * @return string $main_is_passed
     */
    public function afrsm_pro_match_user_rule($user_array, $general_rule_match) {
        if (!is_user_logged_in()) {
            return false;
        }
        $current_user_id = get_current_user_id();
        $is_passed = array();
        foreach ($user_array as $key => $user) {
            $user['product_fees_conditions_values'] = array_map('intval', $user['product_fees_conditions_values']);
            if ('is_equal_to' === $user['product_fees_conditions_is']) {
                if (in_array($current_user_id, $user['product_fees_conditions_values'], true)) {
                    $is_passed[$key]['has_fee_based_on_user'] = 'yes';
                } else {
                    $is_passed[$key]['has_fee_based_on_user'] = 'no';
                }
            }
            if ('not_in' === $user['product_fees_conditions_is']) {
                if (in_array($current_user_id, $user['product_fees_conditions_values'], true)) {
                    $is_passed[$key]['has_fee_based_on_user'] = 'no';
                } else {
                    $is_passed[$key]['has_fee_based_on_user'] = 'yes';
                }
            }
        }

        $main_is_passed = $this->afrsm_pro_check_all_passed_general_rule($is_passed, 'has_fee_based_on_user', $general_rule_match);
        return $main_is_passed;
    }

    /**
     * Match user role rules
     *
     * @since    3.4
     *
     * @uses is_user_logged_in()
     *
     * @param array $user_role_array
     * @param string $general_rule_match
     *
     * @return string $main_is_passed
     */
    public function afrsm_pro_match_user_role_rule($user_role_array, $general_rule_match) {
        /**
         * check user loggedin or not
         */
        global $current_user;
        if (is_user_logged_in()) {
            $current_user_role = $current_user->roles[0];
        } else {
            $current_user_role = 'guest';
        }
        $is_passed = array();
        foreach ($user_role_array as $key => $user_role) {
            if ('is_equal_to' === $user_role['product_fees_conditions_is']) {
                if (in_array($current_user_role, $user_role['product_fees_conditions_values'], true)) {
                    $is_passed[$key]['has_fee_based_on_user_role'] = 'yes';
                } else {
                    $is_passed[$key]['has_fee_based_on_user_role'] = 'no';
                }
            }
            if ('not_in' === $user_role['product_fees_conditions_is']) {
                if (in_array($current_user_role, $user_role['product_fees_conditions_values'], true)) {
                    $is_passed[$key]['has_fee_based_on_user_role'] = 'no';
                } else {
                    $is_passed[$key]['has_fee_based_on_user_role'] = 'yes';
                }
            }
        }

        $main_is_passed = $this->afrsm_pro_check_all_passed_general_rule($is_passed, 'has_fee_based_on_user_role', $general_rule_match);
        return $main_is_passed;
    }

    /**
     * Match coupon role rules
     *
     * @since    3.4
     *
     * @uses WC_Cart::get_coupons()
     * @uses WC_Coupon::is_valid()
     *
     * @param string $wc_curr_version
     * @param array $coupon_array
     * @param string $general_rule_match
     *
     * @return string $main_is_passed
     */
    public function afrsm_pro_match_coupon_rule($wc_curr_version, $coupon_array, $general_rule_match) {
        global $woocommerce;
        if ($wc_curr_version >= 3.0) {
            $cart_coupon = WC()->cart->get_coupons();
        } else {
            $cart_coupon = isset($woocommerce->cart->coupons) && !empty($woocommerce->cart->coupons) ? $woocommerce->cart->coupons : array();
        }

        $couponId = array();
        $is_passed = array();
        foreach ($cart_coupon as $cartCoupon) {
            if ($cartCoupon->is_valid() && isset($cartCoupon) && !empty($cartCoupon)) {
                if ($wc_curr_version >= 3.0) {
                    $couponId[] = $cartCoupon->get_id();
                } else {
                    $couponId[] = $cartCoupon->id;
                }
            }
        }

        if (!empty($couponId)) {
            foreach ($coupon_array as $key => $coupon) {
                if ('is_equal_to' === $coupon['product_fees_conditions_is']) {
                    if (!empty($coupon['product_fees_conditions_values'])) {
                        foreach ($coupon['product_fees_conditions_values'] as $coupon_id) {
                            settype($coupon_id, 'integer');
                            if (in_array($coupon_id, $couponId, true)) {
                                $is_passed[$key]['has_fee_based_on_coupon'] = 'yes';
                                break;
                            } else {
                                $is_passed[$key]['has_fee_based_on_coupon'] = 'no';
                            }
                        }
                    }
                }
                if ('not_in' === $coupon['product_fees_conditions_is']) {
                    if (!empty($coupon['product_fees_conditions_values'])) {
                        foreach ($coupon['product_fees_conditions_values'] as $coupon_id) {
                            settype($coupon_id, 'integer');
                            if (in_array($coupon_id, $couponId, true)) {
                                $is_passed[$key]['has_fee_based_on_coupon'] = 'no';
                                break;
                            } else {
                                $is_passed[$key]['has_fee_based_on_coupon'] = 'yes';
                            }
                        }
                    }
                }
            }
        }

        $main_is_passed = $this->afrsm_pro_check_all_passed_general_rule($is_passed, 'has_fee_based_on_coupon', $general_rule_match);
        return $main_is_passed;
    }

    /**
     * Match rule based on cart subtotal before discount
     *
     * @since    3.4
     *
     * @uses WC_Cart::get_subtotal()
     *
     * @param string $wc_curr_version
     * @param array $cart_total_array
     * @param string $general_rule_match
     *
     * @return string $main_is_passed
     *
     */
    public function afrsm_pro_match_cart_subtotal_before_discount_rule($wc_curr_version, $cart_total_array, $general_rule_match) {
        global $woocommerce, $woocommerce_wpml;
        if ($wc_curr_version >= 3.0) {
            $total = $this->afrsm_pro_get_cart_subtotal();
        } else {
            $total = $woocommerce->cart->subtotal;
        }
        if (isset($woocommerce_wpml) && !empty($woocommerce_wpml->multi_currency)) {
            $new_total = $woocommerce_wpml->multi_currency->prices->unconvert_price_amount($total);
        } else {
            $new_total = $total;
        }

        $is_passed = array();
        foreach ($cart_total_array as $key => $cart_total) {
            settype($cart_total['product_fees_conditions_values'], 'float');
            if ('is_equal_to' === $cart_total['product_fees_conditions_is']) {
                if (!empty($cart_total['product_fees_conditions_values'])) {
                    if ($cart_total['product_fees_conditions_values'] === $new_total) {
                        $is_passed[$key]['has_fee_based_on_cart_total'] = 'yes';
                    } else {
                        $is_passed[$key]['has_fee_based_on_cart_total'] = 'no';
                    }
                }
            }
            if ('less_equal_to' === $cart_total['product_fees_conditions_is']) {
                if (!empty($cart_total['product_fees_conditions_values'])) {
                    if ($cart_total['product_fees_conditions_values'] >= $new_total) {
                        $is_passed[$key]['has_fee_based_on_cart_total'] = 'yes';
                    } else {
                        $is_passed[$key]['has_fee_based_on_cart_total'] = 'no';
                    }
                }
            }
            if ('less_then' === $cart_total['product_fees_conditions_is']) {
                if (!empty($cart_total['product_fees_conditions_values'])) {
                    if ($cart_total['product_fees_conditions_values'] > $new_total) {
                        $is_passed[$key]['has_fee_based_on_cart_total'] = 'yes';
                    } else {
                        $is_passed[$key]['has_fee_based_on_cart_total'] = 'no';
                    }
                }
            }
            if ('greater_equal_to' === $cart_total['product_fees_conditions_is']) {
                if (!empty($cart_total['product_fees_conditions_values'])) {
                    if ($cart_total['product_fees_conditions_values'] <= $new_total) {
                        $is_passed[$key]['has_fee_based_on_cart_total'] = 'yes';
                    } else {
                        $is_passed[$key]['has_fee_based_on_cart_total'] = 'no';
                    }
                }
            }
            if ('greater_then' === $cart_total['product_fees_conditions_is']) {
                $cart_total['product_fees_conditions_values'];
                if (!empty($cart_total['product_fees_conditions_values'])) {
                    if ($cart_total['product_fees_conditions_values'] < $new_total) {
                        $is_passed[$key]['has_fee_based_on_cart_total'] = 'yes';
                    } else {
                        $is_passed[$key]['has_fee_based_on_cart_total'] = 'no';
                    }
                }
            }
            if ('not_in' === $cart_total['product_fees_conditions_is']) {
                if (!empty($cart_total['product_fees_conditions_values'])) {
                    if ($new_total === $cart_total['product_fees_conditions_values']) {
                        $is_passed[$key]['has_fee_based_on_cart_total'] = 'yes';
                    } else {
                        $is_passed[$key]['has_fee_based_on_cart_total'] = 'no';
                    }
                }
            }
        }

        $main_is_passed = $this->afrsm_pro_check_all_passed_general_rule($is_passed, 'has_fee_based_on_cart_total', $general_rule_match);
        return $main_is_passed;
    }

    /**
     * Match rule based on cart subtotal after discount
     *
     * @since    3.4
     *
     * @uses afrsm_pro_remove_currency_symbol()
     * @uses WC_Cart::get_subtotal()
     * @uses WC_Cart::get_total_discount()
     *
     * @param string $wc_curr_version
     * @param array $cart_totalafter_array
     * @param string $general_rule_match
     *
     * @return string $main_is_passed
     */
    public function afrsm_pro_match_cart_subtotal_after_discount_rule($wc_curr_version, $cart_totalafter_array, $general_rule_match) {
        global $woocommerce, $woocommerce_wpml;
        if ($wc_curr_version >= 3.0) {
            $totalprice = $this->afrsm_pro_get_cart_subtotal();
        } else {
            $totalprice = $this->afrsm_pro_remove_currency_symbol($woocommerce->cart->get_cart_subtotal());
        }
        if ($wc_curr_version >= 3.0) {
            $totaldisc = $this->afrsm_pro_remove_currency_symbol(WC()->cart->get_total_discount());
        } else {
            $totaldisc = $this->afrsm_pro_remove_currency_symbol($woocommerce->cart->get_total_discount());
        }

        $is_passed = array();
        if ('' !== $totaldisc && 0.0 !== $totaldisc) {
            $resultprice = $totalprice - $totaldisc;
            if (isset($woocommerce_wpml) && !empty($woocommerce_wpml->multi_currency)) {
                $new_resultprice = $woocommerce_wpml->multi_currency->prices->unconvert_price_amount($resultprice);
            } else {
                $new_resultprice = $resultprice;
            }

            foreach ($cart_totalafter_array as $key => $cart_totalafter) {
                settype($cart_totalafter['product_fees_conditions_values'], 'float');
                if ('is_equal_to' === $cart_totalafter['product_fees_conditions_is']) {
                    if (!empty($cart_totalafter['product_fees_conditions_values'])) {
                        if ($cart_totalafter['product_fees_conditions_values'] === $new_resultprice) {
                            $is_passed[$key]['has_fee_based_on_cart_totalafter'] = 'yes';
                        } else {
                            $is_passed[$key]['has_fee_based_on_cart_totalafter'] = 'no';
                        }
                    }
                }
                if ('less_equal_to' === $cart_totalafter['product_fees_conditions_is']) {
                    if (!empty($cart_totalafter['product_fees_conditions_values'])) {
                        if ($cart_totalafter['product_fees_conditions_values'] >= $new_resultprice) {
                            $is_passed[$key]['has_fee_based_on_cart_totalafter'] = 'yes';
                        } else {
                            $is_passed[$key]['has_fee_based_on_cart_totalafter'] = 'no';
                        }
                    }
                }
                if ('less_then' === $cart_totalafter['product_fees_conditions_is']) {
                    if (!empty($cart_totalafter['product_fees_conditions_values'])) {
                        if ($cart_totalafter['product_fees_conditions_values'] > $new_resultprice) {
                            $is_passed[$key]['has_fee_based_on_cart_totalafter'] = 'yes';
                        } else {
                            $is_passed[$key]['has_fee_based_on_cart_totalafter'] = 'no';
                        }
                    }
                }
                if ('greater_equal_to' === $cart_totalafter['product_fees_conditions_is']) {
                    if (!empty($cart_totalafter['product_fees_conditions_values'])) {
                        if ($cart_totalafter['product_fees_conditions_values'] <= $new_resultprice) {
                            $is_passed[$key]['has_fee_based_on_cart_totalafter'] = 'yes';
                        } else {
                            $is_passed[$key]['has_fee_based_on_cart_totalafter'] = 'no';
                        }
                    }
                }
                if ('greater_then' === $cart_totalafter['product_fees_conditions_is']) {
                    if (!empty($cart_totalafter['product_fees_conditions_values'])) {
                        if ($cart_totalafter['product_fees_conditions_values'] < $new_resultprice) {
                            $is_passed[$key]['has_fee_based_on_cart_totalafter'] = 'yes';
                        } else {
                            $is_passed[$key]['has_fee_based_on_cart_totalafter'] = 'no';
                        }
                    }
                }
                if ('not_in' === $cart_totalafter['product_fees_conditions_is']) {
                    if (!empty($cart_totalafter['product_fees_conditions_values'])) {
                        if ($new_resultprice === $cart_totalafter['product_fees_conditions_values']) {
                            $is_passed[$key]['has_fee_based_on_cart_totalafter'] = 'yes';
                        } else {
                            $is_passed[$key]['has_fee_based_on_cart_totalafter'] = 'no';
                        }
                    }
                }
            }
        }

        $main_is_passed = $this->afrsm_pro_check_all_passed_general_rule($is_passed, 'has_fee_based_on_cart_totalafter', $general_rule_match);
        return $main_is_passed;
    }

    /**
     * Match rule based on total cart quantity
     *
     * @since    3.4
     *
     * @uses WC_Cart::get_cart()
     *
     * @param array $cart_array
     * @param array $quantity_array
     * @param string $general_rule_match
     *
     * @return string $main_is_passed
     */
    public function afrsm_pro_match_cart_total_cart_qty_rule( $cart_array, $quantity_array, $general_rule_match ) {
        $quantity_total = 0;

        foreach ($cart_array as $woo_cart_item) {
            $quantity_total += $woo_cart_item['quantity'];
        }

        $is_passed = array();
        foreach ($quantity_array as $key => $quantity) {
            settype($quantity['product_fees_conditions_values'], 'integer');
            if ('is_equal_to' === $quantity['product_fees_conditions_is']) {
                if (!empty($quantity['product_fees_conditions_values'])) {
                    if ($quantity_total === $quantity['product_fees_conditions_values']) {
                        $is_passed[$key]['has_fee_based_on_quantity'] = 'yes';
                    } else {
                        $is_passed[$key]['has_fee_based_on_quantity'] = 'no';
                    }
                }
            }
            if ('less_equal_to' === $quantity['product_fees_conditions_is']) {
                if (!empty($quantity['product_fees_conditions_values'])) {
                    if ($quantity['product_fees_conditions_values'] >= $quantity_total) {
                        $is_passed[$key]['has_fee_based_on_quantity'] = 'yes';
                    } else {
                        $is_passed[$key]['has_fee_based_on_quantity'] = 'no';
                    }
                }
            }
            if ('less_then' === $quantity['product_fees_conditions_is']) {
                if (!empty($quantity['product_fees_conditions_values'])) {
                    if ($quantity['product_fees_conditions_values'] > $quantity_total) {
                        $is_passed[$key]['has_fee_based_on_quantity'] = 'yes';
                    } else {
                        $is_passed[$key]['has_fee_based_on_quantity'] = 'no';
                    }
                }
            }
            if ('greater_equal_to' === $quantity['product_fees_conditions_is']) {
                if (!empty($quantity['product_fees_conditions_values'])) {
                    if ($quantity['product_fees_conditions_values'] <= $quantity_total) {
                        $is_passed[$key]['has_fee_based_on_quantity'] = 'yes';
                    } else {
                        $is_passed[$key]['has_fee_based_on_quantity'] = 'no';
                    }
                }
            }
            if ('greater_then' === $quantity['product_fees_conditions_is']) {
                if (!empty($quantity['product_fees_conditions_values'])) {
                    if ($quantity['product_fees_conditions_values'] < $quantity_total) {
                        $is_passed[$key]['has_fee_based_on_quantity'] = 'yes';
                    } else {
                        $is_passed[$key]['has_fee_based_on_quantity'] = 'no';
                    }
                }
            }
            if ('not_in' === $quantity['product_fees_conditions_is']) {
                if (!empty($quantity['product_fees_conditions_values'])) {
                    if ($quantity_total === $quantity['product_fees_conditions_values']) {
                        $is_passed[$key]['has_fee_based_on_quantity'] = 'yes';
                    } else {
                        $is_passed[$key]['has_fee_based_on_quantity'] = 'no';
                    }
                }
            }
        }

        $main_is_passed = $this->afrsm_pro_check_all_passed_general_rule($is_passed, 'has_fee_based_on_quantity', $general_rule_match);
        return $main_is_passed;
    }

    /**
     * Match rule based on total cart weight
     *
     * @since    3.4
     *
     * @uses WC_Cart::get_cart()
     * @uses WC_Product class
     * @uses WC_Product::is_virtual()
     *
     * @param array $cart_array
     * @param array $weight_array
     * @param string $general_rule_match
     *
     * @return string $main_is_passed
     */
    public function afrsm_pro_match_cart_total_weight_rule( $cart_array, $weight_array, $general_rule_match ) {
        $weight_total = 0;
        foreach ($cart_array as $woo_cart_item) {
            $product_cart_array = new WC_Product($woo_cart_item['product_id']);
            if (!($product_cart_array->is_virtual('yes'))) {
                $product_weight = $woo_cart_item['data']->get_weight();
                if ('0' !== $product_weight && '' !== $product_weight) {
                    $woo_cart_item_quantity = $woo_cart_item['quantity'];
                    $weight_total += $product_weight * $woo_cart_item_quantity;
                }
            }
        }

        $is_passed = array();
        foreach ($weight_array as $key => $weight) {
            settype($weight_total, 'float');
            settype($weight['product_fees_conditions_values'], 'float');
            if ('is_equal_to' === $weight['product_fees_conditions_is']) {
                if (!empty($weight['product_fees_conditions_values'])) {
                    if ($weight_total === $weight['product_fees_conditions_values']) {
                        $is_passed[$key]['has_fee_based_on_weight'] = 'yes';
                    } else {
                        $is_passed[$key]['has_fee_based_on_weight'] = 'no';
                    }
                }
            }
            if ('less_equal_to' === $weight['product_fees_conditions_is']) {
                if (!empty($weight['product_fees_conditions_values'])) {
                    if ($weight['product_fees_conditions_values'] >= $weight_total) {
                        $is_passed[$key]['has_fee_based_on_weight'] = 'yes';
                    } else {
                        $is_passed[$key]['has_fee_based_on_weight'] = 'no';
                    }
                }
            }
            if ('less_then' === $weight['product_fees_conditions_is']) {
                if (!empty($weight['product_fees_conditions_values'])) {
                    if ($weight['product_fees_conditions_values'] > $weight_total) {
                        $is_passed[$key]['has_fee_based_on_weight'] = 'yes';
                    } else {
                        $is_passed[$key]['has_fee_based_on_weight'] = 'no';
                    }
                }
            }
            if ('greater_equal_to' === $weight['product_fees_conditions_is']) {
                if (!empty($weight['product_fees_conditions_values'])) {
                    if ($weight['product_fees_conditions_values'] <= $weight_total) {
                        $is_passed[$key]['has_fee_based_on_weight'] = 'yes';
                    } else {
                        $is_passed[$key]['has_fee_based_on_weight'] = 'no';
                    }
                }
            }
            if ('greater_then' === $weight['product_fees_conditions_is']) {
                if (!empty($weight['product_fees_conditions_values'])) {
                    if ($weight_total > $weight['product_fees_conditions_values']) {
                        $is_passed[$key]['has_fee_based_on_weight'] = 'yes';
                    } else {
                        $is_passed[$key]['has_fee_based_on_weight'] = 'no';
                    }
                }
            }
            if ('not_in' === $weight['product_fees_conditions_is']) {
                if (!empty($weight['product_fees_conditions_values'])) {
                    if ($weight_total === $weight['product_fees_conditions_values']) {
                        $is_passed[$key]['has_fee_based_on_weight'] = 'yes';
                    } else {
                        $is_passed[$key]['has_fee_based_on_weight'] = 'no';
                    }
                }
            }
        }

        $main_is_passed = $this->afrsm_pro_check_all_passed_general_rule($is_passed, 'has_fee_based_on_weight', $general_rule_match);
        return $main_is_passed;
    }

    /**
     * Match rule based on shipping class
     *
     * @since    3.4
     *
     * @uses WC_Product class
     * @uses WC_Product::is_virtual()
     * @uses get_the_terms()
     * @uses afrsm_pro_array_flatten()
     *
     * @param array $cart_product_ids_array
     * @param array $shipping_class_array
     * @param string $general_rule_match
     *
     * @return string $main_is_passed
     */
    public function afrsm_pro_match_shipping_class_rule( $cart_product_ids_array, $shipping_class_array, $general_rule_match ) {
        $shippingclass = array();
        $passed_shipping_class = array();

        foreach ($cart_product_ids_array as $product) {
            $get_shipping_class = wp_get_post_terms($product, 'product_shipping_class', array('fields' => 'ids'));
            if (isset($get_shipping_class) && !empty($get_shipping_class) && is_array($get_shipping_class)) {
                $shippingclass[] = $get_shipping_class;
            }
        }

        $get_shipping_class_all = array_unique($this->afrsm_pro_array_flatten($shippingclass));
        $is_passed = array();
        foreach ($shipping_class_array as $key => $shipping_class) {
            if ('is_equal_to' === $shipping_class['product_fees_conditions_is']) {
                if (!empty($shipping_class['product_fees_conditions_values'])) {
                    foreach ($shipping_class['product_fees_conditions_values'] as $shipping_class_slug) {
                        $shipping_class_term_id = get_term_by('slug', $shipping_class_slug, 'product_shipping_class');
                        $shipping_class_id = $shipping_class_term_id->term_id;
                        settype($shipping_class_id, 'integer');
                        $passed_shipping_class[] = $shipping_class_id;
                        if (in_array($shipping_class_id, $get_shipping_class_all ,true)) {
                            $is_passed[$key]['has_fee_based_on_shipping_class'] = 'yes';
                            break;
                        } else {
                            $is_passed[$key]['has_fee_based_on_shipping_class'] = 'no';
                        }
                    }
                }
            }
            if ('not_in' === $shipping_class['product_fees_conditions_is']) {
                if (!empty($shipping_class['product_fees_conditions_values'])) {
                    foreach ($shipping_class['product_fees_conditions_values'] as $shipping_class_slug) {
                        $shipping_class_term_id = get_term_by('slug', $shipping_class_slug, 'product_shipping_class');
                        $shipping_class_id = $shipping_class_term_id->term_id;
                        settype($shipping_class_id, 'integer');
                        if (in_array($shipping_class_id, $get_shipping_class_all ,true)) {
                            $is_passed[$key]['has_fee_based_on_shipping_class'] = 'no';
                            break;
                        } else {
                            $is_passed[$key]['has_fee_based_on_shipping_class'] = 'yes';
                        }
                    }
                }
            }
        }

        $main_is_passed = $this->afrsm_pro_check_all_passed_general_rule($is_passed, 'has_fee_based_on_shipping_class', $general_rule_match);
        return $main_is_passed;
    }

    /**
     * Match rule based on payment gateway
     *
     * @since    3.4
     *
     * @uses WC_Session::get()
     *
     * @param array $payment_methods_array
     *
     * @return array $is_passed
     */
    public function afrsm_pro_match_payment_gateway_rule( $payment_methods_array, $general_rule_match ) {
        $is_passed = array();

        $chosen_payment_method = WC()->session->get( 'chosen_payment_method' );
        foreach ($payment_methods_array as $key => $payment) {
            if ($payment['product_fees_conditions_is'] === 'is_equal_to') {
                if (in_array($chosen_payment_method, $payment['product_fees_conditions_values'], true)) {
                    $is_passed[$key]['has_fee_based_on_payment'] = 'yes';
                } else {
                    $is_passed[$key]['has_fee_based_on_payment'] = 'no';
                }
            }
            if ($payment['product_fees_conditions_is'] === 'not_in') {
                if (!in_array($chosen_payment_method, $payment['product_fees_conditions_values'], true)) {
                    $is_passed[$key]['has_fee_based_on_payment'] = 'no';
                } else {
                    $is_passed[$key]['has_fee_based_on_payment'] = 'yes';
                }
            }
        }

        $main_is_passed = $this->afrsm_pro_check_all_passed_general_rule($is_passed, 'has_fee_based_on_payment', $general_rule_match);
        return $main_is_passed;
    }

    /**
     * Find unique id based on given array
     *
     * @since    3.6
     *
     * @param array $is_passed
     * @param string $has_fee_based
     * @param string $general_rule_match
     *
     * @return string $main_is_passed
     */
    public function afrsm_pro_check_all_passed_general_rule($is_passed, $has_fee_based, $general_rule_match) {
        $main_is_passed = 'no';
        $flag = array();
        if (!empty($is_passed)) {
            foreach ($is_passed as $key => $is_passed_value) {
                if ('yes' === $is_passed_value[$has_fee_based]) {
                    $flag[$key] = true;
                } else {
                    $flag[$key] = false;
                }
            }
            if ('any' === $general_rule_match) {
                if (in_array(true, $flag, true)) {
                    $main_is_passed = 'yes';
                } else {
                    $main_is_passed = 'no';
                }
            } else {
                if (in_array(false, $flag, true)) {
                    $main_is_passed = 'no';
                } else {
                    $main_is_passed = 'yes';
                }
            }
        }
        return $main_is_passed;
    }

    /**
     * Find unique id based on given array
     *
     * @since    1.0.0
     *
     * @param array $array
     *
     * @return array $result if $array is empty it will return false otherwise return array as $result
     */
    public function afrsm_pro_array_flatten($array) {
        if (!is_array($array)) {
            return false;
        }
        $result = array();
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $result = array_merge($result, $this->afrsm_pro_array_flatten($value));
            } else {
                $result[$key] = $value;
            }
        }
        return $result;
    }

    /**
     * Find a matching zone for a given package.
     * @since  3.0.0
     *
     * @uses   afrsm_pro_wc_make_numeric_postcode()
     *
     * @param array|object $package
     * @param  array $available_zone_id_array
     *
     * @return int $return_zone_id
     */
    public function afrsm_pro_check_zone_available($package, $available_zone_id_array) {
        $country = $package['destination']['country'];
        if (!empty($package['destination']['state']) && '' !== $package['destination']['state']) {
            $state = $country . ':' . $package['destination']['state'];
        } else {
            $state = '';
        }

        $postcode = $package['destination']['postcode'];
        $valid_postcodes = array('*', $postcode);

        // Work out possible valid wildcard postcodes
        $postcode_length = strlen($postcode);
        $wildcard_postcode = $postcode;

        for ($i = 0; $i < $postcode_length; $i++) {
            $wildcard_postcode = substr($wildcard_postcode, 0, -1);
            $valid_postcodes[] = $wildcard_postcode . '*';
        }

        $return_zone_id = '';
        foreach ($available_zone_id_array as $available_zone_id) {
            $postcode_ranges = new WP_Query(array(
                'post_type' => 'wc_afrsm_zone',
                'post_status' => 'publish',
                'posts_per_page' => -1,
                'post__in' => array($available_zone_id)
            ));

            $location_code = array();
            foreach ($postcode_ranges->posts as $postcode_ranges_value) {
                $postcode_ranges_location_code = get_post_meta($postcode_ranges_value->ID, 'location_code', false);
                $zone_type = get_post_meta($postcode_ranges_value->ID, 'zone_type', true);
                $location_code[$postcode_ranges_value->ID] = $postcode_ranges_location_code;

                foreach ($postcode_ranges_location_code as $location_code_sub_val) {
                    $country_array = array();
                    $state_array = array();
                    foreach ($location_code_sub_val as $location_country_state => $location_code_postcode_val) {
                        if ('postcodes' === $zone_type) {
                            if (false !== strpos($location_country_state, ':')) {
                                $location_country_state_explode = explode(':', $location_country_state);
                            } else {
                                $state_array = array();
                            }
                            if (!empty($location_country_state_explode)) {
                                if ( array_key_exists( '0', $location_country_state_explode ) ) {
                                    $country_array[] = $location_country_state_explode[0];
                                }
                            } else {
                                $country_array[] = $location_country_state;
                            }
                            if (!empty($location_country_state_explode)) {
                                if ( array_key_exists( '1', $location_country_state_explode ) ) {
                                    $state_array[] = $location_country_state;
                                }
                            }
                            foreach ($location_code_postcode_val as $location_code_val) {
                                if (false !== strpos($location_code_val, '=')) {
                                    $location_code_val = str_replace('=', ' ', $location_code_val);
                                }
                                if (false !== strpos($location_code_val, '-')) {
                                    if ($postcode_ranges->posts) {
                                        $encoded_postcode = $this->afrsm_pro_wc_make_numeric_postcode($postcode);
                                        $encoded_postcode_len = strlen($encoded_postcode);

                                        $range = array_map('trim', explode('-', $location_code_val));

                                        if (2 !== sizeof($range)) {
                                            continue;
                                        }

                                        if (is_numeric($range[0]) && is_numeric($range[1])) {
                                            $encoded_postcode = $postcode;
                                            $min = $range[0];
                                            $max = $range[1];
                                        } else {
                                            $min = $this->afrsm_pro_wc_make_numeric_postcode($range[0]);
                                            $max = $this->afrsm_pro_wc_make_numeric_postcode($range[1]);
                                            $min = str_pad($min, $encoded_postcode_len, '0');
                                            $max = str_pad($max, $encoded_postcode_len, '9');
                                        }

                                        if ($encoded_postcode >= $min && $encoded_postcode <= $max) {
                                            $return_zone_id = $available_zone_id;
                                        }

                                    }
                                } elseif (false !== strpos($location_code_val, '*')) {
                                    if (in_array($location_code_val, $valid_postcodes, true)) {
                                        $return_zone_id = $available_zone_id;
                                    }
                                } else {
                                    if (in_array($country, $country_array, true)) {
                                        if (!empty($state_array)) {
                                            if (in_array($state, $state_array, true)) {
                                                if (in_array($postcode, $location_code_postcode_val, true)) {
                                                    $return_zone_id = $available_zone_id;
                                                }
                                            }
                                        } else {
                                            if (in_array($postcode, $location_code_postcode_val, true)) {
                                                $return_zone_id = $available_zone_id;
                                            }
                                        }
                                    }
                                }
                            }
                        } elseif ('countries' === $zone_type) {
                            if (!empty($country) && in_array($country, $location_code_postcode_val, true)) {
                                $return_zone_id = $available_zone_id;
                            }
                        } elseif ('states' === $zone_type) {
                            if (!empty($state) && in_array($state, $location_code_postcode_val, true)) {
                                $return_zone_id = $available_zone_id;
                            }
                        }
                    }
                }
            }
        }
        return $return_zone_id;
    }

    /**
     * Make numeric postcode function.
     *
     * @since  1.0.0
     *
     * Converts letters to numbers so we can do a simple range check on postcodes.
     *
     * E.g. PE30 becomes 16050300 (P = 16, E = 05, 3 = 03, 0 = 00)
     *
     * @access public
     *
     * @param mixed $postcode
     *
     * @return void
     */
    function afrsm_pro_wc_make_numeric_postcode($postcode) {
        $postcode_length = strlen($postcode);
        $letters_to_numbers = array_merge(array(0), range('A', 'Z'));
        $letters_to_numbers = array_flip($letters_to_numbers);
        $numeric_postcode = '';

        for ($i = 0; $i < $postcode_length; $i++) {
            if (is_numeric($postcode[$i])) {
                $numeric_postcode .= str_pad($postcode[$i], 2, '0', STR_PAD_LEFT);
            } elseif (isset($letters_to_numbers[$postcode[$i]])) {
                $numeric_postcode .= str_pad($letters_to_numbers[$postcode[$i]], 2, '0', STR_PAD_LEFT);
            } else {
                $numeric_postcode .= '00';
            }
        }

        return $numeric_postcode;
    }

    /**
     * Display array column
     *
     * @since  1.0.0
     *
     * @param  array $input
     * @param  int $columnKey
     * @param  int $indexKey
     *
     * @return array $array It will return array if any error generate then it will return false
     */
    public function afrsm_pro_fee_array_column_admin(array $input, $columnKey, $indexKey = null) {
        $array = array();
        foreach ($input as $value) {
            if (!isset($value[$columnKey])) {
                wp_die(sprintf(esc_html_x('Key %d does not exist in array', esc_attr($columnKey), 'advanced-flat-rate-shipping-for-woocommerce')));
                return false;
            }
            if (is_null($indexKey)) {
                $array[] = $value[$columnKey];
            } else {
                if (!isset($value[$indexKey])) {
                    wp_die(sprintf(esc_html_x('Key %d does not exist in array', esc_attr($indexKey), 'advanced-flat-rate-shipping-for-woocommerce')));
                    return false;
                }
                if (!is_scalar($value[$indexKey])) {
                    wp_die(sprintf(esc_html_x('Key %d does not contain scalar value', esc_attr($indexKey), 'advanced-flat-rate-shipping-for-woocommerce')));
                    return false;
                }
                $array[$value[$indexKey]] = $value[$columnKey];
            }
        }
        return $array;
    }

    /**
     * Remove WooCommerce currency symbol
     *
     * @since  1.0.0
     *
     * @uses get_woocommerce_currency_symbol()
     *
     * @param  float $price
     *
     * @return float $new_price2
     */
    public function afrsm_pro_remove_currency_symbol($price) {
        $wc_currency_symbol = get_woocommerce_currency_symbol();
        $new_price = str_replace($wc_currency_symbol, '', $price);
        $new_price2 = (double)preg_replace('/[^.\d]/', '', $new_price);
        return $new_price2;
    }

    /*
     * Get WooCommerce version number
     *
     * @since 1.0.0
     *
     * @return string if file is not exists then it will return null
     */
    function afrsm_pro_get_woo_version_number() {
        // If get_plugins() isn't available, require it
        if (!function_exists('get_plugins')) {
            require_once(ABSPATH . 'wp-admin/includes/plugin.php');
        }

        // Create the plugins folder and file variables
        $plugin_folder = get_plugins('/' . 'woocommerce');
        $plugin_file = 'woocommerce.php';

        // If the plugin version number is set, return it
        if (isset($plugin_folder[$plugin_file]['Version'])) {
            return $plugin_folder[$plugin_file]['Version'];
        } else {
            return NULL;
        }
    }

    /**
     * Save shipping order in shipping list section
     *
     * @since 1.0.0
     */
    public function afrsm_pro_sm_sort_order() {
        $default_lang = $this->afrsm_pro_get_default_langugae_with_sitpress();

        $get_smOrderArray   = filter_input(INPUT_GET, 'smOrderArray', FILTER_SANITIZE_NUMBER_INT, FILTER_REQUIRE_ARRAY);
        $smOrderArray = !empty($get_smOrderArray) ? array_map('sanitize_text_field', wp_unslash($get_smOrderArray)): '';
        if (isset($smOrderArray) && !empty($smOrderArray)) {
            update_option('sm_sortable_order_'. $default_lang, $smOrderArray);
        }
        wp_die();
    }

    /**
     * Save master settings data
     *
     * @since 1.0.0
     */
    public function afrsm_pro_save_master_settings() {
        $get_what_to_do = filter_input(INPUT_GET, 'what_to_do', FILTER_SANITIZE_STRING);
        $get_shipping_display_mode = filter_input(INPUT_GET, 'shipping_display_mode', FILTER_SANITIZE_STRING);
        $get_combine_default_shipping_with_forceall = filter_input(INPUT_GET, 'combine_default_shipping_with_forceall', FILTER_SANITIZE_STRING);
        $get_forceall_label = filter_input(INPUT_GET, 'forceall_label', FILTER_SANITIZE_STRING);
        $get_chk_enable_logging = filter_input(INPUT_GET, 'chk_enable_logging', FILTER_SANITIZE_STRING);


        $what_to_do = !empty($get_what_to_do) ? sanitize_text_field(wp_unslash($get_what_to_do)) : '';
        $shipping_display_mode = !empty($get_shipping_display_mode) ? sanitize_text_field(wp_unslash($get_shipping_display_mode)) : '';
        $combine_default_shipping_with_forceall = !empty($get_combine_default_shipping_with_forceall) ? sanitize_text_field(wp_unslash($get_combine_default_shipping_with_forceall)) : '';
        $forceall_label = !empty($get_forceall_label) ? sanitize_text_field(wp_unslash($get_forceall_label)) : '';

        if (isset($what_to_do) && !empty($what_to_do)) {
            update_option('what_to_do_method', $what_to_do);

            if ('allow_customer' === $what_to_do) {
                if (isset($shipping_display_mode) && !empty($shipping_display_mode)) {
                    update_option('md_woocommerce_shipping_method_format', $shipping_display_mode);
                }
            } else {
                update_option('md_woocommerce_shipping_method_format', 'radio_button_mode');
            }
        }

        if (isset($combine_default_shipping_with_forceall) && !empty($combine_default_shipping_with_forceall)) {
            update_option('combine_default_shipping_with_forceall', $combine_default_shipping_with_forceall);
        }
        if (isset($get_chk_enable_logging) && !empty($get_chk_enable_logging)) {
            update_option('chk_enable_logging', $get_chk_enable_logging);
        }
        if (isset($get_chk_enable_logging) && !empty($get_chk_enable_logging)) {
            update_option('forceall_label', $forceall_label);
        } else {
            update_option('forceall_label', '');
        }
        wp_die();
    }

    /**
     * Display textfield and multiselect dropdown based on country, state, zone and etc
     *
     * @since 1.0.0
     *
     * @uses afrsm_pro_get_country_list()
     * @uses afrsm_pro_get_states_list()
     * @uses afrsm_pro_get_zones_list()
     * @uses afrsm_pro_get_product_list()
     * @uses afrsm_pro_get_varible_product_list()
     * @uses afrsm_pro_get_category_list()
     * @uses afrsm_pro_get_tag_list()
     * @uses afrsm_pro_get_sku_list()
     * @uses afrsm_pro_get_user_list()
     * @uses afrsm_pro_get_user_role_list()
     * @uses afrsm_pro_get_coupon_list()
     * @uses afrsm_pro_get_advance_flat_rate_class()
     * @uses Advanced_Flat_Rate_Shipping_For_WooCommerce_Pro::afrsm_pro_allowed_html_tags()
     *
     * @return string $html
     */
    public function afrsm_pro_product_fees_conditions_values_ajax() {

        $get_condition = filter_input(INPUT_GET, 'condition', FILTER_SANITIZE_STRING);
        $get_count = filter_input(INPUT_GET, 'count', FILTER_SANITIZE_NUMBER_INT);

        $condition = isset($get_condition) ? sanitize_text_field($get_condition) : '';
        $count = isset($get_count) ? sanitize_text_field($get_count) : '';

        $att_taxonomy = wc_get_attribute_taxonomy_names();

        $html = '';
        if ('country' === $condition) {
            $html .= wp_json_encode($this->afrsm_pro_get_country_list($count,[],true));
        } elseif ('state' === $condition) {
            $html .= wp_json_encode($this->afrsm_pro_get_states_list($count,[],true));
        } elseif ('postcode' === $condition) {
            $html .= 'textarea';
        } elseif ('zone' === $condition) {
            $html .= wp_json_encode($this->afrsm_pro_get_zones_list($count,[],true));
        } elseif ('product' === $condition) {
            $html .= wp_json_encode($this->afrsm_pro_get_product_list($count,[],true));
        } elseif ('variableproduct' === $condition) {
            $html .= wp_json_encode($this->afrsm_pro_get_varible_product_list($count,[],true));
        } elseif ('category' === $condition) {
            $html .= wp_json_encode($this->afrsm_pro_get_category_list($count,[],true));
        } elseif ('tag' === $condition) {
            $html .= wp_json_encode($this->afrsm_pro_get_tag_list($count,[],true));
        } elseif (in_array($condition, $att_taxonomy, true) ) {
            $html .= wp_json_encode($this->afrsm_pro_get_att_term_list($count,$condition,[],true));
        } elseif ('sku' === $condition) {
            $html .= wp_json_encode($this->afrsm_pro_get_sku_list($count,[],true));
        } elseif ('user' === $condition) {
            $html .= wp_json_encode($this->afrsm_pro_get_user_list($count,[],true));
        } elseif ('user_role' === $condition) {
            $html .= wp_json_encode($this->afrsm_pro_get_user_role_list($count,[],true));
        } elseif ('cart_total' === $condition) {
            $html .= 'input';
        } elseif ('cart_totalafter' === $condition) {
            $html .= 'input';
        } elseif ('quantity' === $condition) {
            $html .= 'input';
        } elseif ('weight' === $condition) {
            $html .= 'input';
        } elseif ('coupon' === $condition) {
            $html .= wp_json_encode($this->afrsm_pro_get_coupon_list($count,[],true));
        } elseif ('shipping_class' === $condition) {
            $html .= wp_json_encode($this->afrsm_pro_get_advance_flat_rate_class($count,[],true));
        } elseif ('payment_method' === $condition) {
            $html .= wp_json_encode($this->afrsm_pro_get_payment($count,[],true));
        }
        echo wp_kses($html, Advanced_Flat_Rate_Shipping_For_WooCommerce_Pro::afrsm_pro_allowed_html_tags());
        wp_die();// this is required to terminate immediately and return a proper response
    }

    /**
     * Get country list
     *
     * @since  1.0.0
     *
     * @uses WC_Countries() class
     *
     * @param string $count
     * @param array $selected
     *
     * @return string $html
     */
    public function afrsm_pro_get_country_list($count = '', $selected = array(), $json=false) {
        $countries_obj = new WC_Countries();
        $getCountries = $countries_obj->__get('countries');
        $html = '<select name="fees[product_fees_conditions_values][value_' . esc_attr( $count ). '][]" class="afrsm_select product_fees_conditions_values multiselect2 product_fees_conditions_values_country" multiple="multiple">';
        if (!empty($getCountries)) {
            foreach ($getCountries as $code => $country) {
                $selectedVal = is_array($selected) && !empty($selected) && in_array($code, $selected, true) ? 'selected=selected' : '';
                $html .= '<option value="' . esc_attr( $code ) . '" ' . esc_attr( $selectedVal ) . '>' . esc_html( $country ) . '</option>';
            }
        }

        $html .= '</select>';
        if($json){
            return $this->afrsm_pro_convert_array_to_json($getCountries);
        }
        return $html;
    }

    /**
     * Get the states for a country.
     *
     * @since  1.0.0
     *
     * @uses WC_Countries::get_allowed_countries()
     * @uses WC_Countries::get_states()
     *
     * @param string $count
     * @param array $selected
     *
     * @return string $html
     */
    public function afrsm_pro_get_states_list($count = '', $selected = array(), $json=false) {
        $filter_states = [];
        $countries = WC()->countries->get_allowed_countries();
        $html = '<select name="fees[product_fees_conditions_values][value_' . esc_attr( $count ) . '][]" class="afrsm_select product_fees_conditions_values multiselect2 product_fees_conditions_values_state" multiple="multiple">';
        if (!empty($countries) && is_array($countries)):
            foreach ($countries as $key => $val) {
                $states = WC()->countries->get_states($key);
                if (!empty($states)) {
                    foreach ($states as $state_key => $state_value) {
                        $selectedVal = is_array($selected) && !empty($selected) && in_array(esc_attr($key . ':' . $state_key), $selected, true) ? 'selected=selected' : '';
                        $html .= '<option value="' . esc_attr($key . ':' . $state_key) . '" ' . esc_attr( $selectedVal ) . '>' . esc_html($val . ' -> ' . $state_value) . '</option>';
                        $filter_states[$key.':'.$state_key] = $val . ' -> ' . $state_value;
                    }
                }
            }
        endif;
        $html .= '</select>';
        if($json){
            return $this->afrsm_pro_convert_array_to_json($filter_states);
        }
        return $html;
    }

    /**
     * Get all zones list
     *
     * @since  1.0.0
     *
     * @uses afrsm_pro_get_default_langugae_with_sitpress()
     *
     * @param string $count
     * @param array $selected
     *
     * @return string $html
     */
    public function afrsm_pro_get_zones_list( $count = '', $selected = array(), $json=false) {
        global $sitepress;
        $default_lang = $this->afrsm_pro_get_default_langugae_with_sitpress();
        $filter_zone=[];

        $get_all_zones = new WP_Query(array(
            'post_type' => 'wc_afrsm_zone',
            'post_status' => 'publish',
            'posts_per_page' => -1
        ));

        $get_zones_data = $get_all_zones->posts;

        $html = '<select rel-id="' . esc_attr($count) . '" name="fees[product_fees_conditions_values][value_' . esc_attr( $count ) . '][]" class="afrsm_select product_fees_conditions_values multiselect2 product_fees_conditions_values_zone" multiple="multiple">';
        if (isset($get_zones_data) && !empty($get_zones_data)) {
            foreach ($get_zones_data as $get_all_zone) {

                if (!empty($sitepress)) {
                    $new_zone_id = apply_filters('wpml_object_id', $get_all_zone->ID, 'wc_afrsm_zone', TRUE, $default_lang);
                } else {
                    $new_zone_id = $get_all_zone->ID;
                }
                $selected = array_map('intval',$selected);
                $selectedVal = is_array($selected) && !empty($selected) && in_array($new_zone_id, $selected, true) ? 'selected=selected' : '';
                $html .= '<option value="' . esc_attr( $new_zone_id ) . '" ' . esc_attr( $selectedVal ) . '>' . esc_html( get_the_title($new_zone_id) ) . '</option>';
                $filter_zone[$new_zone_id]= get_the_title($new_zone_id);

            }
        }
        $html .= '</select>';
        if($json){
            return $this->afrsm_pro_convert_array_to_json($filter_zone);
        }
        return $html;
    }

    /**
     * Get product list
     *
     * @since  1.0.0
     *
     * @uses afrsm_pro_get_default_langugae_with_sitpress()
     *
     * @param string $count
     * @param array $selected
     *
     * @return string $html
     */
    public function afrsm_pro_get_product_list($count = '', $selected = array(),$json=false) {
        global $sitepress;
        $default_lang = $this->afrsm_pro_get_default_langugae_with_sitpress();

        $get_all_products = new WP_Query(array(
            'post_type' => 'product',
            'post_status' => 'publish',
            'posts_per_page' => -1
        ));

        $html = '<select id="product-filter-' . esc_attr( $count ) . '" rel-id="' . esc_attr( $count ) . '" name="fees[product_fees_conditions_values][value_' . esc_attr( $count ) . '][]" class="afrsm_select product_fees_conditions_values multiselect2 product_fees_conditions_values_product product_fees_conditions_values_'.esc_attr( $count ).'" multiple="multiple">';
        if (isset($get_all_products->posts) && !empty($get_all_products->posts)) {

            foreach ($get_all_products->posts as $get_all_product) {

                if (!empty($sitepress)) {
                    $new_product_id = apply_filters('wpml_object_id', $get_all_product->ID, 'product', TRUE, $default_lang);
                } else {
                    $new_product_id = $get_all_product->ID;
                }
                $selected = array_map('intval',$selected);
                $selectedVal = is_array($selected) && !empty($selected) && in_array($new_product_id, $selected, true) ? 'selected=selected' : '';
                if ('' !== $selectedVal) {
                    $html .= '<option value="' . esc_attr( $new_product_id ) . '" ' . esc_attr( $selectedVal ) . '>' . '#' . esc_html( $new_product_id ) . ' - ' . esc_html( get_the_title($new_product_id) ) . '</option>';
                }
            }
        }
        $html .= '</select>';
        if($json){
            return [];
        }
        return $html;
    }

    /**
     * Get variable product list in Advance pricing rules
     *
     * @since  3.4
     *
     * @uses afrsm_pro_get_default_langugae_with_sitpress()
     * @uses wc_get_product()
     * @uses WC_Product::is_type()
     *
     * @param string $count
     * @param array $selected
     *
     * @return string $html
     */
    public function afrsm_pro_get_product_options($count = '', $selected = array()) {
        global $sitepress;
        $default_lang = $this->afrsm_pro_get_default_langugae_with_sitpress();

        $get_all_products = new WP_Query(array(
            'post_type' => 'product',
            'post_status' => 'publish',
            'posts_per_page' => -1
        ));

        $baselang_variation_product_ids = array();
        $defaultlang_simple_product_ids = array();

        $html = '';
        if (isset($get_all_products->posts) && !empty($get_all_products->posts)) {
            foreach ($get_all_products->posts as $get_all_product) {
                $_product = wc_get_product($get_all_product->ID);
                if ($_product->is_type('variable')) {
                    $variations = $_product->get_available_variations();
                    foreach ($variations as $value) {
                        if (!empty($sitepress)) {
                            $defaultlang_variation_product_id = apply_filters('wpml_object_id', $value['variation_id'], 'product', TRUE, $default_lang);
                        } else {
                            $defaultlang_variation_product_id = $value['variation_id'];
                        }
                        $baselang_variation_product_ids[] = $defaultlang_variation_product_id;
                    }
                }
                if ($_product->is_type('simple')) {
                    if (!empty($sitepress)) {
                        $defaultlang_simple_product_id = apply_filters('wpml_object_id', $get_all_product->ID, 'product', TRUE, $default_lang);
                    } else {
                        $defaultlang_simple_product_id = $get_all_product->ID;
                    }
                    $defaultlang_simple_product_ids[] = $defaultlang_simple_product_id;
                }
            }
        }

        $baselang_product_ids = array_merge($baselang_variation_product_ids, $defaultlang_simple_product_ids);

        if (isset($baselang_product_ids) && !empty($baselang_product_ids)) {
            foreach ($baselang_product_ids as $baselang_product_id) {
                $selected = array_map('intval',$selected);
                $selectedVal = is_array($selected) && !empty($selected) && in_array($baselang_product_id, $selected, true) ? 'selected=selected' : '';
                if ('' !== $selectedVal) {
                    $html .= '<option value="' . esc_attr( $baselang_product_id ) . '" ' . esc_attr( $selectedVal ) . '>' . '#' . esc_html( $baselang_product_id ) . ' - ' . esc_html( get_the_title($baselang_product_id) ) . '</option>';
                }
            }
        }
        return $html;
    }

    /**
     * Get category list in Advance pricing rules
     *
     * @since  3.4
     *
     * @uses afrsm_pro_get_default_langugae_with_sitpress()
     *
     * @param array $selected
     *
     * @return string $html
     */
    public function afrsm_pro_get_category_options($selected = array(), $json) {
        global $sitepress;
        $default_lang = $this->afrsm_pro_get_default_langugae_with_sitpress();
        $filter_category_list = [];

        $taxonomy = 'product_cat';
        $post_status = 'publish';
        $orderby = 'name';
        $hierarchical = 1;
        $empty = 0;

        $args = array(
            'post_type' => 'product',
            'post_status' => $post_status,
            'taxonomy' => $taxonomy,
            'orderby' => $orderby,
            'hierarchical' => $hierarchical,
            'hide_empty' => $empty,
            'posts_per_page' => -1
        );
        $get_all_categories = get_categories($args);
        $html = '';
        if (isset($get_all_categories) && !empty($get_all_categories)) {
            foreach ($get_all_categories as $get_all_category) {

                if ( ! empty( $sitepress ) ) {
                    $new_cat_id = apply_filters( 'wpml_object_id', $get_all_category->term_id, 'product_cat', true, $default_lang );
                } else {
                    $new_cat_id = $get_all_category->term_id;
                }
                $category        = get_term_by( 'id', $new_cat_id, 'product_cat' );
                $parent_category = get_term_by( 'id', $category->parent, 'product_cat' );

                if (!empty($selected)) {
                    $selected = array_map('intval',$selected);
                    $selectedVal = is_array($selected) && !empty($selected) && in_array($new_cat_id, $selected, true) ? 'selected=selected' : '';

                    if ( $category->parent > 0 ) {
                        $html .= '<option value=' . $category->term_id . ' ' . $selectedVal . '>' . '' . $parent_category->name . '->' . $category->name . '</option>';
                    } else {
                        $html .= '<option value=' . $category->term_id . ' ' . $selectedVal . '>' . $category->name . '</option>';
                    }
                } else {
                    if ( $category->parent > 0 ) {
                        $filter_category_list[$category->term_id] = $parent_category->name . '->' . $category->name;
                    } else {
                        $filter_category_list[$category->term_id] = $category->name;
                    }
                }
            }
        }
        if(true === $json){
            return wp_json_encode($this->afrsm_pro_convert_array_to_json($filter_category_list));
        } else {
            return $html;
        }

    }

    /**
     * Get shipping class list
     *
     * @since  1.0.0
     *
     * @uses WC_Shipping::get_shipping_classes()
     *
     * @param array $selected
     *
     * @return string $html
     */
    public function afrsm_pro_get_class_options($selected = array(), $json) {

        $shipping_classes = WC()->shipping->get_shipping_classes();
        $filter_shipping_class_list = [];

        $html = '';
        if (isset($shipping_classes) && !empty($shipping_classes)) {
            foreach ($shipping_classes as $shipping_classes_key) {
                $selectedVal = !empty($selected) && in_array($shipping_classes_key->slug, $selected, true) ? 'selected=selected' : '';
                $html .= '<option value="' . esc_attr( $shipping_classes_key->slug ). '" ' . esc_attr( $selectedVal ) . '>' . esc_html( $shipping_classes_key->name ) . '</option>';
                $filter_shipping_class_list[$shipping_classes_key->slug] = $shipping_classes_key->name;
            }
        }
        if(true === $json){
            return wp_json_encode($this->afrsm_pro_convert_array_to_json($filter_shipping_class_list));
        } else {
            return $html;
        }
    }

    /**
     * Get variable product list in Shipping Method Rules
     *
     * @since  1.0.0
     *
     * @uses afrsm_pro_get_default_langugae_with_sitpress()
     * @uses wc_get_product()
     * @uses WC_Product::is_type()
     * @uses get_available_variations()
     *
     * @param string $count
     * @param array $selected
     *
     * @return string $html
     */
    public function afrsm_pro_get_varible_product_list($count = '', $selected = array(),$json=false) {
        global $sitepress;
        $default_lang = $this->afrsm_pro_get_default_langugae_with_sitpress();

        $get_all_products = new WP_Query(array(
            'post_type' => 'product',
            'post_status' => 'publish',
            'posts_per_page' => -1
        ));
        $html = '<select id="var-product-filter-' . esc_attr( $count ) . '" rel-id="' . esc_attr( $count ) . '" name="fees[product_fees_conditions_values][value_' . esc_attr( $count ) . '][]" class="afrsm_select product_fees_conditions_values multiselect2 product_fees_conditions_values_var_product" multiple="multiple">';
        if (!empty($get_all_products->posts)) {
            foreach ($get_all_products->posts as $post) {

                $_product = wc_get_product($post->ID);

                if ($_product->is_type('variable')) {
                    $variations = $_product->get_available_variations();
                    foreach ($variations as $value) {
                        if (!empty($sitepress)) {
                            $new_product_id = apply_filters('wpml_object_id', $value['variation_id'], 'product', TRUE, $default_lang);
                        } else {
                            $new_product_id = $value['variation_id'];
                        }
                        $selected = array_map('intval',$selected);
                        $selectedVal = is_array($selected) && !empty($selected) && in_array($new_product_id, $selected, true) ? 'selected=selected' : '';
                        if ('' !== $selectedVal) {
                            $html .= '<option value="' . esc_attr( $new_product_id ) . '" ' . esc_attr( $selectedVal ) . '>' . '#' . esc_html( $new_product_id ) . ' - ' . esc_html( get_the_title($new_product_id) ) . '</option>';
                        }
                    }
                }
            }
        }
        $html .= '</select>';
        if($json){
            return [];
        }
        return $html;
    }

    /**
     * Get category list in Shipping Method Rules
     *
     * @since  1.0.0
     *
     * @uses afrsm_pro_get_default_langugae_with_sitpress()
     * @uses get_categories()
     * @uses get_term_by()
     *
     * @param string $count
     * @param array $selected
     *
     * @return string $html
     */
    public function afrsm_pro_get_category_list($count = '', $selected = array(),$json=false) {
        global $sitepress;
        $default_lang = $this->afrsm_pro_get_default_langugae_with_sitpress();
        $filter_categories=[];

        $taxonomy = 'product_cat';
        $post_status = 'publish';
        $orderby = 'name';
        $hierarchical = 1;
        $empty = 0;

        $args = array(
            'post_type' => 'product',
            'post_status' => $post_status,
            'taxonomy' => $taxonomy,
            'orderby' => $orderby,
            'hierarchical' => $hierarchical,
            'hide_empty' => $empty,
            'posts_per_page' => -1
        );
        $get_all_categories = get_categories($args);
        $html = '<select rel-id="' . esc_attr( $count ) . '" name="fees[product_fees_conditions_values][value_' . esc_attr( $count ) . '][]" class="afrsm_select product_fees_conditions_values multiselect2 product_fees_conditions_values_cat_product" multiple="multiple">';
        if (isset($get_all_categories) && !empty($get_all_categories)) {
            foreach ($get_all_categories as $get_all_category) {

                if (!empty($sitepress)) {
                    $new_cat_id = apply_filters('wpml_object_id', $get_all_category->term_id, 'product_cat', TRUE, $default_lang);
                } else {
                    $new_cat_id = $get_all_category->term_id;
                }
                $selected = array_map('intval',$selected);
                $selectedVal = is_array($selected) && !empty($selected) && in_array($new_cat_id, $selected, true) ? 'selected=selected' : '';
                $category = get_term_by('id', $new_cat_id, 'product_cat');
                $parent_category = get_term_by('id', $category->parent, 'product_cat');

                if ($category->parent > 0) {
                    $html .= '<option value=' . esc_attr( $category->term_id ) . ' ' . esc_attr( $selectedVal ) . '>' . '#' . esc_html( $parent_category->name ) . '->' . esc_html( $category->name ) . '</option>';
                    $filter_categories[$category->term_id]='#'.$parent_category->name . '->' . $category->name;
                } else {
                    $html .= '<option value=' . esc_attr( $category->term_id ) . ' ' . esc_attr( $selectedVal ) . '>' . esc_html( $category->name ) . '</option>';
                    $filter_categories[$category->term_id]=$category->name;
                }
            }
        }
        $html .= '</select>';
        if($json){
            return $this->afrsm_pro_convert_array_to_json($filter_categories);
        }
        return $html;
    }

    /**
     * Get tag list in Shipping Method Rules
     *
     * @since  1.0.0
     *
     * @uses afrsm_pro_get_default_langugae_with_sitpress()
     * @uses get_term_by()
     *
     * @param string $count
     * @param array $selected
     *
     * @return string $html
     */
    public function afrsm_pro_get_tag_list($count = '', $selected = array(),$json=false) {
        global $sitepress;
        $default_lang = $this->afrsm_pro_get_default_langugae_with_sitpress();
        $filter_tags=[];

        $taxonomy = 'product_tag';
        $orderby = 'name';
        $hierarchical = 1;
        $empty = 0;

        $args = array(
            'post_type' => 'product',
            'post_status' => 'publish',
            'taxonomy' => $taxonomy,
            'orderby' => $orderby,
            'hierarchical' => $hierarchical,
            'hide_empty' => $empty,
            'posts_per_page' => -1,
            'suppress_filters' => false
        );

        $get_all_tags = get_categories($args);

        $html = '<select rel-id="' . esc_attr( $count ) . '" name="fees[product_fees_conditions_values][value_' . esc_attr( $count ) . '][]" class="afrsm_select product_fees_conditions_values multiselect2 product_fees_conditions_values_tag_product" multiple="multiple">';
        if (isset($get_all_tags) && !empty($get_all_tags)) {
            foreach ($get_all_tags as $get_all_tag) {

                if (!empty($sitepress)) {
                    $new_tag_id = apply_filters('wpml_object_id', $get_all_tag->term_id, 'product_tag', TRUE, $default_lang);
                } else {
                    $new_tag_id = $get_all_tag->term_id;
                }
                $selected = array_map('intval',$selected);
                $selectedVal = is_array($selected) && !empty($selected) && in_array($new_tag_id, $selected, true) ? 'selected=selected' : '';
                $tag = get_term_by('id', $new_tag_id, 'product_tag');

                $html .= '<option value="' . esc_attr( $tag->term_id ) . '" ' . esc_attr( $selectedVal ) . '>' . esc_html( $tag->name ) . '</option>';
                $filter_tags[$tag->term_id] = $tag->name;
            }
        }
        $html .= '</select>';
        if($json){
            return $this->afrsm_pro_convert_array_to_json($filter_tags);
        }
        return $html;
    }

    /**
     * Get sku list in Shipping Method Rules
     *
     * @since  1.0.0
     *
     * @uses get_post_meta()
     *
     * @param string $count
     * @param array $selected
     *
     * @return string $html
     */
    public function afrsm_pro_get_sku_list($count = '', $selected = array(),$json=false) {
        global $sitepress;
        $default_lang = $this->afrsm_pro_get_default_langugae_with_sitpress();
        $get_products_array = new WP_Query(array(
            'post_type' => 'product',
            'post_status' => 'publish',
            'posts_per_page' => -1
        ));
        $filter_skus = [];

        $products_array = $get_products_array->posts;

        $baselang_simple_product_ids = array();
        $baselang_variation_product_ids = array();
        if (!empty($products_array)) {
            foreach ($products_array as $get_product) {
                $_product = wc_get_product($get_product->ID);
                if ($_product->is_type('variable')) {
                    $variations = $_product->get_available_variations();
                    foreach ($variations as $value) {
                        if (!empty($sitepress)) {
                            $defaultlang_variation_product_id = apply_filters('wpml_object_id', $value['variation_id'], 'product', TRUE, $default_lang);
                        } else {
                            $defaultlang_variation_product_id = $value['variation_id'];
                        }
                        $baselang_variation_product_ids[] = $defaultlang_variation_product_id;
                    }
                }
                if ($_product->is_type('simple')) {
                    if (!empty($sitepress)) {
                        $defaultlang_simple_product_id = apply_filters('wpml_object_id', $get_product->ID, 'product', TRUE, $default_lang);
                    } else {
                        $defaultlang_simple_product_id = $get_product->ID;
                    }
                    $baselang_simple_product_ids[] = $defaultlang_simple_product_id;
                }
            }
        }
        $baselang_product_ids = array_merge($baselang_variation_product_ids, $baselang_simple_product_ids);

        $html = '<select rel-id="' . esc_attr( $count ) . '" name="fees[product_fees_conditions_values][value_' . esc_attr( $count ) . '][]" class="afrsm_select product_fees_conditions_values multiselect2 product_fees_conditions_values_sku_product" multiple="multiple">';

        if (isset($baselang_product_ids) && !empty($baselang_product_ids)) {
            foreach ($baselang_product_ids as $baselang_product_id) {
                if (!empty($baselang_product_id)) {
                    $product_sku = get_post_meta($baselang_product_id, '_sku', true);
                }
                settype($product_sku, 'string');
                $selectedVal = is_array($selected) && !empty($selected) && in_array($product_sku, $selected, true) ? 'selected=selected' : '';
                if (!empty($product_sku) || $product_sku !== '') {
                    $html .= '<option value="' . esc_attr($product_sku) . '" ' . esc_attr($selectedVal) . '>' . esc_html($product_sku) . '</option>';
                }
                $filter_skus[$product_sku] = $product_sku;
            }
        }

        $html .= '</select>';
        if($json){
            return $this->afrsm_pro_convert_array_to_json($filter_skus);
        }
        return $html;
    }

    /**
     * Get attribute list in Shipping Method Rules
     *
     * @since  1.0.0
     *
     * @param string $count
     * @param string $condition
     * @param array $selected
     *
     * @return string $html
     */
    public function afrsm_pro_get_att_term_list($count = '', $condition = '', $selected = array(), $json = false)
    {
        $att_terms = get_terms( array(
            'taxonomy' => $condition,
            'parent'   => 0,
            'hide_empty' => false,
        ) );
        $filter_attributes = [];
        $html = '<select rel-id="' . $count . '" name="fees[product_fees_conditions_values][value_' . $count . '][]" class="afrsm_select product_fees_conditions_values multiselect2 product_fees_conditions_values_att_term" multiple="multiple">';
        if (!empty($att_terms)) {
            foreach ($att_terms as $term) {
                $term_name = $term->name;
                $term_slug = $term->slug;

                $selectedVal = is_array($selected) && !empty($selected) && in_array($term_slug, $selected, true) ? 'selected=selected' : '';
                $html .= '<option value="' . $term_slug . '" ' . $selectedVal . '>' . $term_name . '</option>';
                $filter_attributes[$term_slug] = $term_name;
            }
        }
        $html .= '</select>';
        if($json){
            return $this->afrsm_pro_convert_array_to_json($filter_attributes);
        }
        return $html;
    }

    /**
     * Get user list in Shipping Method Rules
     *
     * @since  1.0.0
     *
     * @param string $count
     * @param array $selected
     *
     * @return string $html
     */
    public function afrsm_pro_get_user_list($count = '', $selected = array(), $json=false) {
        $filter_users=[];
        $get_all_users = get_users();

        $html = '<select rel-id="' . esc_attr( $count ) . '" name="fees[product_fees_conditions_values][value_' . esc_attr( $count ) . '][]" class="afrsm_select product_fees_conditions_values multiselect2 product_fees_conditions_values_user" multiple="multiple">';
        if (isset($get_all_users) && !empty($get_all_users)) {
            foreach ($get_all_users as $get_all_user) {
                $selectedVal = is_array($selected) && !empty($selected) && in_array($get_all_user->data->ID, $selected, true) ? 'selected=selected' : '';
                $html .= '<option value="' . esc_attr( $get_all_user->data->ID ) . '" ' . esc_attr( $selectedVal ) . '>' . esc_html( $get_all_user->data->user_login ) . '</option>';
                $filter_users[$get_all_user->data->ID]=$get_all_user->data->user_login;
            }
        }
        $html .= '</select>';
        if($json){
            return $this->afrsm_pro_convert_array_to_json($filter_users);
        }
        return $html;
    }

    /**
     * Get role user list in Shipping Method Rules
     *
     * @since  1.0.0
     *
     * @param string $count
     * @param array $selected
     *
     * @return string $html
     */
    public function afrsm_pro_get_user_role_list($count = '', $selected = array(),$json=false) {
        $filter_user_roles=[];
        global $wp_roles;

        $html = '<select rel-id="' . esc_attr( $count ) . '" name="fees[product_fees_conditions_values][value_' . esc_attr( $count ) . '][]" class="afrsm_select product_fees_conditions_values multiselect2 product_fees_conditions_values_role" multiple="multiple">';
        if (isset($wp_roles->roles) && !empty($wp_roles->roles)) {
            $defaultSel = !empty($selected) && in_array('guest', $selected, true) ? 'selected=selected' : '';
            $html .= '<option value="guest" ' . esc_attr( $defaultSel ) . '>' . esc_html__('Guest', 'advanced-flat-rate-shipping-for-woocommerce') . '</option>';
            $filter_user_roles['guest'] = esc_html__('Guest', 'advanced-flat-rate-shipping-for-woocommerce');
            foreach ($wp_roles->roles as $user_role_key => $get_all_role) {
                $selectedVal = is_array($selected) && !empty($selected) && in_array($user_role_key, $selected, true) ? 'selected=selected' : '';
                $html .= '<option value="' . esc_attr( $user_role_key ) . '" ' . esc_attr( $selectedVal ) . '>' . esc_html( $get_all_role['name'] ) . '</option>';
                $filter_user_roles[$user_role_key]=$get_all_role['name'];
            }
        }
        $html .= '</select>';
        if($json){
            return $this->afrsm_pro_convert_array_to_json($filter_user_roles);
        }
        return $html;
    }

    /**
     * Get coupon list in Shipping Method Rules
     *
     * @since  1.0.0
     *
     * @param string $count
     * @param array $selected
     *
     * @return string $html
     */
    public function afrsm_pro_get_coupon_list($count = '', $selected = array(),$json=false) {
        $filter_coupon_list=[];
        $get_all_coupon = new WP_Query(array(
            'post_type' => 'shop_coupon',
            'post_status' => 'publish',
            'posts_per_page' => -1
        ));
        $html = '<select rel-id="' . esc_attr( $count ) . '" name="fees[product_fees_conditions_values][value_' . esc_attr( $count ) . '][]" class="afrsm_select product_fees_conditions_values multiselect2 product_fees_conditions_values_coupon" multiple="multiple">';
        if (isset($get_all_coupon->posts) && !empty($get_all_coupon->posts)) {
            foreach ($get_all_coupon->posts as $get_all_coupon) {
                $selected = array_map('intval',$selected);
                $selectedVal = is_array($selected) && !empty($selected) && in_array($get_all_coupon->ID, $selected, true) ? 'selected=selected' : '';
                $html .= '<option value="' . esc_attr( $get_all_coupon->ID ) . '" ' . esc_attr( $selectedVal ) . '>' . esc_html( $get_all_coupon->post_title ) . '</option>';
                $filter_coupon_list[$get_all_coupon->ID]=$get_all_coupon->post_title;
            }
        }
        $html .= '</select>';
        if($json){
            return $this->afrsm_pro_convert_array_to_json($filter_coupon_list);
        }
        return $html;
    }

    /**
     * Get shipping class list in Shipping Method Rules
     *
     * @since  1.0.0
     *
     * @uses WC_Shipping::get_shipping_classes()
     *
     * @param string $count
     * @param array $selected
     *
     * @return string $html
     */
    public function afrsm_pro_get_advance_flat_rate_class($count = '', $selected = array(), $json=false) {
        $filter_rate_class=[];
        $shipping_classes = WC()->shipping->get_shipping_classes();
        $html = '<select rel-id="' . esc_attr( $count ) . '" name="fees[product_fees_conditions_values][value_' . esc_attr( $count ). '][]" class="afrsm_select product_fees_conditions_values multiselect2 product_fees_conditions_values_class" multiple="multiple">';
        if (isset($shipping_classes) && !empty($shipping_classes)) {
            foreach ($shipping_classes as $shipping_classes_key) {
                $selectedVal = !empty($selected) && in_array($shipping_classes_key->slug, $selected, true) ? 'selected=selected' : '';
                $html .= '<option value="' . esc_attr( $shipping_classes_key->slug ) . '" ' . esc_attr( $selectedVal ) . '>' . esc_html( $shipping_classes_key->name ) . '</option>';
                $filter_rate_class[$shipping_classes_key->slug]=$shipping_classes_key->name;
            }
        }
        $html .= '</select>';
        if($json){
            return $this->afrsm_pro_convert_array_to_json($filter_rate_class);
        }
        return $html;
    }

    /**
     * Get payment method in Shipping Method Rules
     *
     * @since  1.0.0
     *
     * @uses WC_Payment_Gateways::get_available_payment_gateways()
     *
     * @param string $count
     * @param array $selected
     *
     * @return string $html
     */
    public function afrsm_pro_get_payment($count = '', $selected = array(), $json=false) {
        $filter_rate_payment = [];
        $gateways = WC()->payment_gateways->get_available_payment_gateways();
        $html = '<select rel-id="' . $count . '" name="fees[product_fees_conditions_values][value_' . $count . '][]" class="afrsm_select product_fees_conditions_values multiselect2 product_fees_conditions_values_payment" multiple="multiple">';
        if (isset($gateways) && !empty($gateways)) {
            foreach( $gateways as $gateway ) {
                if( $gateway->enabled === 'yes' ) {
                    $selectedVal = !empty($selected) && in_array($gateway->id, $selected, true) ? 'selected=selected' : '';
                    $html .= '<option value="' . $gateway->id . '" ' . $selectedVal . '>' . esc_html__($gateway->title, 'advanced-flat-rate-shipping-for-woocommerce') . '</option>';
                    $filter_rate_payment[$gateway->id] = esc_html__($gateway->title, 'advanced-flat-rate-shipping-for-woocommerce');
                }
            }
        }
        $html .= '</select>';
        if($json){
            return $this->afrsm_pro_convert_array_to_json($filter_rate_payment);
        }
        return $html;
    }

    /**
     * Display product list based product specific option
     *
     * @since  1.0.0
     *
     * @uses afrsm_pro_get_default_langugae_with_sitpress()
     * @uses Advanced_Flat_Rate_Shipping_For_WooCommerce_Pro::afrsm_pro_allowed_html_tags()
     *
     * @return string $html
     */
    public function afrsm_pro_product_fees_conditions_values_product_ajax() {
        global $sitepress;

        $json = true;
        $filter_product_list=[];

        $default_lang = $this->afrsm_pro_get_default_langugae_with_sitpress();

        $request_value = filter_input(INPUT_GET,'value',FILTER_SANITIZE_STRING);
        $post_value = isset($request_value) ? sanitize_text_field($request_value) : '';

        $baselang_product_ids = array();

        function afrsm_pro_posts_where($where, $wp_query)
        {
            global $wpdb;
            $search_term = $wp_query->get('search_pro_title');
            if (!empty($search_term)) {
                $search_term_like = $wpdb->esc_like($search_term);
                $where .= ' AND ' . $wpdb->posts . '.post_title LIKE \'%' . esc_sql($search_term_like) . '%\'';
            }
            return $where;
        }

        $product_args = array(
            'post_type' => 'product',
            'posts_per_page' => -1,
            'search_pro_title' => $post_value,
            'post_status' => 'publish',
            'orderby' => 'title',
            'order' => 'ASC'
        );

        add_filter('posts_where', 'afrsm_pro_posts_where', 10, 2);
        $get_wp_query = new WP_Query($product_args);
        remove_filter('posts_where', 'afrsm_pro_posts_where', 10, 2);

        $get_all_products = $get_wp_query->posts;

        if (isset($get_all_products) && !empty($get_all_products)) {
            foreach ($get_all_products as $get_all_product) {
                if (!empty($sitepress)) {
                    $defaultlang_product_id = apply_filters('wpml_object_id', $get_all_product->ID, 'product', TRUE, $default_lang);
                } else {
                    $defaultlang_product_id = $get_all_product->ID;
                }
                $baselang_product_ids[] = $defaultlang_product_id;
            }
        }

        $html = '';
        if (isset($baselang_product_ids) && !empty($baselang_product_ids)) {
            foreach ($baselang_product_ids as $baselang_product_id) {
                $html .= '<option value="' . esc_attr( $baselang_product_id ) . '">' . '#' . esc_html( $baselang_product_id ) . ' - ' . esc_html( get_the_title($baselang_product_id) ) . '</option>';
                $filter_product_list[] = array( $baselang_product_id, get_the_title($baselang_product_id) );
            }
        }
        if($json){
            echo wp_json_encode( $filter_product_list );
            wp_die();
        }
        echo wp_kses($html, Advanced_Flat_Rate_Shipping_For_WooCommerce_Pro::afrsm_pro_allowed_html_tags());
        wp_die();
    }

    /**
     * Display variable product list based product specific option
     *
     * @since  1.0.0
     *
     * @uses afrsm_pro_get_default_langugae_with_sitpress()
     * @uses wc_get_product()
     * @uses WC_Product::is_type()
     * @uses Advanced_Flat_Rate_Shipping_For_WooCommerce_Pro::afrsm_pro_allowed_html_tags()
     *
     * @return string $html
     */
    public function afrsm_pro_product_fees_conditions_varible_values_product_ajax() {
        global $sitepress;
        $default_lang = $this->afrsm_pro_get_default_langugae_with_sitpress();
        $json = true;
        $filter_variable_product_list=[];

        $request_value = filter_input(INPUT_GET,'value',FILTER_SANITIZE_STRING);
        $post_value = isset($request_value) ? sanitize_text_field($request_value) : '';

        $baselang_product_ids = array();

        function wcpfc_posts_wheres($where, &$wp_query)
        {
            global $wpdb;
            $search_term = $wp_query->get('search_pro_title');
            if (!empty($search_term)) {
                $search_term_like = $wpdb->esc_like($search_term);
                $where .= ' AND ' . $wpdb->posts . '.post_title LIKE \'%' . esc_sql($search_term_like) . '%\'';
            }
            return $where;
        }

        $product_args = array(
            'post_type' => 'product',
            'posts_per_page' => -1,
            'search_pro_title' => $post_value,
            'post_status' => 'publish',
            'orderby' => 'title',
            'order' => 'ASC'
        );
        $get_all_products = new WP_Query($product_args);

        if (!empty($get_all_products)) {
            foreach ($get_all_products->posts as $get_all_product) {
                $_product = wc_get_product($get_all_product->ID);
                if ($_product->is_type('variable')) {
                    $variations = $_product->get_available_variations();
                    foreach ($variations as $value) {
                        if (!empty($sitepress)) {
                            $defaultlang_product_id = apply_filters('wpml_object_id', $value['variation_id'], 'product', TRUE, $default_lang);
                        } else {
                            $defaultlang_product_id = $value['variation_id'];
                        }
                        $baselang_product_ids[] = $defaultlang_product_id;
                    }
                }
            }
        }
        $html = '';
        if (isset($baselang_product_ids) && !empty($baselang_product_ids)) {
            foreach ($baselang_product_ids as $baselang_product_id) {
                $html .= '<option value="' . esc_attr( $baselang_product_id ) . '">' . '#' . esc_html( $baselang_product_id ) . ' - ' . esc_html( get_the_title($baselang_product_id) ) . '</option>';
                $filter_variable_product_list[] = array( $baselang_product_id, get_the_title($baselang_product_id) );
            }
        }
        if($json){
            echo wp_json_encode( $filter_variable_product_list );
            wp_die();
        }
        echo wp_kses($html, Advanced_Flat_Rate_Shipping_For_WooCommerce_Pro::afrsm_pro_allowed_html_tags());
        wp_die();
    }

    /**
     * Display simple and variable product list based product specific option in Advance Pricing Rules
     *
     * @since  3.4
     *
     * @uses afrsm_pro_get_default_langugae_with_sitpress()
     * @uses wc_get_product()
     * @uses WC_Product::is_type()
     * @uses get_available_variations()
     * @uses Advanced_Flat_Rate_Shipping_For_WooCommerce_Pro::afrsm_pro_allowed_html_tags()
     *
     * @return string $html
     */
    public function afrsm_pro_simple_and_variation_product_list_ajax() {
        global $sitepress;
        $default_lang = $this->afrsm_pro_get_default_langugae_with_sitpress();
        $json = true;
        $filter_product_list = [];

        $request_value = filter_input(INPUT_GET,'value',FILTER_SANITIZE_STRING);
        $post_value = isset($request_value) ? sanitize_text_field($request_value) : '';

        $baselang_simple_product_ids = array();
        $baselang_variation_product_ids = array();

        function afrsm_pro_posts_where($where, $wp_query)
        {
            global $wpdb;
            $search_term = $wp_query->get('search_pro_title');
            if (!empty($search_term)) {
                $search_term_like = $wpdb->esc_like($search_term);
                $where .= ' AND ' . $wpdb->posts . '.post_title LIKE \'%' . esc_sql($search_term_like) . '%\'';
            }
            return $where;
        }

        $product_args = array(
            'post_type' => 'product',
            'posts_per_page' => -1,
            'search_pro_title' => $post_value,
            'post_status' => 'publish',
            'orderby' => 'title',
            'order' => 'ASC'
        );

        add_filter('posts_where', 'afrsm_pro_posts_where', 10, 2);
        $get_wp_query = new WP_Query($product_args);
        remove_filter('posts_where', 'afrsm_pro_posts_where', 10, 2);

        $get_all_products = $get_wp_query->posts;

        if (isset($get_all_products) && !empty($get_all_products)) {
            foreach ($get_all_products as $get_all_product) {
                $_product = wc_get_product($get_all_product->ID);
                if ($_product->is_type('variable')) {
                    $variations = $_product->get_available_variations();
                    foreach ($variations as $value) {
                        if (!empty($sitepress)) {
                            $defaultlang_variation_product_id = apply_filters('wpml_object_id', $value['variation_id'], 'product', TRUE, $default_lang);
                        } else {
                            $defaultlang_variation_product_id = $value['variation_id'];
                        }
                        $baselang_variation_product_ids[] = $defaultlang_variation_product_id;
                    }
                }
                if ($_product->is_type('simple')) {
                    if (!empty($sitepress)) {
                        $defaultlang_simple_product_id = apply_filters('wpml_object_id', $get_all_product->ID, 'product', TRUE, $default_lang);
                    } else {
                        $defaultlang_simple_product_id = $get_all_product->ID;
                    }
                    $baselang_simple_product_ids[] = $defaultlang_simple_product_id;
                }
            }
        }

        $baselang_product_ids = array_merge($baselang_variation_product_ids, $baselang_simple_product_ids);

        $html = '';
        if (isset($baselang_product_ids) && !empty($baselang_product_ids)) {
            foreach ($baselang_product_ids as $baselang_product_id) {
                $html .= '<option value="' . esc_attr( $baselang_product_id ) . '">' . '#' . esc_html( $baselang_product_id ) . ' - ' . esc_html( get_the_title($baselang_product_id) ) . '</option>';
                $filter_product_list[] = array( $baselang_product_id, get_the_title($baselang_product_id) );
            }
        }
        if($json){
            echo wp_json_encode( $filter_product_list );
            wp_die();
        }
        echo wp_kses($html, Advanced_Flat_Rate_Shipping_For_WooCommerce_Pro::afrsm_pro_allowed_html_tags());
        wp_die();
    }

    /**
     * Delete multiple shipping method
     *
     * @since  1.0.0
     *
     * @uses wp_delete_post()
     *
     * @return string $result
     */
    public function afrsm_pro_wc_multiple_delete_shipping_method() {
        check_ajax_referer( 'dsm_nonce', 'nonce' );

        $result = 0;
        $get_allVals   = filter_input(INPUT_GET, 'allVals', FILTER_SANITIZE_NUMBER_INT, FILTER_REQUIRE_ARRAY);
        $allVals = !empty($get_allVals) ? array_map( 'sanitize_text_field', wp_unslash($get_allVals) ) : array();
        if (!empty($allVals)) {
            foreach ($allVals as $val) {
                wp_delete_post($val);
                $result = 1;
            }
        }
        echo (int)$result;
        wp_die();
    }

    /**
     * Count total shipping method
     *
     * @return int $count_method
     * @since    3.5
     *
     */
    public static function afrsm_pro_sm_count_method() {
        $shipping_method_args = array(
            'post_type'      => self::afrsm_shipping_post_type,
            'post_status'    => array( 'publish', 'draft' ),
            'posts_per_page' => - 1,
            'orderby'        => 'ID',
            'order'          => 'DESC',
        );
        $sm_post_query        = new WP_Query( $shipping_method_args );
        $shipping_method_list = $sm_post_query->posts;

        return count( $shipping_method_list );
    }

    /**
     * Save shipping method
     *
     * @since  1.0.0
     *
     * @uses update_post_meta()
     *
     * @param array $post
     *
     * @return bool false if post is empty otherwise it will redirect to shipping method list
     */
    function afrsm_pro_fees_conditions_save($post) {
        global $sitepress;

        if (empty($post)) {
            return false;
        }

        $post_type = filter_input( INPUT_POST, 'post_type', FILTER_SANITIZE_STRING );
        $afrsm_pro_conditions_save = filter_input( INPUT_POST, 'afrsm_pro_conditions_save', FILTER_SANITIZE_STRING );

        if (isset($post_type) && self::afrsm_shipping_post_type === sanitize_text_field($post['post_type']) &&
            wp_verify_nonce( sanitize_text_field( $afrsm_pro_conditions_save ),'afrsm_pro_save_action')) {

            $method_id                              = filter_input( INPUT_POST, 'fee_post_id', FILTER_SANITIZE_NUMBER_INT );
            $fees                                   = filter_input( INPUT_POST, 'fees', FILTER_SANITIZE_STRING, FILTER_REQUIRE_ARRAY );
            $sm_status                              = filter_input( INPUT_POST, 'sm_status', FILTER_SANITIZE_STRING );
            $fee_settings_product_fee_title         = filter_input( INPUT_POST, 'fee_settings_product_fee_title', FILTER_SANITIZE_STRING );
            $get_condition_key                      = filter_input( INPUT_POST, 'condition_key', FILTER_SANITIZE_STRING, FILTER_REQUIRE_ARRAY );
            $get_how_to_apply                       = filter_input( INPUT_POST, 'how_to_apply', FILTER_SANITIZE_STRING );
            $get_sm_product_cost                    = filter_input( INPUT_POST, 'sm_product_cost', FILTER_SANITIZE_STRING );
            $get_sm_fee_chk_qty_price               = filter_input( INPUT_POST, 'sm_fee_chk_qty_price', FILTER_SANITIZE_STRING );
            $get_sm_fee_per_qty                     = filter_input( INPUT_POST, 'sm_fee_per_qty', FILTER_SANITIZE_STRING );
            $get_sm_extra_product_cost              = filter_input( INPUT_POST, 'sm_extra_product_cost', FILTER_SANITIZE_STRING );
            $get_sm_tooltip_desc                    = filter_input( INPUT_POST, 'sm_tooltip_desc', FILTER_SANITIZE_STRING );
            $get_sm_select_taxable                  = filter_input( INPUT_POST, 'sm_select_taxable', FILTER_SANITIZE_STRING );
            $get_sm_estimation_delivery             = filter_input( INPUT_POST, 'sm_estimation_delivery', FILTER_SANITIZE_STRING );
            $get_sm_start_date                      = filter_input( INPUT_POST, 'sm_start_date', FILTER_SANITIZE_STRING );
            $get_sm_end_date                        = filter_input( INPUT_POST, 'sm_end_date', FILTER_SANITIZE_STRING );
            $get_sm_extra_cost                      = filter_input( INPUT_POST, 'sm_extra_cost', FILTER_SANITIZE_STRING, FILTER_REQUIRE_ARRAY );
            $get_sm_extra_cost_calculation_type     = filter_input( INPUT_POST, 'sm_extra_cost_calculation_type', FILTER_SANITIZE_STRING );

            $get_ap_rule_status                     = filter_input( INPUT_POST, 'ap_rule_status', FILTER_SANITIZE_STRING );

            $get_cost_on_product_status             = filter_input( INPUT_POST, 'cost_on_product_status', FILTER_SANITIZE_STRING );
            $get_cost_on_product_weight_status      = filter_input( INPUT_POST, 'cost_on_product_weight_status', FILTER_SANITIZE_STRING );
            $get_cost_on_product_subtotal_status    = filter_input( INPUT_POST, 'cost_on_product_subtotal_status', FILTER_SANITIZE_STRING );

            $get_cost_on_category_status            = filter_input( INPUT_POST, 'cost_on_category_status', FILTER_SANITIZE_STRING );
            $get_cost_on_category_weight_status     = filter_input( INPUT_POST, 'cost_on_category_weight_status', FILTER_SANITIZE_STRING );
            $get_cost_on_category_subtotal_status   = filter_input( INPUT_POST, 'cost_on_category_subtotal_status', FILTER_SANITIZE_STRING );

            $get_cost_on_total_cart_qty_status      = filter_input( INPUT_POST, 'cost_on_total_cart_qty_status', FILTER_SANITIZE_STRING );
            $get_cost_on_total_cart_weight_status   = filter_input( INPUT_POST, 'cost_on_total_cart_weight_status', FILTER_SANITIZE_STRING );
            $get_cost_on_total_cart_subtotal_status = filter_input( INPUT_POST, 'cost_on_total_cart_subtotal_status', FILTER_SANITIZE_STRING );
            $get_cost_on_shipping_class_subtotal_status = filter_input( INPUT_POST, 'cost_on_shipping_class_subtotal_status', FILTER_SANITIZE_STRING );

            $get_sm_select_day_of_week              = filter_input( INPUT_POST, 'sm_select_day_of_week', FILTER_SANITIZE_STRING, FILTER_REQUIRE_ARRAY );
            $get_sm_time_from                       = filter_input( INPUT_POST, 'sm_time_from', FILTER_SANITIZE_STRING );
            $get_sm_time_to                         = filter_input( INPUT_POST, 'sm_time_to', FILTER_SANITIZE_STRING );
            $get_cost_rule_match                    = filter_input( INPUT_POST, 'cost_rule_match', FILTER_SANITIZE_STRING, FILTER_REQUIRE_ARRAY );
            $get_main_rule_condition                = filter_input( INPUT_POST, 'main_rule_condition', FILTER_SANITIZE_STRING );

            $get_fee_settings_unique_shipping_title                = filter_input( INPUT_POST, 'fee_settings_unique_shipping_title', FILTER_SANITIZE_STRING );

            $fee_settings_unique_shipping_title                   = isset( $get_fee_settings_unique_shipping_title ) ? sanitize_text_field( $get_fee_settings_unique_shipping_title ) : '';
            $how_to_apply                   = isset( $get_how_to_apply ) ? sanitize_text_field( $get_how_to_apply ) : '';
            $sm_product_cost                = isset( $get_sm_product_cost ) ? sanitize_text_field( $get_sm_product_cost ) : '';
            $sm_fee_chk_qty_price           = isset( $get_sm_fee_chk_qty_price ) ? sanitize_text_field( $get_sm_fee_chk_qty_price ) : '';
            $sm_fee_per_qty                 = isset( $get_sm_fee_per_qty ) ? sanitize_text_field( $get_sm_fee_per_qty ) : '';
            $sm_extra_product_cost          = isset( $get_sm_extra_product_cost ) ? sanitize_text_field( $get_sm_extra_product_cost ) : '';
            $sm_tooltip_desc                = isset( $get_sm_tooltip_desc ) ? sanitize_textarea_field( $get_sm_tooltip_desc ) : '';
            $sm_select_taxable              = isset( $get_sm_select_taxable ) ? sanitize_text_field( $get_sm_select_taxable ) : '';
            $sm_estimation_delivery         = isset( $get_sm_estimation_delivery ) ? sanitize_text_field( $get_sm_estimation_delivery ) : '';
            $sm_start_date                  = isset( $get_sm_start_date ) ? sanitize_text_field( $get_sm_start_date ) : '';
            $sm_end_date                    = isset( $get_sm_end_date ) ? sanitize_text_field( $get_sm_end_date ) : '';
            $sm_extra_cost                  = isset( $get_sm_extra_cost ) ? array_map( 'sanitize_text_field', $get_sm_extra_cost ) : array();
            $sm_extra_cost_calculation_type = isset( $get_sm_extra_cost_calculation_type ) ? sanitize_text_field( $get_sm_extra_cost_calculation_type ) : '';

            $ap_rule_status                     = isset( $get_ap_rule_status ) ? sanitize_text_field( $get_ap_rule_status ) : "off";
            $cost_on_product_status                 = isset( $get_cost_on_product_status ) ? sanitize_text_field( $get_cost_on_product_status ) : 'off';
            $cost_on_product_weight_status          = isset( $get_cost_on_product_weight_status ) ? sanitize_text_field( $get_cost_on_product_weight_status ) : 'off';
            $cost_on_product_subtotal_status        = isset( $get_cost_on_product_subtotal_status ) ? sanitize_text_field( $get_cost_on_product_subtotal_status ) : 'off';

            $cost_on_category_status                = isset( $get_cost_on_category_status ) ? sanitize_text_field( $get_cost_on_category_status ) : 'off';
            $cost_on_category_weight_status         = isset( $get_cost_on_category_weight_status ) ? sanitize_text_field( $get_cost_on_category_weight_status ) : 'off';
            $cost_on_category_subtotal_status       = isset( $get_cost_on_category_subtotal_status ) ? sanitize_text_field( $get_cost_on_category_subtotal_status ) : 'off';

            $cost_on_total_cart_qty_status          = isset( $get_cost_on_total_cart_qty_status ) ? sanitize_text_field( $get_cost_on_total_cart_qty_status ) : 'off';
            $cost_on_total_cart_weight_status       = isset( $get_cost_on_total_cart_weight_status ) ? sanitize_text_field( $get_cost_on_total_cart_weight_status ) : 'off';
            $cost_on_total_cart_subtotal_status     = isset( $get_cost_on_total_cart_subtotal_status ) ? sanitize_text_field( $get_cost_on_total_cart_subtotal_status ) : 'off';
            $cost_on_shipping_class_subtotal_status     = isset( $get_cost_on_shipping_class_subtotal_status ) ? sanitize_text_field( $get_cost_on_shipping_class_subtotal_status ) : 'off';

            $sm_select_day_of_week = isset( $get_sm_select_day_of_week ) ? array_map( 'sanitize_text_field', $get_sm_select_day_of_week ) : array();
            $sm_time_from          = isset( $get_sm_time_from ) ? sanitize_text_field( $get_sm_time_from ) : '';
            $sm_time_to            = isset( $get_sm_time_to ) ? sanitize_text_field( $get_sm_time_to ) : '';

            $cost_rule_match                        = isset( $get_cost_rule_match ) ? array_map( 'sanitize_text_field', $get_cost_rule_match ) : array();
            $main_rule_condition                    = isset( $get_main_rule_condition ) ? sanitize_text_field( $get_main_rule_condition ) : '';

            $shipping_method_count = self::afrsm_pro_sm_count_method();

            settype( $method_id, 'integer' );

            if ( isset( $sm_status ) ) {
                $post_status = 'publish';
            } else {
                $post_status = 'draft';
            }

            if ( '' !== $method_id && 0 !== $method_id ) {
                $fee_post  = array(
                    'ID'          => $method_id,
                    'post_title'  => sanitize_text_field( $fee_settings_product_fee_title ),
                    'post_status' => $post_status,
                    'menu_order'  => $shipping_method_count + 1,
                    'post_type'   => self::afrsm_shipping_post_type,
                );
                $method_id = wp_update_post( $fee_post );
            } else {
                $fee_post  = array(
                    'post_title'  => sanitize_text_field( $fee_settings_product_fee_title ),
                    'post_status' => $post_status,
                    'menu_order'  => $shipping_method_count + 1,
                    'post_type'   => self::afrsm_shipping_post_type,
                );
                $method_id = wp_insert_post( $fee_post );
            }

            if ( '' !== $method_id && 0 !== $method_id ) {
                if ( $method_id > 0 ) {
                    $feesArray                  = array();
                    $ap_product_arr             = array();
                    $ap_product_weight_arr      = array();
                    $ap_product_subtotal_arr    = array();
                    $ap_category_arr            = array();
                    $ap_category_weight_arr     = array();
                    $ap_category_subtotal_arr   = array();
                    $ap_total_cart_qty_arr      = array();
                    $ap_total_cart_weight_arr   = array();
                    $ap_total_cart_subtotal_arr = array();
                    $ap_shipping_class_subtotal_arr = array();
                    $ap_class_arr               = array();
                    $conditions_values_array    = array();

                    $condition_key     = isset( $get_condition_key ) ? $get_condition_key : array();
                    $fees_conditions   = $fees['product_fees_conditions_condition'];
                    $conditions_is     = $fees['product_fees_conditions_is'];
                    $conditions_values = isset( $fees['product_fees_conditions_values'] ) && ! empty( $fees['product_fees_conditions_values'] ) ? $fees['product_fees_conditions_values'] : array();
                    $size              = count( $fees_conditions );

                    foreach ( array_keys( $condition_key ) as $key ) {
                        if ( ! array_key_exists( $key, $conditions_values ) ) {
                            $conditions_values[ $key ] = array();
                        }
                    }

                    uksort( $conditions_values, 'strnatcmp' );
                    foreach ( $conditions_values as $v ) {
                        $conditions_values_array[] = $v;
                    }
                    for ( $i = 0; $i < $size; $i ++ ) {
                        $feesArray[] = array(
                            'product_fees_conditions_condition' => $fees_conditions[ $i ],
                            'product_fees_conditions_is'        => $conditions_is[ $i ],
                            'product_fees_conditions_values'    => $conditions_values_array[ $i ]
                        );
                    }

                    //qty for Multiple product
                    if ( isset( $fees['ap_product_fees_conditions_condition'] ) ) {
                        $fees_products         = $fees['ap_product_fees_conditions_condition'];
                        $fees_ap_prd_min_qty   = $fees['ap_fees_ap_prd_min_qty'];
                        $fees_ap_prd_max_qty   = $fees['ap_fees_ap_prd_max_qty'];
                        $fees_ap_price_product = $fees['ap_fees_ap_price_product'];

                        $prd_arr = array();
                        foreach ( $fees_products as $fees_prd_val ) {
                            $prd_arr[] = $fees_prd_val;
                        }

                        $size_product_cond = count( $fees_products );

                        if ( ! empty( $size_product_cond ) && $size_product_cond > 0 ):
                            for ( $product_cnt = 0; $product_cnt < $size_product_cond; $product_cnt ++ ) {
                                foreach ( $prd_arr as $prd_key => $prd_val ) {
                                    if ( $prd_key === $product_cnt ) {
                                        $ap_product_arr[] = array(
                                            'ap_fees_products'         => $prd_val,
                                            'ap_fees_ap_prd_min_qty'   => $fees_ap_prd_min_qty[ $product_cnt ],
                                            'ap_fees_ap_prd_max_qty'   => $fees_ap_prd_max_qty[ $product_cnt ],
                                            'ap_fees_ap_price_product' => $fees_ap_price_product[ $product_cnt ]
                                        );
                                    }
                                }
                            }
                        endif;
                    }

                    //product subtotal
                    if ( isset( $fees['ap_product_subtotal_fees_conditions_condition'] ) ) {
                        $fees_product_subtotal            = $fees['ap_product_subtotal_fees_conditions_condition'];
                        $fees_ap_product_subtotal_min_qty = $fees['ap_fees_ap_product_subtotal_min_subtotal'];
                        $fees_ap_product_subtotal_max_qty = $fees['ap_fees_ap_product_subtotal_max_subtotal'];
                        $fees_ap_product_subtotal_price   = $fees['ap_fees_ap_price_product_subtotal'];

                        $product_subtotal_arr = array();
                        foreach ( $fees_product_subtotal as $fees_product_subtotal_val ) {
                            $product_subtotal_arr[] = $fees_product_subtotal_val;
                        }
                        $size_product_subtotal_cond = count( $fees_product_subtotal );

                        if ( ! empty( $size_product_subtotal_cond ) && $size_product_subtotal_cond > 0 ):
                            for ( $product_subtotal_cnt = 0; $product_subtotal_cnt < $size_product_subtotal_cond; $product_subtotal_cnt ++ ) {
                                if ( ! empty( $product_subtotal_arr ) && '' !== $product_subtotal_arr ) {
                                    foreach ( $product_subtotal_arr as $product_subtotal_key => $product_subtotal_val ) {
                                        if ( $product_subtotal_key === $product_subtotal_cnt ) {
                                            $ap_product_subtotal_arr[] = array(
                                                'ap_fees_product_subtotal'            => $product_subtotal_val,
                                                'ap_fees_ap_product_subtotal_min_subtotal' => $fees_ap_product_subtotal_min_qty[ $product_subtotal_cnt ],
                                                'ap_fees_ap_product_subtotal_max_subtotal' => $fees_ap_product_subtotal_max_qty[ $product_subtotal_cnt ],
                                                'ap_fees_ap_price_product_subtotal' => $fees_ap_product_subtotal_price[ $product_subtotal_cnt ]
                                            );
                                        }
                                    }
                                }
                            }
                        endif;
                    }

                    //qty for Multiple category
                    if ( isset( $fees['ap_category_fees_conditions_condition'] ) ) {
                        $fees_categories        = $fees['ap_category_fees_conditions_condition'];
                        $fees_ap_cat_min_qty    = $fees['ap_fees_ap_cat_min_qty'];
                        $fees_ap_cat_max_qty    = $fees['ap_fees_ap_cat_max_qty'];
                        $fees_ap_price_category = $fees['ap_fees_ap_price_category'];

                        $cat_arr = array();
                        foreach ( $fees_categories as $fees_cat_val ) {
                            $cat_arr[] = $fees_cat_val;
                        }
                        $size_category_cond = count( $fees_categories );

                        if ( ! empty( $size_category_cond ) && $size_category_cond > 0 ):
                            for ( $category_cnt = 0; $category_cnt < $size_category_cond; $category_cnt ++ ) {
                                if ( ! empty( $cat_arr ) && '' !== $cat_arr ) {
                                    foreach ( $cat_arr as $cat_key => $cat_val ) {
                                        if ( $cat_key === $category_cnt ) {
                                            $ap_category_arr[] = array(
                                                'ap_fees_categories'        => $cat_val,
                                                'ap_fees_ap_cat_min_qty'    => $fees_ap_cat_min_qty[ $category_cnt ],
                                                'ap_fees_ap_cat_max_qty'    => $fees_ap_cat_max_qty[ $category_cnt ],
                                                'ap_fees_ap_price_category' => $fees_ap_price_category[ $category_cnt ]
                                            );
                                        }
                                    }
                                }
                            }
                        endif;
                    }

                    //category subtotal
                    if ( isset( $fees['ap_category_subtotal_fees_conditions_condition'] ) ) {
                        $fees_category_subtotal            = $fees['ap_category_subtotal_fees_conditions_condition'];
                        $fees_ap_category_subtotal_min_qty = $fees['ap_fees_ap_category_subtotal_min_subtotal'];
                        $fees_ap_category_subtotal_max_qty = $fees['ap_fees_ap_category_subtotal_max_subtotal'];
                        $fees_ap_price_category_subtotal   = $fees['ap_fees_ap_price_category_subtotal'];

                        $category_subtotal_arr = array();
                        foreach ( $fees_category_subtotal as $fees_category_subtotal_val ) {
                            $category_subtotal_arr[] = $fees_category_subtotal_val;
                        }
                        $size_category_subtotal_cond = count( $fees_category_subtotal );

                        if ( ! empty( $size_category_subtotal_cond ) && $size_category_subtotal_cond > 0 ):
                            for ( $category_subtotal_cnt = 0; $category_subtotal_cnt < $size_category_subtotal_cond; $category_subtotal_cnt ++ ) {
                                if ( ! empty( $category_subtotal_arr ) && '' !== $category_subtotal_arr ) {
                                    foreach ( $category_subtotal_arr as $category_subtotal_key => $category_subtotal_val ) {
                                        if ( $category_subtotal_key === $category_subtotal_cnt ) {
                                            $ap_category_subtotal_arr[] = array(
                                                'ap_fees_category_subtotal'          => $category_subtotal_val,
                                                'ap_fees_ap_category_subtotal_min_subtotal' => $fees_ap_category_subtotal_min_qty[ $category_subtotal_cnt ],
                                                'ap_fees_ap_category_subtotal_max_subtotal' => $fees_ap_category_subtotal_max_qty[ $category_subtotal_cnt ],
                                                'ap_fees_ap_price_category_subtotal'   => $fees_ap_price_category_subtotal[ $category_subtotal_cnt ]
                                            );
                                        }
                                    }
                                }
                            }
                        endif;
                    }

                    //qty for total cart qty
                    if ( isset( $fees['ap_total_cart_qty_fees_conditions_condition'] ) ) {
                        $fees_total_cart_qty            = $fees['ap_total_cart_qty_fees_conditions_condition'];
                        $fees_ap_total_cart_qty_min_qty = $fees['ap_fees_ap_total_cart_qty_min_qty'];
                        $fees_ap_total_cart_qty_max_qty = $fees['ap_fees_ap_total_cart_qty_max_qty'];
                        $fees_ap_price_total_cart_qty   = $fees['ap_fees_ap_price_total_cart_qty'];

                        $total_cart_qty_arr = array();
                        foreach ( $fees_total_cart_qty as $fees_total_cart_qty_val ) {
                            $total_cart_qty_arr[] = $fees_total_cart_qty_val;
                        }
                        $size_total_cart_qty_cond = count( $fees_total_cart_qty );

                        if ( ! empty( $size_total_cart_qty_cond ) && $size_total_cart_qty_cond > 0 ):
                            for ( $total_cart_qty_cnt = 0; $total_cart_qty_cnt < $size_total_cart_qty_cond; $total_cart_qty_cnt ++ ) {
                                if ( ! empty( $total_cart_qty_arr ) && '' !== $total_cart_qty_arr ) {
                                    foreach ( $total_cart_qty_arr as $total_cart_qty_key => $total_cart_qty_val ) {
                                        if ( $total_cart_qty_key === $total_cart_qty_cnt ) {
                                            $ap_total_cart_qty_arr[] = array(
                                                'ap_fees_total_cart_qty'            => $total_cart_qty_val,
                                                'ap_fees_ap_total_cart_qty_min_qty' => $fees_ap_total_cart_qty_min_qty[ $total_cart_qty_cnt ],
                                                'ap_fees_ap_total_cart_qty_max_qty' => $fees_ap_total_cart_qty_max_qty[ $total_cart_qty_cnt ],
                                                'ap_fees_ap_price_total_cart_qty'   => $fees_ap_price_total_cart_qty[ $total_cart_qty_cnt ]
                                            );
                                        }
                                    }
                                }
                            }
                        endif;
                    }

                    //product weight
                    if ( isset( $fees['ap_product_weight_fees_conditions_condition'] ) ) {
                        $fees_product_weight            = $fees['ap_product_weight_fees_conditions_condition'];
                        $fees_ap_product_weight_min_qty = $fees['ap_fees_ap_product_weight_min_weight'];
                        $fees_ap_product_weight_max_qty = $fees['ap_fees_ap_product_weight_max_weight'];
                        $fees_ap_price_product_weight   = $fees['ap_fees_ap_price_product_weight'];

                        $product_weight_arr = array();
                        foreach ( $fees_product_weight as $fees_product_weight_val ) {
                            $product_weight_arr[] = $fees_product_weight_val;
                        }
                        $size_product_weight_cond = count( $fees_product_weight );

                        if ( ! empty( $size_product_weight_cond ) && $size_product_weight_cond > 0 ):
                            for ( $product_weight_cnt = 0; $product_weight_cnt < $size_product_weight_cond; $product_weight_cnt ++ ) {
                                if ( ! empty( $product_weight_arr ) && '' !== $product_weight_arr ) {
                                    foreach ( $product_weight_arr as $product_weight_key => $product_weight_val ) {
                                        if ( $product_weight_key === $product_weight_cnt ) {
                                            $ap_product_weight_arr[] = array(
                                                'ap_fees_product_weight'            => $product_weight_val,
                                                'ap_fees_ap_product_weight_min_qty' => $fees_ap_product_weight_min_qty[ $product_weight_cnt ],
                                                'ap_fees_ap_product_weight_max_qty' => $fees_ap_product_weight_max_qty[ $product_weight_cnt ],
                                                'ap_fees_ap_price_product_weight'   => $fees_ap_price_product_weight[ $product_weight_cnt ]
                                            );
                                        }
                                    }
                                }
                            }
                        endif;
                    }

                    //category weight
                    if ( isset( $fees['ap_category_weight_fees_conditions_condition'] ) ) {
                        $fees_category_weight            = $fees['ap_category_weight_fees_conditions_condition'];
                        $fees_ap_category_weight_min_qty = $fees['ap_fees_ap_category_weight_min_weight'];
                        $fees_ap_category_weight_max_qty = $fees['ap_fees_ap_category_weight_max_weight'];
                        $fees_ap_price_category_weight   = $fees['ap_fees_ap_price_category_weight'];

                        $category_weight_arr = array();
                        foreach ( $fees_category_weight as $fees_category_weight_val ) {
                            $category_weight_arr[] = $fees_category_weight_val;
                        }
                        $size_category_weight_cond = count( $fees_category_weight );

                        if ( ! empty( $size_category_weight_cond ) && $size_category_weight_cond > 0 ):
                            for ( $category_weight_cnt = 0; $category_weight_cnt < $size_category_weight_cond; $category_weight_cnt ++ ) {
                                if ( ! empty( $category_weight_arr ) && '' !== $category_weight_arr ) {
                                    foreach ( $category_weight_arr as $category_weight_key => $category_weight_val ) {
                                        if ( $category_weight_key === $category_weight_cnt ) {
                                            $ap_category_weight_arr[] = array(
                                                'ap_fees_categories_weight'          => $category_weight_val,
                                                'ap_fees_ap_category_weight_min_qty' => $fees_ap_category_weight_min_qty[ $category_weight_cnt ],
                                                'ap_fees_ap_category_weight_max_qty' => $fees_ap_category_weight_max_qty[ $category_weight_cnt ],
                                                'ap_fees_ap_price_category_weight'   => $fees_ap_price_category_weight[ $category_weight_cnt ]
                                            );
                                        }
                                    }
                                }
                            }
                        endif;
                    }

                    //Total cart weight
                    if ( isset( $fees['ap_total_cart_weight_fees_conditions_condition'] ) ) {
                        $fees_total_cart_weight               = $fees['ap_total_cart_weight_fees_conditions_condition'];
                        $fees_ap_total_cart_weight_min_weight = $fees['ap_fees_ap_total_cart_weight_min_weight'];
                        $fees_ap_total_cart_weight_max_weight = $fees['ap_fees_ap_total_cart_weight_max_weight'];
                        $fees_ap_price_total_cart_weight      = $fees['ap_fees_ap_price_total_cart_weight'];

                        $total_cart_weight_arr = array();
                        foreach ( $fees_total_cart_weight as $fees_total_cart_weight_val ) {
                            $total_cart_weight_arr[] = $fees_total_cart_weight_val;
                        }
                        $size_total_cart_weight_cond = count( $fees_total_cart_weight );

                        if ( ! empty( $size_total_cart_weight_cond ) && $size_total_cart_weight_cond > 0 ):
                            for ( $total_cart_weight_cnt = 0; $total_cart_weight_cnt < $size_total_cart_weight_cond; $total_cart_weight_cnt ++ ) {
                                if ( ! empty( $total_cart_weight_arr ) && '' !== $total_cart_weight_arr ) {
                                    foreach ( $total_cart_weight_arr as $total_cart_weight_key => $total_cart_weight_val ) {
                                        if ( $total_cart_weight_key === $total_cart_weight_cnt ) {
                                            $ap_total_cart_weight_arr[] = array(
                                                'ap_fees_total_cart_weight'               => $total_cart_weight_val,
                                                'ap_fees_ap_total_cart_weight_min_weight' => $fees_ap_total_cart_weight_min_weight[ $total_cart_weight_cnt ],
                                                'ap_fees_ap_total_cart_weight_max_weight' => $fees_ap_total_cart_weight_max_weight[ $total_cart_weight_cnt ],
                                                'ap_fees_ap_price_total_cart_weight'      => $fees_ap_price_total_cart_weight[ $total_cart_weight_cnt ]
                                            );
                                        }
                                    }
                                }
                            }
                        endif;
                    }

                    //Cart subtotal
                    if ( isset( $fees['ap_total_cart_subtotal_fees_conditions_condition'] ) ) {
                        $fees_total_cart_subtotal                 = $fees['ap_total_cart_subtotal_fees_conditions_condition'];
                        $fees_ap_total_cart_subtotal_min_subtotal = $fees['ap_fees_ap_total_cart_subtotal_min_subtotal'];
                        $fees_ap_total_cart_subtotal_max_subtotal = $fees['ap_fees_ap_total_cart_subtotal_max_subtotal'];
                        $fees_ap_price_total_cart_subtotal        = $fees['ap_fees_ap_price_total_cart_subtotal'];

                        $total_cart_subtotal_arr = array();
                        foreach ( $fees_total_cart_subtotal as $total_cart_subtotal_key => $total_cart_subtotal_val ) {
                            $total_cart_subtotal_arr[] = $total_cart_subtotal_val;
                        }
                        $size_total_cart_subtotal_cond = count( $fees_total_cart_subtotal );

                        if ( ! empty( $size_total_cart_subtotal_cond ) && $size_total_cart_subtotal_cond > 0 ):
                            for ( $total_cart_subtotal_cnt = 0; $total_cart_subtotal_cnt < $size_total_cart_subtotal_cond; $total_cart_subtotal_cnt ++ ) {
                                if ( ! empty( $total_cart_subtotal_arr ) && $total_cart_subtotal_arr !== '' ) {
                                    foreach ( $total_cart_subtotal_arr as $total_cart_subtotal_key => $total_cart_subtotal_val ) {
                                        if ( $total_cart_subtotal_key === $total_cart_subtotal_cnt ) {
                                            $ap_total_cart_subtotal_arr[] = array(
                                                'ap_fees_total_cart_subtotal'                 => $total_cart_subtotal_val,
                                                'ap_fees_ap_total_cart_subtotal_min_subtotal' => $fees_ap_total_cart_subtotal_min_subtotal[ $total_cart_subtotal_cnt ],
                                                'ap_fees_ap_total_cart_subtotal_max_subtotal' => $fees_ap_total_cart_subtotal_max_subtotal[ $total_cart_subtotal_cnt ],
                                                'ap_fees_ap_price_total_cart_subtotal'        => $fees_ap_price_total_cart_subtotal[ $total_cart_subtotal_cnt ]
                                            );
                                        }
                                    }
                                }
                            }
                        endif;
                    }

                    //Shipping Class subtotal
                    if ( isset( $fees['ap_shipping_class_subtotal_fees_conditions_condition'] ) ) {
                        $fees_shipping_class_subtotal                 = $fees['ap_shipping_class_subtotal_fees_conditions_condition'];
                        $fees_ap_shipping_class_subtotal_min_subtotal = $fees['ap_fees_ap_shipping_class_subtotal_min_subtotal'];
                        $fees_ap_shipping_class_subtotal_max_subtotal = $fees['ap_fees_ap_shipping_class_subtotal_max_subtotal'];
                        $fees_ap_price_shipping_class_subtotal        = $fees['ap_fees_ap_price_shipping_class_subtotal'];

                        $shipping_class_subtotal_arr = array();
                        foreach ( $fees_shipping_class_subtotal as $shipping_class_subtotal_key => $shipping_class_subtotal_val ) {
                            $shipping_class_subtotal_arr[] = $shipping_class_subtotal_val;
                        }
                        $size_shipping_class_subtotal_cond = count( $fees_shipping_class_subtotal );

                        if ( ! empty( $size_shipping_class_subtotal_cond ) && $size_shipping_class_subtotal_cond > 0 ):
                            for ( $shipping_class_subtotal_cnt = 0; $shipping_class_subtotal_cnt < $size_shipping_class_subtotal_cond; $shipping_class_subtotal_cnt ++ ) {
                                if ( ! empty( $shipping_class_subtotal_arr ) && $shipping_class_subtotal_arr !== '' ) {
                                    foreach ( $shipping_class_subtotal_arr as $shipping_class_subtotal_key => $shipping_class_subtotal_val ) {
                                        if ( $shipping_class_subtotal_key === $shipping_class_subtotal_cnt ) {
                                            $ap_shipping_class_subtotal_arr[] = array(
                                                'ap_fees_shipping_class_subtotals'                 => $shipping_class_subtotal_val,
                                                'ap_fees_ap_shipping_class_subtotal_min_subtotal' => $fees_ap_shipping_class_subtotal_min_subtotal[ $shipping_class_subtotal_cnt ],
                                                'ap_fees_ap_shipping_class_subtotal_max_subtotal' => $fees_ap_shipping_class_subtotal_max_subtotal[ $shipping_class_subtotal_cnt ],
                                                'ap_fees_ap_price_shipping_class_subtotal'        => $fees_ap_price_shipping_class_subtotal[ $shipping_class_subtotal_cnt ]
                                            );
                                        }
                                    }
                                }
                            }
                        endif;
                    }

                    update_post_meta( $method_id, 'fee_settings_unique_shipping_title', $fee_settings_unique_shipping_title );
                    update_post_meta( $method_id, 'ap_rule_status', $ap_rule_status );
                    update_post_meta( $method_id, 'cost_rule_match', maybe_serialize($cost_rule_match) );
                    update_post_meta( $method_id, 'main_rule_condition', $main_rule_condition );

                    update_post_meta( $method_id, 'sm_product_cost', $sm_product_cost );
                    update_post_meta( $method_id, 'how_to_apply', $how_to_apply );

                    /* Apply per quantity postmeta start */
                    update_post_meta( $method_id, 'sm_fee_chk_qty_price', $sm_fee_chk_qty_price );
                    update_post_meta( $method_id, 'sm_fee_per_qty', $sm_fee_per_qty );
                    update_post_meta( $method_id, 'sm_extra_product_cost', $sm_extra_product_cost );
                    /* Apply per quantity postmeta end */
                    update_post_meta( $method_id, 'sm_tooltip_desc', $sm_tooltip_desc );
                    update_post_meta( $method_id, 'sm_select_taxable', $sm_select_taxable );
                    update_post_meta( $method_id, 'sm_estimation_delivery', $sm_estimation_delivery );
                    update_post_meta( $method_id, 'sm_start_date', $sm_start_date );
                    update_post_meta( $method_id, 'sm_end_date', $sm_end_date );
                    update_post_meta( $method_id, 'sm_extra_cost', $sm_extra_cost );
                    update_post_meta( $method_id, 'sm_extra_cost_calculation_type', $sm_extra_cost_calculation_type );

                    /*Advance Pricing Rules Particular Status*/
                    update_post_meta( $method_id, 'cost_on_product_status', $cost_on_product_status );
                    update_post_meta( $method_id, 'cost_on_product_weight_status', $cost_on_product_weight_status );
                    update_post_meta( $method_id, 'cost_on_product_subtotal_status', $cost_on_product_subtotal_status );

                    update_post_meta( $method_id, 'cost_on_category_status',$cost_on_category_status);
                    update_post_meta( $method_id, 'cost_on_category_weight_status', $cost_on_category_weight_status );
                    update_post_meta( $method_id, 'cost_on_category_subtotal_status', $cost_on_category_subtotal_status );

                    update_post_meta( $method_id, 'cost_on_total_cart_qty_status', $cost_on_total_cart_qty_status );
                    update_post_meta( $method_id, 'cost_on_total_cart_weight_status', $cost_on_total_cart_weight_status );
                    update_post_meta( $method_id, 'cost_on_total_cart_subtotal_status', $cost_on_total_cart_subtotal_status );
                    update_post_meta( $method_id, 'cost_on_shipping_class_subtotal_status', $cost_on_shipping_class_subtotal_status );

                    if ( isset( $sm_select_day_of_week ) ) {
                        update_post_meta( $method_id, 'sm_select_day_of_week', $sm_select_day_of_week );
                    }

                    if ( isset( $sm_time_from ) ) {
                        update_post_meta( $method_id, 'sm_time_from', $sm_time_from );
                    }
                    if ( isset( $sm_time_to ) ) {
                        update_post_meta( $method_id, 'sm_time_to', $sm_time_to );
                    }

                    update_post_meta( $method_id, 'sm_metabox', $feesArray );
                    update_post_meta( $method_id, 'sm_metabox_ap_product', $ap_product_arr );
                    update_post_meta( $method_id, 'sm_metabox_ap_product_weight', $ap_product_weight_arr );
                    update_post_meta( $method_id, 'sm_metabox_ap_product_subtotal', $ap_product_subtotal_arr );

                    update_post_meta( $method_id, 'sm_metabox_ap_category', $ap_category_arr );
                    update_post_meta( $method_id, 'sm_metabox_ap_category_weight', $ap_category_weight_arr );
                    update_post_meta( $method_id, 'sm_metabox_ap_category_subtotal', $ap_category_subtotal_arr );

                    update_post_meta( $method_id, 'sm_metabox_ap_total_cart_qty', $ap_total_cart_qty_arr );
                    update_post_meta( $method_id, 'sm_metabox_ap_total_cart_weight', $ap_total_cart_weight_arr );
                    update_post_meta( $method_id, 'sm_metabox_ap_total_cart_subtotal', $ap_total_cart_subtotal_arr );

                    update_post_meta( $method_id, 'sm_metabox_ap_shipping_class_subtotal', $ap_shipping_class_subtotal_arr );

                    update_post_meta( $method_id, 'sm_metabox_ap_class', $ap_class_arr );
                    if ( ! empty( $sitepress ) ) {
                        do_action( 'wpml_register_single_string', 'advanced-flat-rate-shipping-for-woocommerce', sanitize_text_field( $fee_settings_product_fee_title ), sanitize_text_field( $fee_settings_product_fee_title ) );
                    }

                    $getSortOrder = get_option( 'sm_sortable_order' );
                    if ( !empty( $getSortOrder ) ) {
                        foreach ( $getSortOrder as $getSortOrder_id ) {
                            settype( $getSortOrder_id, 'integer' );

                        }
                        array_unshift($getSortOrder,$method_id);
                    }
                    update_option('sm_sortable_order', $getSortOrder);
                }
            } else {
                echo '<div class="updated error"><p>' . esc_html__( 'Error saving shipping method.', 'advanced-flat-rate-shipping-for-woocommerce' ) . '</p></div>';
                return false;
            }

            $afrsmnonce = wp_create_nonce('afrsmnonce');
            wp_safe_redirect(add_query_arg(array('page' => 'afrsm-pro-list', '_wpnonce' => esc_attr( $afrsmnonce )), admin_url('admin.php')));
            exit();
        }
    }

    /**
     * Review message in footer
     *
     * @since  1.0.0
     *
     * @return string
     */
    public function afrsm_pro_admin_footer_review() {
        echo sprintf( wp_kses( __( 'If you like <strong>Advanced Flat Rate Shipping For WooCommerce</strong> plugin, please leave us ★★★★★ ratings on <a href="%1$s" target="_blank">DotStore</a>.', 'advanced-flat-rate-shipping-for-woocommerce' ), array('strong' => array(), 'a' => array('href' => array()) )), esc_url('https://www.thedotstore.com/advanced-flat-rate-shipping-method-for-woocommerce/'));
    }

    /**
     * Clone shipping method
     *
     * @since  3.4
     *
     * @uses get_post()
     * @uses wp_get_current_user()
     * @uses wp_insert_post()
     *
     * @return string true if current_shipping_id is empty then it will give message.
     */
    public function afrsm_pro_clone_shipping_method() {
        /* Check for post request */
        $get_current_shipping_id = filter_input(INPUT_GET,'current_shipping_id',FILTER_SANITIZE_NUMBER_INT);

        $get_post_id = isset($get_current_shipping_id) ? absint($get_current_shipping_id) : '';

        if (empty($get_post_id)) {
            echo sprintf( wp_kses( __('<strong>No post to duplicate has been supplied!</strong>', 'advanced-flat-rate-shipping-for-woocommerce' ), array('strong' => array())) );
            wp_die();

        }
        /* End of if */
        /* Get the original post id */
        if (!empty($get_post_id) || '' !== $get_post_id) {
            /* Get all the original post data */
            $post = get_post($get_post_id);
            /* Get current user and make it new post user (duplicate post) */
            $current_user = wp_get_current_user();
            $new_post_author = $current_user->ID;
            /* If post data exists, duplicate the data into new duplicate post */
            if (isset($post) && null !== $post) {
                /* New post data array */
                $args = array(
                    'comment_status' => $post->comment_status,
                    'ping_status' => $post->ping_status,
                    'post_author' => $new_post_author,
                    'post_content' => $post->post_content,
                    'post_excerpt' => $post->post_excerpt,
                    'post_name' => $post->post_name,
                    'post_parent' => $post->post_parent,
                    'post_password' => $post->post_password,
                    'post_status' => 'draft',
                    'post_title' => $post->post_title . '-duplicate',
                    'post_type' => self::afrsm_shipping_post_type,
                    'to_ping' => $post->to_ping,
                    'menu_order' => $post->menu_order
                );
                /* Duplicate the post by wp_insert_post() function */
                $new_post_id = wp_insert_post($args);
                /* Duplicate all post meta-data */
                $post_meta_data = get_post_meta($get_post_id);
                if (0 !== count($post_meta_data)) {
                    foreach ($post_meta_data as $meta_key => $meta_data) {
                        if ('_wp_old_slug' === $meta_key) {
                            continue;
                        }
                        $meta_value = maybe_unserialize($meta_data[0]);
                        update_post_meta( $new_post_id, $meta_key, $meta_value);
                    }
                }
            }
            $afrsmnonce = wp_create_nonce('afrsmnonce');
            $redirect_url = add_query_arg( array(
                'page'     => 'afrsm-pro-edit-shipping',
                'id'     => $new_post_id,
                'action'   => 'edit',
                '_wpnonce' => esc_attr( $afrsmnonce )
            ), admin_url( 'admin.php' ) );
            echo wp_json_encode(array(true, $redirect_url));
        }
        wp_die();
    }

    /**
     * Change shipping status from list of shipping method
     *
     * @since  3.4
     *
     * @uses update_post_meta()
     *
     * if current_shipping_id is empty then it will give message.
     */
    public function afrsm_pro_change_status_from_list_section() {
        global $sitepress;
        $default_lang = $this->afrsm_pro_get_default_langugae_with_sitpress();
        /* Check for post request */
        $get_current_shipping_id = filter_input(INPUT_GET,'current_shipping_id',FILTER_SANITIZE_NUMBER_INT);
        if (!empty($sitepress)) {
            $get_current_shipping_id = apply_filters('wpml_object_id', $get_current_shipping_id, 'product', TRUE, $default_lang);
        } else {
            $get_current_shipping_id = $get_current_shipping_id;
        }

        $get_current_value = filter_input(INPUT_GET,'current_value',FILTER_SANITIZE_STRING);

        $get_post_id = isset($get_current_shipping_id) ? absint($get_current_shipping_id) : '';

        if (empty($get_post_id)) {
            echo '<strong>' . esc_html__('Something went wrong', 'advanced-flat-rate-shipping-for-woocommerce') . '</strong>';
            wp_die();
        }

        $current_value = isset($get_current_value) ? sanitize_text_field($get_current_value) : '';

        if ('true' === $current_value) {
            $post_args = array(
                'ID' => $get_post_id,
                'post_status' => 'publish',
                'post_type' => self::afrsm_shipping_post_type,
            );
            $post_update = wp_update_post($post_args);
            update_post_meta($get_post_id, 'sm_status', 'on');
        } else {
            $post_args = array(
                'ID' => $get_post_id,
                'post_status' => 'draft',
                'post_type' => self::afrsm_shipping_post_type,
            );
            $post_update = wp_update_post($post_args);
            update_post_meta($get_post_id, 'sm_status', 'off');
        }
        if (!empty($post_update)) {
            echo esc_html__('Shipping status changed successfully.', 'advanced-flat-rate-shipping-for-woocommerce');
        } else {
            echo esc_html__('Something went wrong', 'advanced-flat-rate-shipping-for-woocommerce');
        }
        wp_die();
    }

    /**
     * Change Advance pricing rules status
     *
     * @since  3.4
     *
     * @uses update_post_meta()
     *
     * @return string true if current_shipping_id is empty then it will give message.
     *
     */
    public function afrsm_pro_change_status_of_advance_pricing_rules() {
        $get_current_shipping_id = filter_input(INPUT_GET,'current_shipping_id',FILTER_SANITIZE_NUMBER_INT);
        $get_current_value = filter_input(INPUT_GET,'current_value',FILTER_SANITIZE_STRING);

        $get_post_id = isset($get_current_shipping_id) ? absint($get_current_shipping_id) : '';

        if (empty($get_post_id)) {
            echo '<strong>' . esc_html__('Something went wrong', 'advanced-flat-rate-shipping-for-woocommerce') . '</strong>';
            wp_die();
        }
        $current_value = isset($get_current_value) ? sanitize_text_field($get_current_value) : '';

        if ('true' === $current_value) {
            update_post_meta($get_post_id, 'ap_rule_status', 'off');
            echo esc_html('true');
        }
        wp_die();
    }

    /**
     * Get default site language
     *
     * @since  3.4
     *
     * @return string $default_lang
     *
     */
    public function afrsm_pro_get_default_langugae_with_sitpress() {
        global $sitepress;

        if (!empty($sitepress)) {
            $default_lang = $sitepress->get_current_language();
        } else {
            $default_lang = $this->afrsm_pro_get_current_site_language();
        }
        return $default_lang;
    }

    /**
     * Get AFRSM shipping method
     *
     * @since  3.4
     *
     * @param string $args
     *
     * @return string $default_lang
     *
     */
    public static function afrsm_pro_get_shipping_method($args) {
        $sm_args = array(
            'post_type' => self::afrsm_shipping_post_type,
            'posts_per_page' => -1,
            'orderby' => 'menu_order',
            'order' => 'ASC',
            'suppress_filters' => false
        );

        if ('not_list' === $args) {
            $sm_args['post_status'] = 'publish';
        }

        $get_all_shipping = new WP_Query($sm_args);
        $get_all_shipping = $get_all_shipping->get_posts();
        return $get_all_shipping;
    }

    /**
     * Convert array to json
     *
     * @since 1.0.0
     *
     * @param array $arr
     *
     * @return array $filter_data
     */
    public function afrsm_pro_convert_array_to_json($arr){
        $filter_data=[];
        foreach ($arr as $key => $value) {
            $option=[];
            $option['name']=$value;
            $option['attributes']['value']=$key;
            $filter_data[]=$option;
        }
        return $filter_data;
    }

    /**
     * Convert array to json
     *
     * @since 1.0.0
     *
     * @param array $arr
     *
     * @return array $filter_data
     */
    public function afrsm_pro_attribute_list() {
        $filter_attr_data = [];
        $filter_attr_json = array();
        $attribute_taxonomies = wc_get_attribute_taxonomies();
        if ( $attribute_taxonomies ){
            foreach ( $attribute_taxonomies as $attribute ) {
                $att_label  = $attribute->attribute_label;
                $att_name   = wc_attribute_taxonomy_name($attribute->attribute_name);
                $filter_attr_json['name'] = $att_label;
                $filter_attr_json['attributes']['value'] = esc_html__( $att_name , 'advanced-flat-rate-shipping-for-woocommerce' );
                $filter_attr_data[] = $filter_attr_json;
            }
        }
        return $filter_attr_data;
    }

    /**
     * Get product id and variation id from cart
     *
     * @since 1.0.0
     *
     * @uses afrsm_pro_get_cart();
     *
     * @param string $sitepress
     * @param string $default_lang
     *
     * @return array $cart_main_product_ids_array
     */
    public function afrsm_pro_get_main_prd_id($sitepress, $default_lang) {
        $cart_array = $this->afrsm_pro_get_cart();
        $cart_main_product_ids_array = array();
        foreach ( $cart_array as $woo_cart_item ) {
            $_product = wc_get_product( $woo_cart_item['product_id'] );
            if (!($_product->is_virtual('yes'))) {
                if (!empty($sitepress)) {
                    $cart_main_product_ids_array[] = apply_filters('wpml_object_id', $woo_cart_item['product_id'], 'product', TRUE, $default_lang);
                } else {
                    $cart_main_product_ids_array[] = $woo_cart_item['product_id'];
                }
            }
        }
        return $cart_main_product_ids_array;
    }

    /**
     * Get product id and variation id from cart
     *
     * @since 1.0.0
     *
     * @uses afrsm_pro_get_cart();
     *
     * @param string $sitepress
     * @param string $default_lang
     *
     * @return array $cart_product_ids_array
     */
    public function afrsm_pro_get_prd_var_id($sitepress, $default_lang) {
        $cart_array = $this->afrsm_pro_get_cart();
        $cart_product_ids_array = array();
        foreach ( $cart_array as $woo_cart_item ) {
            $_product = wc_get_product( $woo_cart_item['product_id'] );
            if (!($_product->is_virtual('yes'))) {
                if ($_product->is_type('variable')) {
                    if (!empty($sitepress)) {
                        $cart_product_ids_array[] = apply_filters('wpml_object_id', $woo_cart_item['variation_id'], 'product', TRUE, $default_lang);
                    } else {
                        $cart_product_ids_array[] = $woo_cart_item['variation_id'];
                    }
                }
                if ($_product->is_type('simple')) {
                    if (!empty($sitepress)) {
                        $cart_product_ids_array[] = apply_filters('wpml_object_id', $woo_cart_item['product_id'], 'product', TRUE, $default_lang);
                    } else {
                        $cart_product_ids_array[] = $woo_cart_item['product_id'];
                    }
                }
            }
        }
        return $cart_product_ids_array;
    }

    /**
     * Get variation name from cart
     *
     * @since 1.0.0
     *
     * @uses afrsm_pro_get_cart();
     *
     * @param string $sitepress
     * @param string $default_lang
     *
     * @return array $cart_product_ids_array
     */
    public function afrsm_pro_get_var_name($sitepress, $default_lang) {
        $cart_array = $this->afrsm_pro_get_cart();
        $cart_product_ids_array = array();
        foreach ( $cart_array as $woo_cart_item ) {
            $_product = wc_get_product( $woo_cart_item['product_id'] );
            if (!($_product->is_virtual('yes'))) {
                if ($_product->is_type('variable')) {
                    if (!empty($sitepress)) {
                        $cart_product_ids_array[] = apply_filters('wpml_object_id', $woo_cart_item['variation_id'], 'product', TRUE, $default_lang);
                    } else {
                        $cart_product_ids_array[] = $woo_cart_item['variation_id'];
                    }
                }
            }
        }

        $variation_cart_product = array();
        foreach ($cart_product_ids_array as $variation_id) {
            $variation = new WC_Product_Variation($variation_id);
            $variation_cart_product[] = $variation->get_variation_attributes();
        }

        $variation_cart_products_array =array();
        if (isset($variation_cart_product) && !empty($variation_cart_product)) {
            foreach ($variation_cart_product as $cart_product_id) {
                if (isset($cart_product_id) && !empty($cart_product_id)) {
                    foreach ($cart_product_id as $v) {
                        $variation_cart_products_array[] = $v;
                    }
                }
            }
        }

        return $variation_cart_products_array;
    }

    /**
     * Get product id and variation id from cart
     *
     * @since 1.0.0
     *
     * @return array $cart_array
     */
    public function afrsm_pro_get_cart() {
        $cart_array = WC()->cart->get_cart();
        return $cart_array;
    }

    /**
     * Get current site langugae
     *
     * @since 1.0.0
     *
     * @return string $default_lang
     */
    public function afrsm_pro_get_current_site_language() {
        $get_site_language = get_bloginfo('language');
        if (false !== strpos($get_site_language, '-')) {
            $get_site_language_explode = explode('-', $get_site_language);
            $default_lang = $get_site_language_explode[0];
        } else {
            $default_lang = $get_site_language;
        }
        return $default_lang;
    }


    /**
     * Remove section from shipping settings because we have added new menu in woocommece section
     *
     * @param array $sections
     *
     * @return array $sections
     *
     * @since    1.0.0
     */
    public function afrsm_pro_remove_section($sections) {
        unset($sections['advanced_flat_rate_shipping'], $sections['forceall']);
        return $sections;
    }

    /**
     * Get cart subtotal
     *
     * @return float $subtotal
     *
     * @since    3.6
     */
    public function afrsm_pro_get_cart_subtotal() {
        $get_customer = WC()->cart->get_customer();
        $get_customer_vat_exempt = WC()->customer->get_is_vat_exempt();
        $tax_display_cart = WC()->cart->tax_display_cart;
        $cart_subtotal = 0;
        if ('incl' === $tax_display_cart && ! ($get_customer && $get_customer_vat_exempt)) {
            if ( WC()->cart->get_subtotal_tax() > 0 && ! wc_prices_include_tax() ) {
                $cart_subtotal +=  WC()->cart->get_subtotal() +  WC()->cart->get_subtotal_tax();
            }
        } else {
            $cart_subtotal +=  WC()->cart->get_subtotal();
        }
        return $cart_subtotal;
    }

    /**
     * Fetch Zone
     *
     * @since    3.6
     */
    public function afrsm_pro_fetch_shipping_zone() {
        global $wpdb;

        $sz_table_name = "{$wpdb->prefix}wcextraflatrate_shipping_zones";
        $szl_table_name = "{$wpdb->prefix}wcextraflatrate_shipping_zone_locations";
        $sz_flag = 0;
        $szl_flag = 0;
        if($wpdb->get_var("SHOW TABLES LIKE '$sz_table_name'") === $sz_table_name) {
            $sz_flag = 1;
            $get_zone_sql = "SELECT * FROM {$wpdb->prefix}wcextraflatrate_shipping_zones as tbl1";
            $get_zone_data = $wpdb->get_results($get_zone_sql);
        }

        if($wpdb->get_var("SHOW TABLES LIKE '$szl_table_name'") === $szl_table_name) {
            $szl_flag = 1;
        }
        $i = 0;
        $success_array = array();
        if (!empty($get_zone_data)) {
            foreach ($get_zone_data as $value) {
                $i++;
                $zone_id = $value->zone_id;
                $zone_name = $value->zone_name;
                $zone_enabled = $value->zone_enabled;
                if ('1' === $zone_enabled) {
                    $post_status = 'publish';
                } else {
                    $post_status = 'draft';
                }
                $zone_type = $value->zone_type;
                $zone_order = $value->zone_order;

                $zone_post = array(
                    'post_title' => $zone_name,
                    'post_status' => $post_status,
                    'menu_order' => $zone_order + 1,
                    'post_type' => 'wc_afrsm_zone',
                );
                $new_zone_id = wp_insert_post($zone_post);
                if(!is_wp_error($new_zone_id)){
                    $locations = $wpdb->get_results($wpdb->prepare("SELECT * FROM {$wpdb->prefix}wcextraflatrate_shipping_zone_locations WHERE zone_id = %s;", $zone_id));
                    if (!empty($locations)) {
                        $locations_list = array();
                        $locations_state_list = array();
                        $locations_country_list= array();
                        foreach ($locations as $locations_value) {
                            $location_type = $locations_value->location_type;
                            if ('country' === $location_type ||'state' === $location_type ) {
                                $postcode_location_type = $locations_value->location_code;
                            }
                            if ('postcode' === $location_type) {
                                $locations_list[$postcode_location_type][] = $locations_value->location_code;
                            }

                            if ('state' === $location_type) {
                                $locations_state_list[] = $locations_value->location_code;
                            }
                            if ('country' === $location_type) {
                                $locations_country_list[] = $locations_value->location_code;
                            }
                            update_post_meta($new_zone_id, 'location_type', $location_type);
                        }


                        if ('postcodes' === $zone_type) {
                            $location_code = $locations_list;
                        } elseif ('countries' === $zone_type) {
                            $location_code[] = $locations_country_list;
                        } else {
                            $location_code[] = $locations_state_list;
                        }

                        update_post_meta($new_zone_id, 'zone_type', $zone_type);
                        update_post_meta($new_zone_id, 'location_code', $location_code);
                        $success_array[] = true;
                    } else {
                        $success_array[] = false;
                    }
                } else {
                    $success_array[] = false;
                }
            }
        }
        $redirect_url = add_query_arg( array(
            'page'     => 'afrsm-wc-shipping-zones',
        ), admin_url( 'admin.php' ) );
        if (in_array(true, $success_array, true)) {
            if (1 === $sz_flag) {
                $sql = "DROP TABLE IF EXISTS $sz_table_name";
                $wpdb->query($sql);
            }
            if (1 === $szl_flag) {
                $sql = "DROP TABLE IF EXISTS $szl_table_name";
                $wpdb->query($sql);
            }
            echo wp_json_encode(array(true, $redirect_url));
        } else {
            echo wp_json_encode(array(false, $redirect_url));
        }
        update_option('zone_migration', 'done');
        wp_die();
    }
}
