<?php
// If this file is called directly, abort.
if ( !defined( 'ABSPATH' ) ) {
    exit;
}
$image_url = AFRSM_PRO_PLUGIN_URL . 'admin/images/right_click.png';
?>
<div class="dotstore_plugin_sidebar">
    <div class="dotstore-important-link">
        <div class="video-detail important-link">
            <a href="<?php echo esc_url('https://www.youtube.com/watch?v=y3Sh6_Qaen0'); ?>" target="_blank">
                <img width="100%" src="<?php echo esc_url(AFRSM_PRO_PLUGIN_URL . 'admin/images/plugin-videodemo.png'); ?>" alt="<?php esc_html_e('Advanced Flat Rate Shipping For WooCommerce', 'advanced-flat-rate-shipping-for-woocommerce');?>">
            </a>
        </div>
    </div>

    <div class="dotstore-important-link">
        <h2><span class="dotstore-important-link-title"><?php esc_html_e('Important link', 'advanced-flat-rate-shipping-for-woocommerce'); ?></span></h2>
        <div class="video-detail important-link">
            <ul>
                <li>
                    <img src="<?php echo esc_url( $image_url ); ?>">
                    <a target="_blank" href="<?php echo esc_url('store.multidots.com/docs/plugin/advanced-flat-rate-shipping-method-for-woocommerce'); ?>"><?php esc_html_e('Plugin documentation', 'advanced-flat-rate-shipping-for-woocommerce'); ?></a>
                </li>
                <li>
                    <img src="<?php echo esc_url( $image_url ); ?>">
                    <a target="_blank" href="<?php echo esc_url('store.multidots.com/support'); ?>"><?php esc_html_e('Support platform', 'advanced-flat-rate-shipping-for-woocommerce'); ?></a>
                </li>
                <li>
                    <img src="<?php echo esc_url( $image_url ); ?>">
                    <a target="_blank" href="<?php echo esc_url('store.multidots.com/suggest-a-feature'); ?>"><?php esc_html_e('Suggest A Feature', 'advanced-flat-rate-shipping-for-woocommerce'); ?></a>
                </li>
                <li>
                    <img src="<?php echo esc_url( $image_url ); ?>">
                    <a  target="_blank" href="<?php echo esc_url('store.multidots.com/advanced-flat-rate-shipping-method-for-woocommerce'); ?>"><?php esc_html_e('Changelog', 'advanced-flat-rate-shipping-for-woocommerce'); ?></a>
                </li>
            </ul>
        </div>
    </div>

    <div class="dotstore-important-link">
        <h2><span class="dotstore-important-link-title"><?php esc_html_e('OUR POPULAR PLUGINS', 'advanced-flat-rate-shipping-for-woocommerce'); ?></span></h2>
        <div class="video-detail important-link">
            <ul>
                <li>
                    <img class="sidebar_plugin_icone" src="<?php echo esc_url( AFRSM_PRO_PLUGIN_URL . 'admin/images/advance-flat-rate-2.png' ); ?>">
                    <a target="_blank" href="<?php echo esc_url('store.multidots.com/advanced-flat-rate-shipping-method-for-woocommerce'); ?>"><?php esc_html_e('Advanced Flat Rate Shipping Method', 'advanced-flat-rate-shipping-for-woocommerce'); ?></a>
                </li>
                <li>
                    <img class="sidebar_plugin_icone" src="<?php echo esc_url( AFRSM_PRO_PLUGIN_URL . 'admin/images/wc-conditional-product-fees.png' ); ?>">
                    <a target="_blank" href="<?php echo esc_url('store.multidots.com/woocommerce-conditional-product-fees-checkout'); ?>"><?php esc_html_e('WooCommerce Conditional Product Fees', 'advanced-flat-rate-shipping-for-woocommerce'); ?></a>
                </li>
                <li>
                    <img class="sidebar_plugin_icone" src="<?php echo esc_url( AFRSM_PRO_PLUGIN_URL . 'admin/images/advance-menu-manager.png' ); ?>">
                    <a target="_blank" href="<?php echo esc_url('store.multidots.com/advance-menu-manager-wordpress'); ?>"><?php esc_html_e('Advance Menu Manager', 'advanced-flat-rate-shipping-for-woocommerce'); ?></a>
                </li>
                <li>
                    <img class="sidebar_plugin_icone" src="<?php echo esc_url( AFRSM_PRO_PLUGIN_URL . 'admin/images/wc-enhanced-ecommerce-analytics-integration.png' ); ?>">
                    <a target="_blank" href="<?php echo esc_url('store.multidots.com/woocommerce-enhanced-ecommerce-analytics-integration-with-conversion-tracking'); ?>"><?php esc_html_e('Woo Enhanced Ecommerce Analytics Integration', 'advanced-flat-rate-shipping-for-woocommerce'); ?></a>
                </li>
                <li>
                    <img class="sidebar_plugin_icone" src="<?php echo esc_url( AFRSM_PRO_PLUGIN_URL . 'admin/images/advanced-product-size-charts.png' ); ?>">
                    <a target="_blank" href="<?php echo esc_url('store.multidots.com/woocommerce-advanced-product-size-charts'); ?>"><?php esc_html_e('Advanced Product Size Charts', 'advanced-flat-rate-shipping-for-woocommerce'); ?></a>
                </li>
                <li>
                    <img class="sidebar_plugin_icone" src="<?php echo esc_url( AFRSM_PRO_PLUGIN_URL . 'admin/images/blockers.png' ); ?>">
                    <a target="_blank" href="<?php echo esc_url('https://www.thedotstore.com/product/woocommerce-blocker-lite-prevent-fake-orders-blacklist-fraud-customers/'); ?>"><?php esc_html_e('WooCommerce Blocker – Prevent Fake Orders', 'advanced-flat-rate-shipping-for-woocommerce'); ?></a>
                </li>
            </ul>
        </div>
        <div class="view-button">
            <a class="view_button_dotstore" target="_blank" href="<?php echo esc_url('store.multidots.com/plugins'); ?>store.multidots.com/plugins"><?php esc_html_e('VIEW ALL', 'advanced-flat-rate-shipping-for-woocommerce'); ?></a>
        </div>
    </div>
</div>
</div>
</div>