<?php
// If this file is called directly, abort.
if ( !defined( 'ABSPATH' ) ) {
    exit;
}

$plugin_name = AFRSM_PRO_PLUGIN_NAME;
$plugin_version = AFRSM_PRO_PLUGIN_VERSION;
?>
<div id="dotsstoremain">
    <div class="all-pad">
        <header class="dots-header">
            <div class="dots-logo-main">
                <img src="<?php echo esc_url(AFRSM_PRO_PLUGIN_URL . 'admin/images/advance-flat-rate.png'); ?>">
            </div>
            <div class="dots-header-right">
                <div class="logo-detail">
                    <strong><?php esc_html_e($plugin_name, 'advanced-flat-rate-shipping-for-woocommerce'); ?></strong>
                    <span><?php esc_html_e('Premium Version', 'advanced-flat-rate-shipping-for-woocommerce'); ?> <?php echo esc_html__( $plugin_version, 'advanced-flat-rate-shipping-for-woocommerce'); ?></span>
                </div>
                <div class="button-group">
                    <?php
                    if ( afrsfw_fs()->is__premium_only() ) {
                        if ( afrsfw_fs()->can_use_premium_code() ) {
                            global $afrsfw_fs;
                            ?>
                            <div class="button-dots-left">
                                <span class="support_dotstore_image"><a target="_blank" href="<?php echo esc_url($afrsfw_fs->get_account_url()); ?>">
                                        <img src="<?php echo esc_url(AFRSM_PRO_PLUGIN_URL . 'admin/images/account_new.png'); ?>"></a>
                                </span>
                            </div>
                            <?php
                        }
                    }
                    ?>
                    <div class="button-dots">
                        <span class="support_dotstore_image"><a target="_blank" href="<?php echo esc_url('http://www.thedotstore.com/support/'); ?>">
                                <img src="<?php echo esc_url(AFRSM_PRO_PLUGIN_URL . 'admin/images/support_new.png'); ?>"></a>
                        </span>
                    </div>
                </div>
            </div>

            <?php
            $current_page = filter_input(INPUT_GET,'page',FILTER_SANITIZE_STRING);
            $add_zone = filter_input(INPUT_GET,'add_zone',FILTER_SANITIZE_STRING);
            $afrsm_shipping_list = isset($current_page) && 'afrsm-pro-list' === $current_page  ? 'active' : '';
            $afrsm_shipping_add = isset($current_page) && 'afrsm-pro-add-shipping' === $current_page ? 'active' : '';
            $afrsm_shipping_edit = isset($current_page) && 'afrsm-pro-edit-shipping' === $current_page ? 'active' : '';
            $afrsm_shipping_zones = isset($current_page) && 'afrsm-wc-shipping-zones' === $current_page ? 'active' : '';
            $afrsm_add_shipping_zone = isset($current_page) && 'afrsm-wc-shipping-zones' === $current_page && isset($add_zone) ? 'active' : '';
            $afrsm_getting_started = isset($current_page) && 'afrsm-pro-get-started' === $current_page ? 'active' : '';
            $afrsm_information = isset($current_page) && 'afrsm-pro-information' === $current_page ? 'active' : '';
            $afrsm_validate = isset($current_page) && 'afrsm-pro-validate' === $current_page ? 'active' : '';
            if (isset($current_page) && 'afrsm-pro-information' === $current_page || isset($current_page) && 'afrsm-pro-get-started' === $current_page) {
                $fee_about = 'active';
            } else {
                $fee_about = '';
            }

            $afrsm_action = filter_input(INPUT_GET,'action',FILTER_SANITIZE_STRING);
            if (isset($afrsm_action) && !empty($afrsm_action)) {
                if ('add' === $afrsm_action || 'edit' === $afrsm_action) {
                    $afrsm_shipping_add = 'active';
                }
            }
            ?>
            <div class="dots-menu-main">
                <nav>
                    <ul>
                        <li>
                            <a class="dotstore_plugin <?php echo esc_attr( $afrsm_shipping_list ); ?>" href="<?php echo esc_url(add_query_arg(array('page' => 'afrsm-pro-list'), admin_url('admin.php'))); ?>"><?php esc_html_e('Manage Shipping Methods', 'advanced-flat-rate-shipping-for-woocommerce'); ?></a>
                        </li>
                        <li>
                            <?php
                            if (isset($current_page) && 'afrsm-pro-add-shipping' === $current_page) {
                                ?>
                                <a class="dotstore_plugin <?php echo esc_attr( $afrsm_shipping_add ); ?>" href="<?php echo esc_url(add_query_arg(array('page' => 'afrsm-pro-add-shipping'), admin_url('admin.php'))); ?>"><?php esc_html_e('Add New Shipping Method', 'advanced-flat-rate-shipping-for-woocommerce'); ?></a>
                                <?php
                            } else if (isset($current_page) && 'afrsm-pro-edit-shipping' === $current_page) {
                                ?>
                                <a class="dotstore_plugin <?php echo esc_attr( $afrsm_shipping_edit ); ?>" href="<?php echo esc_url(add_query_arg(array('page' => 'afrsm-pro-add-shipping'), admin_url('admin.php'))); ?>"><?php esc_html_e('Edit Shipping Method', 'advanced-flat-rate-shipping-for-woocommerce'); ?></a>
                                <?php
                            } else {
                                ?>
                                <a class="dotstore_plugin <?php echo esc_attr( $afrsm_shipping_add ); ?>" href="<?php echo esc_url(add_query_arg(array('page' => 'afrsm-pro-add-shipping'), admin_url('admin.php'))); ?>"><?php esc_html_e('Add New Shipping Method', 'advanced-flat-rate-shipping-for-woocommerce'); ?></a>
                                <?php
                            }
                            ?>
                        </li>
                        <li>
                            <a class="dotstore_plugin <?php echo esc_attr( $afrsm_shipping_zones ); ?>" href="<?php echo esc_url(add_query_arg(array('page' => 'afrsm-wc-shipping-zones'), admin_url('admin.php'))); ?>"><?php esc_html_e('Manage Shipping Zones', 'advanced-flat-rate-shipping-for-woocommerce'); ?></a>
                            <ul class="sub-menu">
                                <li><a class="dotstore_plugin <?php echo esc_attr($afrsm_add_shipping_zone); ?>" href="<?php echo esc_url(add_query_arg(array('page' => 'afrsm-wc-shipping-zones&add_zone'), admin_url('admin.php'))); ?>"><?php esc_html_e('Add Shipping Zone', 'advanced-flat-rate-shipping-for-woocommerce'); ?></a></li>
                            </ul>
                        </li>
                        <li>
                            <a class="dotstore_plugin <?php echo esc_attr( $fee_about ); ?>" href="<?php echo esc_url(add_query_arg(array('page' => 'afrsm-pro-get-started'), admin_url('admin.php'))); ?>"><?php esc_html_e('About Plugin', 'advanced-flat-rate-shipping-for-woocommerce'); ?></a>
                            <ul class="sub-menu">
                                <li><a class="dotstore_plugin <?php echo esc_attr( $afrsm_getting_started ); ?>" href="<?php echo esc_url(add_query_arg(array('page' => 'afrsm-pro-get-started'), admin_url('admin.php'))); ?>"><?php esc_html_e('Getting Started', 'advanced-flat-rate-shipping-for-woocommerce'); ?></a></li>
                                <li><a class="dotstore_plugin <?php echo esc_attr( $afrsm_information ); ?>" href="<?php echo esc_url(add_query_arg(array('page' => 'afrsm-pro-information'), admin_url('admin.php'))); ?>"><?php esc_html_e('Quick info', 'advanced-flat-rate-shipping-for-woocommerce'); ?></a></li>
                            </ul>
                        </li>
                        <li>
                            <a class="dotstore_plugin"><?php esc_html_e('Dotstore', 'advanced-flat-rate-shipping-for-woocommerce'); ?></a>
                            <ul class="sub-menu">
                                <li><a target="_blank" href="<?php echo esc_url('store.multidots.com/woocommerce-plugins'); ?>"><?php esc_html_e('WooCommerce Plugins', 'advanced-flat-rate-shipping-for-woocommerce'); ?></a></li>
                                <li><a target="_blank" href="<?php echo esc_url('store.multidots.com/wordpress-plugins'); ?>"><?php esc_html_e('Wordpress Plugins', 'advanced-flat-rate-shipping-for-woocommerce'); ?></a></li><br>
                                <li><a target="_blank" href="<?php echo esc_url('store.multidots.com/support'); ?>"><?php esc_html_e('Contact Support', 'advanced-flat-rate-shipping-for-woocommerce'); ?></a></li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
        </header>