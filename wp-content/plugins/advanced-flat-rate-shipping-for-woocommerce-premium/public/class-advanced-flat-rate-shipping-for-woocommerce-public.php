<?php
// If this file is called directly, abort.
if ( !defined( 'ABSPATH' ) ) {
    exit;
}
/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://www.multidots.com
 * @since      1.0.0
 *
 * @package    Advanced_Flat_Rate_Shipping_For_WooCommerce_Pro
 * @subpackage Advanced_Flat_Rate_Shipping_For_WooCommerce_Pro/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Advanced_Flat_Rate_Shipping_For_WooCommerce_Pro
 * @subpackage Advanced_Flat_Rate_Shipping_For_WooCommerce_Pro/public
 * @author     Multidots <inquiry@multidots.in>
 */
class Advanced_Flat_Rate_Shipping_For_WooCommerce_Pro_Public {

    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $plugin_name    The ID of this plugin.
     */
    private $plugin_name;

    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $version    The current version of this plugin.
     */
    private $version;

    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     * @param      string    $plugin_name       The name of the plugin.
     * @param      string    $version    The version of this plugin.
     */
    public function __construct($plugin_name, $version) {

        $this->plugin_name = $plugin_name;
        $this->version = $version;
    }

    /**
     * Register the stylesheets for the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function afrsm_pro_enqueue_styles() {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Advanced_Flat_Rate_Shipping_For_WooCommerce_Pro_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Advanced_Flat_Rate_Shipping_For_WooCommerce_Pro_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */
        wp_enqueue_style($this->plugin_name, plugin_dir_url(__FILE__) . 'css/advanced-flat-rate-shipping-for-woocommerce-public.css', array(), $this->version, 'all');
        wp_enqueue_style('font-awesome-min', plugin_dir_url(__FILE__) . 'css/font-awesome.min.css', array(), $this->version);
    }

    /**
     * Register the JavaScript for the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function afrsm_pro_enqueue_scripts() {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Advanced_Flat_Rate_Shipping_For_WooCommerce_Pro_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Advanced_Flat_Rate_Shipping_For_WooCommerce_Pro_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */
        wp_enqueue_script($this->plugin_name, plugin_dir_url(__FILE__) . 'js/advanced-flat-rate-shipping-for-woocommerce-public.js', array('jquery'), $this->version, false);
    }

    /**
     * This function return the template from this plugin, if it exists
     *
     * @since    1.0.0
     *
     * @param string $template
     * @param string $template_name that is only the filename
     * @param string $template_path
     *
     * @return string
     */
    public function afrsm_pro_wc_locate_template_sm_conditions($template, $template_name, $template_path) {

        global $woocommerce;
        $_template = $template;

        if (!$template_path) {
            $template_path = $woocommerce->template_url;
        }

        $plugin_path = advanced_flat_rate_shipping_for_woocommerce_pro_plugin_path() . '/woocommerce/';
        $template = locate_template(
                array(
                    $template_path . $template_name,
                    $template_name
                )
        );

        // Modification: Get the template from this plugin, if it exists
        if (!$template && file_exists($plugin_path . $template_name)) {
            $template = $plugin_path . $template_name;
        }

        if (!$template) {
            $template = $_template;
        }

        // Return what we found
        return $template;
    }

    /*
     * Function for update chosen shipping method and woocommerce cart total
     */
    public function afrsm_pro_update_chosen_shipping_method_total() {
        $packages = WC()->shipping->get_packages();
        $available_methods = array();

        if (!empty($packages) && is_array($packages)):
            foreach ($packages as $i => $package) {
                $available_methods = $package['rates'];
            }
        endif;

        $get_what_to_do_method = get_option('what_to_do_method');
        $get_what_to_do_method = !empty($get_what_to_do_method) ? $get_what_to_do_method : 'allow_customer';
        if (!empty($available_methods) && is_array($available_methods)) {
            if (!empty($get_what_to_do_method)) {
                if ($get_what_to_do_method == 'allow_customer') {

                    unset($available_methods['forceall']);

                    $available_methods = $available_methods;
                } elseif ($get_what_to_do_method == 'apply_highest') {

                    unset($available_methods['forceall']);

                    $max = -9999999;
                    $apply_highest = array();
                    $key = '';
                    foreach ($available_methods as $k => $v) {
                        if ($v->cost > $max) {
                            $max = $v->cost;
                            $key = $k;
                            $apply_highest = $v;
                        }
                    }
                    $apply_highest_method[$key] = $apply_highest;
                    $available_methods = $apply_highest_method;
                } elseif ($get_what_to_do_method == 'apply_smallest') {

                    unset($available_methods['forceall']);

                    $min = 9999999;
                    $apply_smallest = array();
                    $key = '';
                    foreach ($available_methods as $k => $v) {
                        if ($v->cost < $min) {
                            $min = $v->cost;
                            $key = $k;
                            $apply_smallest = $v;
                        }
                    }
                    $apply_smallest_method[$key] = $apply_smallest;
                    $available_methods = $apply_smallest_method;
                } elseif ($get_what_to_do_method == 'force_all') {

                    $forceall = array();
                    foreach ($available_methods as $k => $v) {
                        if ($k == 'forceall') {
                            $key = $k;
                            $forceall = $v;
                        }
                    }
                    $forceall_method[$key] = $forceall;
                    $available_methods = $forceall_method;
                }
            }
        }

        if (1 < count($available_methods)) {
            $chosen_shipping_methods = WC()->session->get('chosen_shipping_methods');
            if (in_array("forceall", $chosen_shipping_methods)) {
                $method = current($available_methods);
                $chosen_shipping_methods_array = explode(' ', $method->id);
                WC()->session->set('chosen_shipping_methods', $chosen_shipping_methods_array);
                WC()->cart->shipping_total = $method->cost;
            }
        } elseif (1 === count($available_methods)) {
            $method = current($available_methods);
            WC()->cart->shipping_total = $method->cost;
        }
    }

    /**
     * This function want to display method based on payment method
     *
     * @since   3.5
     */
    public function afrsm_pro_woocommerce_checkout_update_order_review() {
        $payment_method = filter_input( INPUT_POST, 'payment_method', FILTER_SANITIZE_STRING );
        WC()->session->set( 'chosen_payment_method', empty( $payment_method ) ? '' : sanitize_text_field( wc_clean( wp_unslash( $payment_method ) ) ) );
        $bool = true;
        if ( WC()->session->get( 'chosen_payment_method' ) ) {
            $bool = false;
        }

        $shipping_package = WC()->cart->get_shipping_packages();

        foreach ( array_keys( $shipping_package ) as $package_key ) {
            WC()->session->set( 'shipping_for_package_' . $package_key, $bool );
        }
        WC()->cart->calculate_shipping();
    }

    /*
     * Check shipping and remove shipping
     *
     * @since 3.5.2
     *
     * @param $rates
     * @param $package
     *
     * @return $rates
     */
    public function afrsm_pro_remove_shipping_method( $rates, $package ) {
        $get_what_to_do_method = get_option( 'what_to_do_method' );
        $get_what_to_do_method = ! empty( $get_what_to_do_method ) ? $get_what_to_do_method : 'allow_customer';
        $currency_symbol = get_woocommerce_currency_symbol();
        $combine_default_shipping_with_forceall = get_option( 'combine_default_shipping_with_forceall' );
        if ( 'force_all' === $get_what_to_do_method ) {
            if (isset($combine_default_shipping_with_forceall) && 'woo_our' === $combine_default_shipping_with_forceall) {
                if (!empty($rates)) {
                    $total_package_rate_cost = 0;
                    $total_package_rate_tax = 0;
                    $package_rate_label = '';
                    foreach ($rates as $rate_id => $rate) {
                        if ('free_shipping' === $rate->method_id
                            || 'flat_rate' === $rate->method_id
                            || 'local_pickup' === $rate->method_id
                            || 'advanced_flat_rate_shipping' === $rate->method_id) {
                            $package_rate_cost = $rate->cost;
                            $total_package_rate_cost += $rate->cost;
                            $tax_label = '';
                            if (!empty($rate->taxes)) {
                                foreach ($rate->taxes as $tax_cost) {
                                    $total_package_rate_tax += floatval($tax_cost);
                                    $package_rate_tax = floatval($tax_cost);
                                    $package_rate_cost += $package_rate_tax;

                                    $total_package_rate_cost += $package_rate_tax;

                                    $include_tax = wc_prices_include_tax();
                                    $display_prices_including_tax = WC()->cart->display_prices_including_tax();
                                    if ($display_prices_including_tax) {
                                        if ($tax_cost > 0 && !$include_tax) {
                                            $tax_label .= WC()->countries->inc_tax_or_vat();
                                        }
                                    } else {
                                        if ($tax_cost > 0 && $include_tax) {
                                            $tax_label .= WC()->countries->ex_tax_or_vat();
                                        }
                                    }
                                }
                            }
                            if ('forceall' !== $rate->id) {
                                $package_rate_label = $package_rate_label . '\n' . $rate->label . ' : ' . $currency_symbol . '' . floatval($package_rate_cost) . ' ' . $tax_label . "||" . $tax_label;
                            }
                            if ('forceall' !== $rate->id) {
                                unset($rates[$rate_id]);
                            }
                        }
                    }
                    $taxes = array();
                    $taxes[0] = $total_package_rate_tax;
                    $rates['forceall']->label = $package_rate_label;
                    $rates['forceall']->cost = $total_package_rate_cost;
                    $rates['forceall']->taxes = $taxes[0];
                }
                return $rates;
            } elseif (isset($combine_default_shipping_with_forceall) && 'all' === $combine_default_shipping_with_forceall) {
                if (!empty($rates)) {
                    $total_package_rate_cost = 0;
                    $total_package_rate_tax = 0;
                    $package_rate_label = '';
                    foreach ($rates as $rate_id => $rate) {
                        $package_rate_cost = $rate->cost;
                        $total_package_rate_cost += $rate->cost;
                        $tax_label = '';
                        if (!empty($rate->taxes)) {
                            foreach ($rate->taxes as $tax_cost) {
                                $total_package_rate_tax += floatval($tax_cost);
                                $package_rate_tax = floatval($tax_cost);
                                $package_rate_cost += $package_rate_tax;

                                $total_package_rate_cost += $package_rate_tax;

                                $include_tax = wc_prices_include_tax();
                                $display_prices_including_tax = WC()->cart->display_prices_including_tax();
                                if ($display_prices_including_tax) {
                                    if ($tax_cost > 0 && !$include_tax) {
                                        $tax_label .= WC()->countries->inc_tax_or_vat();
                                    }
                                } else {
                                    if ($tax_cost > 0 && $include_tax) {
                                        $tax_label .= WC()->countries->ex_tax_or_vat();
                                    }
                                }
                            }
                        }
                        if ('forceall' !== $rate->id) {
                            $package_rate_label = $package_rate_label . '\n' . $rate->label . ' : ' . $currency_symbol . '' . floatval($package_rate_cost) . ' ' . $tax_label . "||" . $tax_label;
                        }
                        if ('forceall' !== $rate->id) {
                            unset($rates[$rate_id]);
                        }
                    }
                    $taxes = array();
                    $taxes[0] = $total_package_rate_tax;
                    $rates['forceall']->label = $package_rate_label;
                    $rates['forceall']->cost = $total_package_rate_cost;
                    $rates['forceall']->taxes = $taxes[0];
                }
                return $rates;
            } else {
                $total_package_rate_cost = 0;
                $total_package_rate_tax = 0;
                $package_rate_label = '';
                foreach ($rates as $rate_id => $rate) {
                    if ('advanced_flat_rate_shipping' === $rate->method_id) {
                        $package_rate_cost = $rate->cost;
                        $total_package_rate_cost += $rate->cost;
                        $tax_label = '';
                        if (!empty($rate->taxes)) {
                            foreach ($rate->taxes as $tax_cost) {
                                $total_package_rate_tax += floatval($tax_cost);
                                $package_rate_tax = floatval($tax_cost);
                                $package_rate_cost += $package_rate_tax;

                                $total_package_rate_cost += $package_rate_tax;

                                $include_tax = wc_prices_include_tax();
                                $display_prices_including_tax = WC()->cart->display_prices_including_tax();
                                if ($display_prices_including_tax) {
                                    if ($tax_cost > 0 && !$include_tax) {
                                        $tax_label .= WC()->countries->inc_tax_or_vat();
                                    }
                                } else {
                                    if ($tax_cost > 0 && $include_tax) {
                                        $tax_label .= WC()->countries->ex_tax_or_vat();
                                    }
                                }
                            }
                        }
                        if ('forceall' !== $rate->id) {
                            $package_rate_label = $package_rate_label . '\n' . $rate->label . ' : ' . $currency_symbol . '' . floatval($package_rate_cost) . ' ' . $tax_label . "||" . $tax_label;
                        }
                        if ('forceall' !== $rate->id) {
                            unset($rates[$rate_id]);
                        }
                    }
                }
                $taxes = array();
                $taxes[0] = $total_package_rate_tax;
                $rates['forceall']->label = $package_rate_label;
                $rates['forceall']->cost = $total_package_rate_cost;
                $rates['forceall']->taxes = $taxes[0];
                return $rates;
            }
        } elseif ('apply_highest' === $get_what_to_do_method) {
            $check_highest = array();
            $highest_value_key_result = array();
            if (!empty($rates)) {
                foreach ($rates as $key => $rate) {
                    $check_highest[$key] = $rate->cost;
                }
            }

            if (!empty($check_highest)) {
                $highest_value_key_result = array_keys($check_highest, max($check_highest), true);
            }

            if (array_key_exists(0, $highest_value_key_result)) {
                $highest_value_key = $highest_value_key_result[0];
            } else {
                $highest_value_key = '';
            }

            foreach ($rates as $rate_id => $rate) {
                if ($highest_value_key !== $rate_id) {
                    unset($rates[$rate_id]);
                }
            }

            return $rates;
        }  elseif ('apply_smallest' === $get_what_to_do_method) {
            $check_smallest = array();
            $smallest_value_key_result = array();

            if (!empty($rates)) {
                foreach ($rates as $key => $rate) {
                    $check_smallest[$key] = $rate->cost;
                }
            }

            if (!empty($check_smallest)) {
                $smallest_value_key_result = array_keys($check_smallest, min($check_smallest), true);
            }

            if (array_key_exists(0, $smallest_value_key_result)) {
                $smallest_value_key = $smallest_value_key_result[0];
            } else {
                $smallest_value_key = '';
            }

            foreach ($rates as $rate_id => $rate) {
                if ($smallest_value_key !== $rate_id) {
                    unset($rates[$rate_id]);
                }
            }

            return $rates;
        } else {
            if (!empty($rates)) {
                foreach ($rates as $rate_id => $rate) {
                    if ('forceall' === $rate_id) {
                        unset($rates[$rate_id]);
                    }
                }
            }
            return $rates;
        }
    }

    /**
     * Remove WooCommerce currency symbol
     *
     * @since  1.0.0
     *
     * @uses get_woocommerce_currency_symbol()
     *
     * @param  float $price
     *
     * @return float $new_price2
     */
    public function afrsm_pro_remove_currency_symbol_public($price) {
        $wc_currency_symbol = get_woocommerce_currency_symbol();
        $new_price = str_replace($wc_currency_symbol, '', $price);
        $new_price2 = (double)preg_replace('/[^.\d]/', '', $new_price);
        return $new_price2;
    }

    /**
     * Forceall label for cart
     *
     * @since  3.6
     *
     * @param $new_lin_force_all_lable
     * @param $tool_tip_html
     * @param $method
     * @param $forceall_label
     *
     * @return array
     */
    public function afrsm_pro_forceall_label_for_cart($new_lin_force_all_lable, $tool_tip_html, $method, $forceall_label) {
        $total_shipping_lable = '';
        if (false !== strpos($method->label, '\n')) {
            $method_label_explode = explode('\n', $method->label);

            $check_taxable_array = array();
            foreach ($method_label_explode as $key => $label_value) {
                $label_value_ex = explode('||', $label_value);
                $value = '';
                $shipping_id = '';
                if (0 !== $key) {
                    if ($key > 1) {
                        $new_lin_force_all_lable .= '<br>';
                    }
                    $value = $label_value_ex[0];
                    if (array_key_exists('1', $label_value_ex)) {
                        $shipping_id = trim($label_value_ex[1]);
                    }
                    if (!empty($shipping_id) || '' !== $shipping_id) {
                        if ('(incl. tax)' === $shipping_id) {
                            $check_taxable_array[] = 'inc';
                        } else {
                            $check_taxable_array[] = 'exc';
                        }
                    }
                }
                $new_lin_force_all_lable .= $value;
            }
            if (in_array('inc', $check_taxable_array, true)) {
                $total_shipping_lable .= $new_lin_force_all_lable . '<br>(<b>' .
                    esc_html__('Total Shipping', 'advanced-flat-rate-shipping-for-woocommerce') . ': ' .
                    wp_kses(wc_price($method->cost), Advanced_Flat_Rate_Shipping_For_WooCommerce_Pro::afrsm_pro_allowed_html_tags()) .
                    '</b>' . ' <small class="tax_label">' . WC()->countries->inc_tax_or_vat() . '</small>' .')';
            } elseif (in_array('inc', $check_taxable_array, true)) {
                $total_shipping_lable .= $new_lin_force_all_lable . '<br>(<b>' .
                    esc_html__('Total Shipping', 'advanced-flat-rate-shipping-for-woocommerce') . ': ' .
                    wp_kses(wc_price($method->cost), Advanced_Flat_Rate_Shipping_For_WooCommerce_Pro::afrsm_pro_allowed_html_tags()) .
                    '</b>' . '  <small class="tax_label">' . WC()->countries->ex_tax_or_vat() . '</small>' .')';
            } else {
                $total_shipping_lable .= $new_lin_force_all_lable . '<br>(<b>' .
                    esc_html__('Total Shipping', 'advanced-flat-rate-shipping-for-woocommerce') . ': ' .
                    wp_kses(wc_price($method->cost), Advanced_Flat_Rate_Shipping_For_WooCommerce_Pro::afrsm_pro_allowed_html_tags()) .')';
            }
        }
        if (!empty($forceall_label)) {
            ob_start();
            ?>
            <div class="forceall-tooltip">
                <a><i class="fa fa-question-circle fa-lg"></i></a>
                <span class="tooltiptext"><?php echo wp_kses($new_lin_force_all_lable, array('br' => array())); ?></span>
            </div>
            <?php
            $tool_tip_html .= ob_get_contents();
            ob_end_clean();
            $method->label = $forceall_label;
        } else {
            $method->label = $new_lin_force_all_lable;
        }

        return array(
            'method_label' => $method->label,
            'total_shipping_lable' => $total_shipping_lable,
            'tool_tip_html' => $tool_tip_html
        );
    }
}
