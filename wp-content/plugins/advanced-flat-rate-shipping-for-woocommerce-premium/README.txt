=== Plugin Name ===
Contributors: (this should be a list of wordpress.org userid's), freemius
Donate link: http://www.multidots.com
Tags: comments, spam
Requires at least: 4.0
Tested up to: 5.2.3
Requires PHP: 5.7
Stable tag: 3.6
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Here is a short description of the plugin.  This should be no more than 150 characters. No markup here.

== Description ==

This is the long description.  No limit, and you can use Markdown (as well as in the following sections).

For backwards compatibility, if this section is missing, the full length of the short description will be used, and
Markdown parsed.

A few notes about the sections above:

*   "Contributors" is a comma separated list of wp.org/wp-plugins.org usernames
*   "Tags" is a comma separated list of tags that apply to the plugin
*   "Requires at least" is the lowest version that the plugin will work on
*   "Tested up to" is the highest version that you've *successfully used to test the plugin*. Note that it might work on
higher versions... this is just the highest one you've verified.
*   Stable tag should indicate the Subversion "tag" of the latest stable version, or "trunk," if you use `/trunk/` for
stable.

    Note that the `readme.txt` of the stable tag is the one that is considered the defining one for the plugin, so
if the `/trunk/readme.txt` file says that the stable tag is `4.3`, then it is `/tags/4.3/readme.txt` that'll be used
for displaying information about the plugin.  In this situation, the only thing considered from the trunk `readme.txt`
is the stable tag pointer.  Thus, if you develop in trunk, you can update the trunk `readme.txt` to reflect changes in
your in-development version, without having that information incorrectly disclosed about the current stable version
that lacks those changes -- as long as the trunk's `readme.txt` points to the correct stable tag.

    If no stable tag is provided, it is assumed that trunk is stable, but you should specify "trunk" if that's where
you put the stable version, in order to eliminate any doubt.

== Installation ==

This section describes how to install the plugin and get it working.

e.g.

1. Upload `advanced-flat-rate-shipping-for-woocommerce.php` to the `/wp-content/plugins/` directory
1. Activate the plugin through the 'Plugins' menu in WordPress
1. Place `<?php do_action('plugin_name_hook'); ?>` in your templates

== Frequently Asked Questions ==

= A question that someone might have =

An answer to that question.

= What about foo bar? =

Answer to foo bar dilemma.

== Screenshots ==

1. This screen shot description corresponds to screenshot-1.(png|jpg|jpeg|gif). Note that the screenshot is taken from
the /assets directory or the directory that contains the stable readme.txt (tags or trunk). Screenshots in the /assets
directory take precedence. For example, `/assets/screenshot-1.png` would win over `/tags/4.3/screenshot-1.png`
(or jpg, jpeg, gif).
2. This is the second screen shot

== Changelog ==
= 3.6 - 09-10-2019 =
* Compatible with hide shipping
* New - Custom text for forceall shipping
* New - New option for forceall shipping Like - Combine all shipping plugin's into one, Combine default WooCommerce and our plugins, and Combine shipping only our plugins
* New - Max qty/weight/subtotal validation in admin side
* New - AND/OR rule in General Shipping Rule
* New - AND/OR rule in Particular advance shipping rule
* New - Advance shipping cost based on Category subtotal
* New - Advance shipping cost based on Product subtotal
* New - Advance shipping cost based on Shipping class subtotal
* New - Use max_fee shortcode in price
* New - Unique name for shipping title (Admin Purpose only)
* Fixed - WPML Compatible with plugin's meta field
* Fixed - Zone issue - two condition in same row (is equal to and is not equal to)
* Fixed - UK based shipping zone postcode format (use equal to(=) instead of space)
* Fixed - Zone Migration

= 3.5.1 - 18-09-2019 =
* Fixed - Tooltip Issue

= 3.5 - 17-09-2019 =
* VIP minimum - Included with all version.
* WPML Compatible
* Compatible with WooCommerce 3.7

= 3.4.8 - 23-07-2019 =
* Virtual product rules considering in shipping condition issue has been resolved for Advance pricing rules.
* Tax amount does not display in shipping amount for multilanguage issue has been fixed.
* After three characters product search in Advanced Shipping Price Rules section issue has been fixed.

= 3.4.7 - 16-07-2019 =
* Tax amount does not display in shipping amount issue has been fixed.
* Force all '\n' character display issue issue has been fixed.
* Force all amount decimal only 1 number display issue has been fixed.
* Force all shipping amount within label display without tax issue has been fixed.
* After Apply Per Quantity shipping charges cart contain product rule set after first position issue has been fixed.

= 3.4.6 - 11-07-2019 =
* Change code as per WooCommerce requirement

= 3.4.6 - 04-06-2019 =
* Minor bug fixed (Notice: Undefined variable: key in advanced-flat-rate-shipping-for-woocommerce-premium/public/class-advanced-flat-rate-shipping-for-woocommerce-public.php on line 168)
* Minor bug fixed (Notice: Trying to get property 'cost' of non-object in advanced-flat-rate-shipping-for-woocommerce-premium/public/class-advanced-flat-rate-shipping-for-woocommerce-public.php on line 185)
* Advance Pricing Rules: Cost per Product with multiple selections (Allow variation product also)
* Advance Pricing Rules: Cost per Product Weight with multiple selections (Allow variation product also)

= 3.4.5 - 28-05-2019 =
* Compatible with PHP version 7.3.5
* Compatible with Wordpress 5.2.x
* Compatible with WooCommerce 3.6.x

= 3.4.4 - 21-05-2019 =
* Compatible with PHP version 7.2.10
* Compatible with WooCommerce 3.6.3

= 3.4.3 - 14-05-2019 =
* Decimal allow in weight section
* Minor bug fixing (Some time shipping method is not displaying)

= 3.4.2 - 14-05-2019 =
* Display message for cart subtotal after the discount rule
* Solved warning when user apply rule (Cart Subtotal (After Discount) ($)).
* Variable product's list is not displaying properly: Resolved
* Validation in the admin side. (When user select apply per qty and also check advance pricing rules then validation message will display)
* Blocker image is not displaying in the admin side - Resolved
* Some character consistency in the admin side
* Compatible with Wordpress 5.2

= 3.4.1 - 29-04-2019 =
* Change text in admin side for Advance Pricing Rules

= 3.4 - 29-04-2019 =
* Advance Pricing Rules: Cost per Product with multiple selections
* Advance Pricing Rules: Cost per Category with multiple selections
* Advance Pricing Rules: Cost per Total Cart Qty
* Advance Pricing Rules: Cost per Product Weight with multiple selections
* Advance Pricing Rules: Cost per Category Weight with multiple selections
* Advance Pricing Rules: Cost per Total Cart Weight
* Duplicate Shipping Method
* Enable/Disable Shipping Status in the list section
* Compatible with Wordpress 5.1.1 and WooCommerce 3.6.2

= 1.0 =
* A change since the previous version.
* Another change.

= 0.5 =
* List versions from most recent at top to oldest at bottom.

== Upgrade Notice ==

= 1.0 =
Upgrade notices describe the reason a user should upgrade.  No more than 300 characters.

= 0.5 =
This version fixes a security related bug.  Upgrade immediately.

== Arbitrary section ==

You may provide arbitrary sections, in the same format as the ones above.  This may be of use for extremely complicated
plugins where more information needs to be conveyed that doesn't fit into the categories of "description" or
"installation."  Arbitrary sections will be shown below the built-in sections outlined above.

== A brief Markdown Example ==

Ordered list:

1. Some feature
1. Another feature
1. Something else about the plugin

Unordered list:

* something
* something else
* third thing

Here's a link to [WordPress](http://wordpress.org/ "Your favorite software") and one to [Markdown's Syntax Documentation][markdown syntax].
Titles are optional, naturally.

[markdown syntax]: http://daringfireball.net/projects/markdown/syntax
            "Markdown is what the parser uses to process much of the readme file"

Markdown uses email style notation for blockquotes and I've been told:
> Asterisks for *emphasis*. Double it up  for **strong**.

`<?php code(); // goes in backticks ?>`