<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'hempoil_sql');
define('FS_METHOD', 'direct');
/** MySQL database username */
define('DB_USER', 'hempoil_admin');

define( 'WP_MEMORY_LIMIT', '256M' );

/** MySQL database password */
define('DB_PASSWORD', 'q@GT.%O=&G,@');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'R3O!6Cd,F[0ZpNwC-CP:Pu9a{m<$Y<`^i3dnZ/xH,>zev62PfQNONvGP2hy[_>$N');
define('SECURE_AUTH_KEY',  'xA-tW6oSd5|dxpSrs&99j>`PQJ2 DA5iM dKP}+rccmUVC/]W]n4Bm1MHHoQ5vLH');
define('LOGGED_IN_KEY',    'qkAND.OBdNGFmJ~(3hjo@a0y&/c%K0{K]r4{n<xVw7M,.xh0*}d{V#l|rDYj&VyB');
define('NONCE_KEY',        '.8l5-=C6r<1AK/E:~gc!2. MHMp%:0CDIGR,vP p{$YCYp28+Sx*/Ac>bE]tHbNj');
define('AUTH_SALT',        '#NS+ouA6FT*?`rKVXi)G)||ZEm_9<s(eO0qnzb%c.xgW0O/+mZP+E1OY!/V&/>gS');
define('SECURE_AUTH_SALT', 'x:<W%N{{A?{PTuT4k$+^/4zdY1N;SfDNM*T(3CP4fF~VecD4:rI2,2XoA4%PQzy%');
define('LOGGED_IN_SALT',   'C&&2]a1F620%QHH1!iVpFYr)9ry8=c0uJ@)[7}a585]G]U{4&0(Jy[Uwil&HF[:~');
define('NONCE_SALT',       'r-eR%8O Y]<F,:(!(lxxUE64o~b&],XNm2U{=iQoMQ;z->O_RpzYHD/-,XdtG/ .');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'devcg_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
