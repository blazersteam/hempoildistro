<?php 
function upstore_child_register_scripts() {
    $parent_style = 'upstore-style';

    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css', array('upstore-reset') );
    wp_enqueue_style( 'upstore-child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style )
    );
}
add_action( 'wp_enqueue_scripts', 'upstore_child_register_scripts' );
?>